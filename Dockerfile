FROM gitlab-registry.cern.ch/atlas/athena/analysisbase:25.2.42

# set a bunch of environment variables based on CI
ARG CI_COMMIT_SHA
ARG CI_COMMIT_REF_SLUG
ARG CI_COMMIT_TAG
ARG CI_JOB_URL
ARG CI_PROJECT_URL
ENV CI_COMMIT_SHA=$CI_COMMIT_SHA
ENV CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
ENV CI_COMMIT_TAG=$CI_COMMIT_TAG
ENV CI_JOB_URL=$CI_JOB_URL
ENV CI_PROJECT_URL=$CI_PROJECT_URL
# actually install the RPM
COPY ewk_1L2L.rpm /code/ewk_1L2L.rpm
RUN sudo rpm -i /code/ewk_1L2L.rpm && \
    sudo rm -rf /code/ewk_1L2L.rpm && \
    sudo chmod 666 /home/atlas/release_setup.sh && \
    sudo printf '\n# Set up the Ewk_1L2L code\nsource /usr/setup.sh\necho "Configured Ewk_1L2L from: $Ewk_1L2L_DIR"' >> /home/atlas/release_setup.sh && \
    sudo chmod 644 /home/atlas/release_setup.sh

