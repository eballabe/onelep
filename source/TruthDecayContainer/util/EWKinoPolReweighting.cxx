//
//  Demonstration of boson polarization correction in the EWKino pair production signals
//

// System include(s):
#include <memory>
#include <cstdlib>
#include <iostream>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TLegend.h>

#include "xAODRootAccess/Init.h"
#ifdef ROOTCORE
  #include "xAODRootAccess/TEvent.h"
#else
  #include "POOLRootAccess/TEvent.h"
#endif // ROOTCORE

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"

// Local include(s):
#include "TruthDecayContainer/BosonPolReweightingTool.h"
#include "TruthDecayContainer/ProcClassifier.h"
#include "TruthDecayContainer/TruthDecayUtils.h"

/////////////////////////////////////////////////////////
int main( int argc, char* argv[] ) {

  // The application's name:
  const char* APP_NAME = argv[ 0 ];

  // Unload the command line arguments
  TString inFilePath="";
  int maxEvents=-1;
  
  for(int k=0; k<argc; ++k) {
    TString key = argv[k];
    if(key=="-inputFile")   inFilePath  = argv[k+1];
    if(key=="-maxEvents")   maxEvents   = atoi(argv[k+1]);
  }

  // Check if we received a file name:
  if ( inFilePath=="" ) {
    TruthDecayUtils::ERROR( APP_NAME, "No file name received!" );
    TruthDecayUtils::ERROR( APP_NAME, "  Usage: %s -inputFile [full path to the xAOD file]", APP_NAME );
    TruthDecayUtils::ERROR( APP_NAME, "  Options: -maxEvents [Max number of events to loop over]");
    return 1;
  }

  // Open the input file:
  TruthDecayUtils::INFO( APP_NAME, "Opening file: %s", inFilePath.Data() );
  std::auto_ptr< TFile > ifile( TFile::Open( inFilePath, "READ" ) );
  ifile.get();

  // Create a TEvent object:
#ifdef ROOTCORE
  xAOD::TEvent event( xAOD::TEvent::kAthenaAccess );
#else
  POOL::TEvent event( POOL::TEvent::kAthenaAccess );
#endif

  CHECK( event.readFrom( ifile.get() ) );
  TruthDecayUtils::INFO( APP_NAME, "Number of events in the file: %i",
        static_cast< int >( event.getEntries() ) );
  
  // Check if this is simulation
  const xAOD::EventInfo* eventInfo = 0;
  event.getEntry(0);
  CHECK( event.retrieve( eventInfo, "EventInfo") );

  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
    TruthDecayUtils::ERROR( APP_NAME, "This is a data file. Exit. ");
    return 0;
  }

  // Create the tool(s) to test:
  BosonPolReweightingTool *bosonRwgtTool = new BosonPolReweightingTool();
  CHECK( bosonRwgtTool->init() );

  // ------------------------- Loop over the events ------------------------- //

  // Histograms for validation
  TH1F* hist_costhStar      = new TH1F("hist_costhStar_rwgt","Black: nominal / Red: re-weighted; cos#theta^{*} (W/Z, #tilde{#chi}); Entries",20,-1,1);
  TH1F* hist_costhStar_rwgt = new TH1F("hist_costhStar_rwgt","",20,-1,1);

  int nevt = maxEvents>0 ? maxEvents : 2000; 

  for ( int entry = 0; entry < nevt; ++entry ) {

    // Tell the object which entry to look at:
    event.getEntry( entry );

    // Retrieve containers (Note: use the TruthBSMWithDecays container in case of the TRUTH3 format)
    const xAOD::TruthParticleContainer* truthParticles = 0;
    if( !event.retrieve( truthParticles, "TruthParticles" ).isSuccess() ){
      TruthDecayUtils::WARNING(APP_NAME, "TruthParticles is not found in the file. Fallback to TruthBSMWithDecays.");      
      if( !event.retrieve( truthParticles, "TruthBSMWithDecays" ).isSuccess() ){
	TruthDecayUtils::ERROR(APP_NAME, "TruthBSMWithDecays is also not found in the file. Abort.");
	return 1;
      }
      //TruthDecayUtils::INFO(APP_NAME, "Found. TruthBSMWithDecays is going to be used.");      
    }
    //else  TruthDecayUtils::INFO(APP_NAME, "Found. TruthParticles is going to be used.");      

    if(entry<=10)
      TruthDecayUtils::INFO(APP_NAME, "////////////////////// Event: %i //////////////////////////////", entry);
    else if(entry%100==0)
      TruthDecayUtils::INFO(APP_NAME, "---- processed %i events", entry);

    // Calculate the polWeight for each relevant decay
    auto weights = bosonRwgtTool->getPolWeights(truthParticles, (entry<=10 ? true : false));

    //
    // You may also get the combined weight just by:
    //   bosonRwgtTool->getCombinedPolWeight(truthParticles);
    //

    // Fill histograms
    if(weights.size()>=1){
      const float cosThetaStar = weights[0].first;
      const float polWeight = weights[0].second;   
      hist_costhStar->Fill(cosThetaStar);
      hist_costhStar_rwgt->Fill(cosThetaStar,polWeight);
    }
    
  }
  
  // Draw validation histogram
  hist_costhStar->SetMinimum(0);
  hist_costhStar->SetMaximum(hist_costhStar_rwgt->GetMaximum()*1.3);

  hist_costhStar->SetLineWidth(2);
  hist_costhStar_rwgt->SetLineWidth(2);

  hist_costhStar->SetLineColor(1);
  hist_costhStar_rwgt->SetLineColor(2);

  hist_costhStar->Draw("h");
  hist_costhStar_rwgt->Draw("h sames");

  gPad->Print("test_polReweight.pdf","pdf");

  return 0;
}
