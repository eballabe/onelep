
//#include <vector>
//#include <map>
//#include <string>
//#include <utility>

#include "SusySkimDriver/xAODEvtLooper.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//#pragma link C++ class SampleSet+;
//#pragma link C++ class vector<int>+;
//#pragma link C++ class vector<TLorentzVector>+;

#pragma link C++ class xAODEvtLooper+;
//#pragma link C++ class std::vector< xAODEvtLooper* >+;

#endif
