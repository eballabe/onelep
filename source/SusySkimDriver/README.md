### Introduction

----

This package drives the code from the SusySkimMaker package and serves as the interface between this analysis package and the users analysis selector code.


Some commonly used commands:

git clone https://$USER@gitlab.cern.ch:8443/SusySkimAna/SusySkimDriver.git

git add README.md
git commit -m "add README"
git push -u origin master

### Setup

----

The setup of the analysis package is currently described here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisFramework


### Running example

----