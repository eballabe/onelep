#ifndef XAODNTUPLEANALYSIS_OneLep_H
#define XAODNTUPLEANALYSIS_OneLep_H

//RootCore
#include "SusySkimMaker/BaseUser.h"

class OneLep : public BaseUser
{

 public:
  OneLep();
  ~OneLep() {};

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool passMET(ConfigMgr*& configMgr);
  bool passMT(ConfigMgr*& configMgr);
  bool passHardLepton(ConfigMgr*& configMgr);
  bool passSoftLepton(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& /*mergeTool*/){return true; }
  bool execute_merge(MergeTool*& /*mergeTool*/){return true; }

  std::vector<TString> triggerChains;
  std::vector<TString> singleLepTriggers;
  std::vector<TString> singleLepTriggerBranches;

};

static const BaseUser* OneLep_instance __attribute__((used)) = new OneLep();

#endif
