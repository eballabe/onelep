#include "SusySkim1LInclusive/OneLep.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"

// ------------------------------------------------------------------------------------------ //
OneLep::OneLep() : BaseUser("SusySkim1LInclusive","OneLep")
{

}

// ------------------------------------------------------------------------------------------ //
void OneLep::setup(ConfigMgr*& configMgr)
{
  // Make a cutflow stream
  configMgr->cutflow->defineCutFlow("cutFlow",configMgr->treeMaker->getFile("tree"));

  // Use object def from SUSYTools
  configMgr->obj->useSUSYToolsSignalDef(true);

  // Turn on bad muon veto
  configMgr->obj->applyBadMuonVeto(true);
  configMgr->obj->applyCosmicMuonVeto(true);

  // Muon cuts
  configMgr->obj->setBaselineMuonPt(6.0);
  configMgr->obj->setBaselineMuonEta(2.50);
  configMgr->obj->setSignalMuonPt(6.0);
  configMgr->obj->setSignalMuonEta(2.50);

  // Electrons
  configMgr->obj->setBaselineElectronEt(7.0);
  configMgr->obj->setBaselineElectronEta(2.47);
  configMgr->obj->setSignalElectronEt(7.0);
  configMgr->obj->setSignalElectronEta(2.47);

  // Jets
  configMgr->obj->setCJetPt(30.0);
  configMgr->obj->setCJetEta(2.80);
  configMgr->obj->setFJetPt(30.0);
  configMgr->obj->setFJetEtaMin(2.50);
  configMgr->obj->setFJetEtaMax(4.50);

  // Triggers
  // https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled

  // Stores the trigger decision and corresponding SFs ( and their systematics )
  // Defined by run number <start,end>; -1 means ignore that bound
  
  // GRL
  std::vector<int> GR_2015 = {276262, 284484};
  std::vector<int> GR_2016 = {297730, 311481};
  std::vector<int> GR_2017 = {325713, 340453};
  std::vector<int> GR_2018 = {348885, 364292};
  std::vector<int> GR_2022 = {431810, 440613};
  std::vector<int> GR_2023 = {451587, 456749};
    
  // Single electron
  configMgr->addTriggerAna("HLT_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose",GR_2015[0],GR_2015[1], "singleElectronTrig"); // 2015
  configMgr->addTriggerAna("HLT_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0", GR_2016[0], GR_2018[1], "singleElectronTrig"); // 2016-2018
  configMgr->addTriggerAna("HLT_e26_lhtight_ivarloose_L1EM22VHI_OR_HLT_e60_lhmedium_L1EM22VHI_OR_HLT_e140_lhloose_L1EM22VHI",GR_2022[0],GR_2022[1], "singleElectronTrig"); // 2022
  configMgr->addTriggerAna("HLT_e26_lhtight_ivarloose_L1eEM26M_OR_e60_lhmedium_L1eEM26M_OR_HLT_e140_lhloose_L1eEM26M",GR_2023[0],GR_2023[1], "singleElectronTrig"); // 2023
    
  // Single muon
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu40",GR_2015[0],GR_2015[1],"singleMuonTrig"); // 2015
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",GR_2016[0],GR_2018[1],"singleMuonTrig"); // 2016 - 2018
  configMgr->addTriggerAna("HLT_mu24_ivarmedium_L1MU14FCH_OR_HLT_mu50_L1MU14FCH",GR_2022[0],GR_2023[1], "singleMuonTrig"); // 2022-2023

  // pure MET
  configMgr->addTriggerAna("HLT_xe70_mht",GR_2015[0],GR_2015[1],"metTrig"); // 2015
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",GR_2016[0],302919,"metTrig"); // 2016 A-D3
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",302919,GR_2016[1],"metTrig"); // 2016 D3-F2
  configMgr->addTriggerAna("HLT_xe110_pufit_L1XE55",GR_2017[0],GR_2017[1],"metTrig"); // 2017
  configMgr->addTriggerAna("HLT_xe110_pufit_xe70_L1XE50",GR_2018[0],GR_2018[1],"metTrig"); // 2018
  configMgr->addTriggerAna("HLT_xe65_cell_xe90_pfopufit_L1XE50_OR_HLT_xe80_cell_xe115_tcpufit_L1XE50",GR_2022[0],GR_2023[1],"metTrig"); // 2022-2023

  //
  // ---- Output trees ---- //
  //

  // Int variables
  configMgr->treeMaker->addIntVariable("AnalysisType",           0);
  configMgr->treeMaker->addIntVariable("nLep_base",              0);
  configMgr->treeMaker->addIntVariable("nLep_signal",            0);
  configMgr->treeMaker->addIntVariable("nJet30",                 0);
  configMgr->treeMaker->addIntVariable("nFatjets",               0);
  configMgr->treeMaker->addIntVariable("nBJet30",                0);
  configMgr->treeMaker->addIntVariable("nVtx",                   0);
  configMgr->treeMaker->addIntVariable("lep1IFFTruthClassifier", 0);
  configMgr->treeMaker->addIntVariable("lep2IFFTruthClassifier", 0);

  //Boolean Variables
  configMgr->treeMaker->addBoolVariable("IsOS", false);
  configMgr->treeMaker->addBoolVariable("IsSF", false);

  // Float variables
  configMgr->treeMaker->addFloatVariable("met",            -10.0);
  configMgr->treeMaker->addFloatVariable("mt",             -10.0);
  configMgr->treeMaker->addFloatVariable("mt2",            -10.0);
  configMgr->treeMaker->addFloatVariable("amt2",           -10.0);
  configMgr->treeMaker->addFloatVariable("mct",            -10.0);
  configMgr->treeMaker->addFloatVariable("mct2",           -10.0);
  configMgr->treeMaker->addFloatVariable("meffInc30",      -10.0);
  configMgr->treeMaker->addFloatVariable("met_Phi",        -10.0);
  configMgr->treeMaker->addFloatVariable("met_Signif",     -10.0);
  configMgr->treeMaker->addFloatVariable("dPhi_lep_met",   -10.0);
  configMgr->treeMaker->addFloatVariable("dPhi_jet_met",   -10.0);
  configMgr->treeMaker->addFloatVariable("Ht30",           -10.0);
  configMgr->treeMaker->addFloatVariable("lep1Pt",         -10.0);
  configMgr->treeMaker->addFloatVariable("lep1Eta",        -10.0);
  configMgr->treeMaker->addFloatVariable("lep1Phi",        -10.0);
  configMgr->treeMaker->addFloatVariable("lep2Pt",         -10.0);
  configMgr->treeMaker->addFloatVariable("lep2Eta",        -10.0);
  configMgr->treeMaker->addFloatVariable("lep2Phi",        -10.0);
  configMgr->treeMaker->addFloatVariable("mll",            -10.0);
  configMgr->treeMaker->addFloatVariable("Rll",            -10.0);
  configMgr->treeMaker->addFloatVariable("Ptll",           -10.0);
  configMgr->treeMaker->addFloatVariable("dPhiPllMet",     -10.0);
  configMgr->treeMaker->addFloatVariable("dPhill",         -10.0);
  configMgr->treeMaker->addFloatVariable("jet1Pt",         -10.0);
  configMgr->treeMaker->addFloatVariable("jet1Eta",        -10.0);
  configMgr->treeMaker->addFloatVariable("jet1Phi",        -10.0);
  configMgr->treeMaker->addFloatVariable("jet1M",          -10.0);
  configMgr->treeMaker->addFloatVariable("jet1bscore",     -10.0);
  configMgr->treeMaker->addFloatVariable("jet2Pt",         -10.0);
  configMgr->treeMaker->addFloatVariable("jet2Eta",        -10.0);
  configMgr->treeMaker->addFloatVariable("jet2Phi",        -10.0);
  configMgr->treeMaker->addFloatVariable("jet2M",          -10.0);
  configMgr->treeMaker->addFloatVariable("jet2bscore",     -10.0);
  configMgr->treeMaker->addFloatVariable("jet3Pt",         -10.0);
  configMgr->treeMaker->addFloatVariable("jet3Eta",        -10.0);
  configMgr->treeMaker->addFloatVariable("jet3Phi",        -10.0);
  configMgr->treeMaker->addFloatVariable("jet3M",          -10.0);
  configMgr->treeMaker->addFloatVariable("jet3bscore",     -10.0);
  configMgr->treeMaker->addFloatVariable("mjj",            -10.0);
  configMgr->treeMaker->addFloatVariable("Rjj",            -10.0);
  configMgr->treeMaker->addFloatVariable("bjet1Pt",        -10.0);
  configMgr->treeMaker->addFloatVariable("bjet1Eta",       -10.0);
  configMgr->treeMaker->addFloatVariable("bjet1Phi",       -10.0);
  configMgr->treeMaker->addFloatVariable("bjet1M",         -10.0);
  configMgr->treeMaker->addFloatVariable("bjet1bscore",    -10.0);
  configMgr->treeMaker->addFloatVariable("Rb1l",           -10.0);
  configMgr->treeMaker->addFloatVariable("Mb1l",           -10.0);
  configMgr->treeMaker->addFloatVariable("bjet2Pt",        -10.0);
  configMgr->treeMaker->addFloatVariable("bjet2Eta",       -10.0);
  configMgr->treeMaker->addFloatVariable("bjet2Phi",       -10.0);
  configMgr->treeMaker->addFloatVariable("bjet2M",         -10.0);
  configMgr->treeMaker->addFloatVariable("bjet2bscore",    -10.0);
  configMgr->treeMaker->addFloatVariable("Rb2l",           -10.0);
  configMgr->treeMaker->addFloatVariable("Mb2l",           -10.0);
  configMgr->treeMaker->addFloatVariable("Rbb",            -10.0);
  configMgr->treeMaker->addFloatVariable("Mbb",            -10.0);
  configMgr->treeMaker->addFloatVariable("mu",             -10.0);
  configMgr->treeMaker->addFloatVariable("actual_mu",      -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Pt",      -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1M",       -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Eta",     -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Phi",     -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Wtagged", -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Ztagged", -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1D2",      -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Tau21",   -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Tau32",   -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet1nTrk",    -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Pt",      -10.0);
  configMgr->treeMaker->addFloatVariable("fatjet2M",       -10.0);

  // Doubles
  configMgr->treeMaker->addDoubleVariable("pileupWeight",           1.0);
  configMgr->treeMaker->addDoubleVariable("leptonWeight",           1.0);
  configMgr->treeMaker->addDoubleVariable("eventWeight",            1.0);
  configMgr->treeMaker->addDoubleVariable("jvtWeight",              1.0);
  configMgr->treeMaker->addDoubleVariable("bTagWeight",             1.0);
  configMgr->treeMaker->addDoubleVariable("genWeight",              1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightUp",            1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightDown",          1.0);
  configMgr->treeMaker->addDoubleVariable("trigWeight_singleLep",   1.0);
  configMgr->treeMaker->addDoubleVariable("SherpaVjetsNjetsWeight", 1.0);
  configMgr->treeMaker->addDoubleVariable("fatjet1WSF",             1.0);
  configMgr->treeMaker->addDoubleVariable("fatjet1ZSF",             1.0);
  
  // Set lumi
  configMgr->objectTools->setLumi(1.0);

}
// ------------------------------------------------------------------------------------------ //
bool OneLep::doAnalysis(ConfigMgr*& configMgr)
{

  // Pre-selection cuts
  if( !passCuts(configMgr) ) return false;

  // Leading and subleading Lepton
  const LeptonVariable* lep1 = configMgr->obj->baseLeptons[0];
  const LeptonVariable* lep2 = configMgr->obj->baseLeptons[1];
    
  // Analysis Type
  int AnalysisType   = lep1->isMu() ? 2 : 1;

  // --------- Key observables ---------- //

  float dPhi_lep_met = abs( TVector2::Phi_mpi_pi(lep1->Phi()-configMgr->obj->met.phi) );

  // Transverse mass
  float mT  = getMt(configMgr->obj, true, 0);

  // mt2(L1,L2,MET) with various trial invisible particle masses
  float mt2  = getMt2(configMgr->obj, MT2LL, 0., false);

  // mt2 with jets
  float amt2 = getMt2(configMgr->obj, AMT2);

  // Mct variables
  float mct = getMct(configMgr->obj);
  float mct2 = getMct2(configMgr->obj);

  // Inclusive Meff, all jets with pT>30 GeV
  float meffInc30 = getMeff( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Ht: Scalar sum of jets
  float Ht30 = getHt( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  //PV
  int nVtx = configMgr->obj->evt.nVtx;

  // if( lep1->isMu() ){
  //   MuonVariable* mu = (MuonVariable*)lep1;
  //   configMgr->treeMaker->setFloatVariable("muQOverP", mu->q / ( mu->P() * 1000.0 ) );
  //   configMgr->treeMaker->setFloatVariable("muQOverPErr", mu->cbQoverPErr );
  //   configMgr->treeMaker->setIntVariable("muQuality",mu->quality );
  // }
  
  // Second lepton information
  float lep2Pt     = -10.0;
  float lep2Eta    = -10.0;
  float lep2Phi    = -10.0;
  float mll        = -10.0;
  float Rll        = -10.0;
  float Ptll       = -10.0;
  float dPhiPllMet = -10.0;
  float dPhill     = -10.0;
  int lep2IFFTruthClassifier = -1;

  bool isOS = false;
  bool isSF = false;

  if( configMgr->obj->baseLeptons.size() >= 2 ){
    lep2Pt     = lep2->Pt();
    lep2Eta    = lep2->Eta();
    lep2Phi    = lep2->Phi();

    mll = getMll(configMgr->obj, true);
    Rll = getRll(configMgr->obj);
    Ptll       = (*lep1 + *lep2).Pt();
    lep2IFFTruthClassifier = configMgr->obj->baseLeptons[1]->iff_type;
    dPhiPllMet   = (*lep1 + *lep2).DeltaPhi(configMgr->obj->met);
    dPhill       = (*lep1).DeltaPhi(*lep2);

    isOS = Observables::isOS(configMgr->obj->baseLeptons[0], configMgr->obj->baseLeptons[1]);
    isSF = Observables::isSF(configMgr->obj->baseLeptons[0], configMgr->obj->baseLeptons[1]); 

  }

  // Jets

  // Aplanarity
  // float JetAplanarity = getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),0.0, 30.0, true );
  // float LepAplanarity = getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),configMgr->obj->signalLeptons.size(), 30.0, true );

  // Jet multiplicity
  int nJet30  = getNJets( configMgr->obj, 30.0 );
  int nBJet30 = getNBJets( configMgr->obj, 30.0 );
  const int nFatJets = configMgr->obj->cFatjets.size();

  // Jet Kinematics
  float j1Pt         = -10.0;
  float j1Eta        = -10.0;
  float j1Phi        = -10.0;
  float j1M          = -10.0;
  float j1bscore     = -10.0;
  float dPhi_jet_met = -10.0;
  float j2Pt         = -10.0;
  float j2Eta        = -10.0;
  float j2Phi        = -10.0;
  float j2M          = -10.0;
  float j2bscore     = -10.0;
  float mjj          = -10.0;
  float Rjj          = -10.0;
  float j3Pt         = -10.0;
  float j3Eta        = -10.0;
  float j3Phi        = -10.0;
  float j3M          = -10.0;
  float j3bscore     = -10.0;

  if( configMgr->obj->cJets.size() >= 1 ){
    j1Pt         = configMgr->obj->cJets[0]->Pt();
    j1Eta        = configMgr->obj->cJets[0]->Eta();
    j1Phi        = configMgr->obj->cJets[0]->Phi();
    j1M          = configMgr->obj->cJets[0]->M();
    j1bscore     = configMgr->obj->cJets[0]->bjet_continuous;
    dPhi_jet_met = abs( TVector2::Phi_mpi_pi(j1Phi-configMgr->obj->met.phi) );

    if( configMgr->obj->cJets.size() >= 2 ){
      j2Pt       = configMgr->obj->cJets[1]->Pt();
      j2Eta      = configMgr->obj->cJets[1]->Eta();
      j2Phi      = configMgr->obj->cJets[1]->Phi();
      j2M        = configMgr->obj->cJets[1]->M();
      j2bscore   = configMgr->obj->cJets[1]->bjet_continuous;
      mjj        = sqrt ( 2*j1Pt*j2Pt*( cosh(j1Eta-j2Eta)-cos(j1Phi-j2Phi) ) );
      Rjj        = ( *(configMgr->obj->cJets).at(0) ).DeltaR( *(configMgr->obj->cJets).at(1) );
    }

      if( configMgr->obj->cJets.size() >= 3 ){
        j3Pt     = configMgr->obj->cJets[2]->Pt();
        j3Eta    = configMgr->obj->cJets[2]->Eta();
        j3Phi    = configMgr->obj->cJets[2]->Phi();
        j3M      = configMgr->obj->cJets[2]->M();
        j3bscore = configMgr->obj->cJets[2]->bjet_continuous;
    }

  }

  float bj1Pt      = -10.0;
  float bj1Eta     = -10.0;
  float bj1Phi     = -10.0;
  float bj1M       = -10.0;
  float bj1bscore  = -10.0;
  float Rb1l       = -10.0;
  float Mb1l       = -10.0;
  float bj2Pt      = -10.0;
  float bj2Eta     = -10.0;
  float bj2Phi     = -10.0;
  float bj2M       = -10.0;
  float bj2bscore  = -10.0;
  float Rb2l       = -10.0;
  float Mb2l       = -10.0;
  float Rbb        = -10.0;
  float Mbb        = -10.0;



  if (configMgr->obj->bJets.size() >= 1) {
    bj1Pt     = configMgr->obj->bJets[0]->Pt();
    bj1Eta    = configMgr->obj->bJets[0]->Eta();
    bj1Phi    = configMgr->obj->bJets[0]->Phi();
    bj1M      = configMgr->obj->bJets[0]->M();
    bj1bscore = configMgr->obj->bJets[0]->bjet_continuous;
    Rb1l      = configMgr->obj->baseLeptons[0]->DeltaR(*(configMgr->obj->bJets[0]));
    Mb1l      = (*(configMgr->obj->bJets[0]) + *(configMgr->obj->baseLeptons[0])).M();

    if (configMgr->obj->bJets.size() >= 2) {
      bj2Pt     = configMgr->obj->bJets[1]->Pt();
      bj2Eta    = configMgr->obj->bJets[1]->Eta();
      bj2Phi    = configMgr->obj->bJets[1]->Phi();
      bj2M      = configMgr->obj->bJets[1]->M();
      bj2bscore = configMgr->obj->bJets[1]->bjet_continuous;
      Rb2l      = configMgr->obj->baseLeptons[0]->DeltaR(*(configMgr->obj->bJets[1]));
      Mb2l      = (*(configMgr->obj->bJets[1]) + *(configMgr->obj->baseLeptons[0])).M();
      Rbb       = configMgr->obj->bJets[0]->DeltaR(*(configMgr->obj->bJets[1]));
      Mbb       = (*(configMgr->obj->bJets[0]) + *(configMgr->obj->bJets[1])).M();
    }
  }

  
  // Fat jets
  float fatjet1Pt      = -10.0;
  float fatjet1Eta     = -10.0;
  float fatjet1Phi     = -10.0;
  float fatjet1M       = -10.0;
  float fatjet1D2      = -10.0;
  float fatjet1Tau21   = -10.0;
  float fatjet1Tau32   = -10.0;
  float fatjet1Wtagged = -10.0;
  float fatjet1Ztagged = -10.0;
  double fatjet1WSF    = 1.0;
  double fatjet1ZSF    = 1.0;
  // float fatjet1XbbscoreHiggs = -10.0;
  // float fatjet1XbbscoreTop = -10.0;
  // float fatjet1XbbscoreQCD = -10.0;
  float fatjet1nTrk    = -10.0;
  float fatjet2Pt      = -10.0;
  float fatjet2M       = -10.0;
  
  if( configMgr->obj->cFatjets.size() >= 1 ){

    fatjet1Pt      = configMgr->obj->cFatjets[0]->Pt();
    fatjet1Eta     = configMgr->obj->cFatjets[0]->Eta();
    fatjet1Phi     = configMgr->obj->cFatjets[0]->Phi();
    fatjet1M       = configMgr->obj->cFatjets[0]->M();
    fatjet1D2      = configMgr->obj->cFatjets[0]->D2;
    fatjet1Tau21   = configMgr->obj->cFatjets[0]->Tau21;
    fatjet1Tau32   = configMgr->obj->cFatjets[0]->Tau32;
    fatjet1Wtagged = configMgr->obj->cFatjets[0]->passWTag80;
    fatjet1Ztagged = configMgr->obj->cFatjets[0]->passZTag80;
    fatjet1WSF     = configMgr->obj->cFatjets[0]->bjt_wsf80;
    fatjet1ZSF     = configMgr->obj->cFatjets[0]->bjt_zsf80;
    // fatjet1XbbscoreHiggs = configMgr->obj->cFatjets[0]->XbbScoreHiggs;
    // fatjet1XbbscoreTop = configMgr->obj->cFatjets[0]->XbbScoreTop;
    // fatjet1XbbscoreQCD = configMgr->obj->cFatjets[0]->XbbScoreQCD;
    fatjet1nTrk   = configMgr->obj->cFatjets[0]->nTrk;
    
    if( configMgr->obj->cFatjets.size() >= 2 ){
      fatjet2Pt   = configMgr->obj->cFatjets[1]->Pt();
      fatjet2M    = configMgr->obj->cFatjets[1]->M();
    }
  }

  // Truth information for ttbar
  // int intTTbarDecayMode = -1;
  // if( configMgr->obj->truthEvent.TTbarTLVs.size()>0 ){
  //   TruthEvent::TTbarDecayMode decayMode = configMgr->obj->truthEvent.decayMode;
  //   intTTbarDecayMode = (int)decayMode;
  //   const TLorentzVector* top     = configMgr->obj->truthEvent.getTTbarTLV("top");
  //   const TLorentzVector* antitop = configMgr->obj->truthEvent.getTTbarTLV("antitop");
  //   configMgr->treeMaker->setFloatVariable("truthTopPt", top->Pt()  );
  //   configMgr->treeMaker->setFloatVariable("truthAntiTopPt", antitop->Pt() );
  //   configMgr->treeMaker->setFloatVariable("truthTtbarPt", (*top+*antitop).Pt()  );
  //   configMgr->treeMaker->setFloatVariable("truthTtbarM", (*top+*antitop).M()  );
  //   float avgPt = (top->Pt() + antitop->Pt() ) / 2.0;
  //   float NNLOWeight = configMgr->objectTools->getTtbarNNLOWeight( avgPt*1000.0,configMgr->obj->evt.dsid,true);
  //   configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeight", NNLOWeight );
  //   configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeightUp", NNLOWeight );
  //   configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeightDown", NNLOWeight );
  // }

  // TTbar2LTagger
  // float ttReso = 0.;
  // float TopMass1 = 0.;
  // float TopMass2 = 0.;
  // float WMass1 = 0.;
  // float WMass2 = 0.;
  // int Npairs = getTTbar2LTagger( configMgr->obj, WMass1, WMass2, TopMass1, TopMass2, ttReso);




  //
  // -------- Fill the output tree variables ---------- //
  //

  configMgr->treeMaker->setIntVariable("AnalysisType",AnalysisType);
  configMgr->treeMaker->setIntVariable("nLep_base",configMgr->obj->baseLeptons.size());
  configMgr->treeMaker->setIntVariable("nLep_signal",configMgr->obj->signalLeptons.size());
  configMgr->treeMaker->setIntVariable("nJet30",nJet30);
  configMgr->treeMaker->setIntVariable("nFatjets",nFatJets);
  configMgr->treeMaker->setIntVariable("nBJet30",nBJet30);
  configMgr->treeMaker->setIntVariable("nVtx",nVtx);
  configMgr->treeMaker->setIntVariable("lep1IFFTruthClassifier",lep1->iff_type);
  configMgr->treeMaker->setIntVariable("lep2IFFTruthClassifier",lep2IFFTruthClassifier);  
  configMgr->treeMaker->setBoolVariable("IsOS",isOS);
  configMgr->treeMaker->setBoolVariable("IsSF",isSF);
  configMgr->treeMaker->setFloatVariable("met",configMgr->obj->met.Et);
  configMgr->treeMaker->setFloatVariable("mt",mT);
  configMgr->treeMaker->setFloatVariable("mt2",mt2);
  configMgr->treeMaker->setFloatVariable("amt2",amt2);
  configMgr->treeMaker->setFloatVariable("mct",mct);
  configMgr->treeMaker->setFloatVariable("mct2",mct2);
  configMgr->treeMaker->setFloatVariable("met_Phi",configMgr->obj->met.phi );
  configMgr->treeMaker->setFloatVariable("met_Signif",configMgr->obj->met.metSignif);
  configMgr->treeMaker->setFloatVariable("dPhi_lep_met",dPhi_lep_met);
  configMgr->treeMaker->setFloatVariable("dPhi_jet_met",dPhi_jet_met);
  configMgr->treeMaker->setFloatVariable("meffInc30",meffInc30);
  configMgr->treeMaker->setFloatVariable("Ht30",Ht30);
  configMgr->treeMaker->setFloatVariable("lep1Pt",lep1->Pt() );
  configMgr->treeMaker->setFloatVariable("lep1Eta",lep1->Eta() );
  configMgr->treeMaker->setFloatVariable("lep1Phi",lep1->Phi() );
  configMgr->treeMaker->setFloatVariable("lep2Pt",lep2Pt);
  configMgr->treeMaker->setFloatVariable("lep2Eta",lep2Eta);
  configMgr->treeMaker->setFloatVariable("lep2Phi",lep2Phi);
  configMgr->treeMaker->setFloatVariable("jet1Pt",j1Pt);
  configMgr->treeMaker->setFloatVariable("jet1Eta",j1Eta);
  configMgr->treeMaker->setFloatVariable("jet1Phi",j1Phi);
  configMgr->treeMaker->setFloatVariable("jet1M",j1M);
  configMgr->treeMaker->setFloatVariable("jet1bscore",j1bscore);
  configMgr->treeMaker->setFloatVariable("jet2Pt",j2Pt);
  configMgr->treeMaker->setFloatVariable("jet2Eta",j2Eta);
  configMgr->treeMaker->setFloatVariable("jet2Phi",j2Phi);
  configMgr->treeMaker->setFloatVariable("jet2M",j2M);
  configMgr->treeMaker->setFloatVariable("jet2bscore",j2bscore);
  configMgr->treeMaker->setFloatVariable("jet3Pt",j3Pt);
  configMgr->treeMaker->setFloatVariable("jet3Eta",j3Eta);
  configMgr->treeMaker->setFloatVariable("jet3Phi",j3Phi);
  configMgr->treeMaker->setFloatVariable("jet3M",j3M);
  configMgr->treeMaker->setFloatVariable("jet3bscore",j3bscore);
  configMgr->treeMaker->setFloatVariable("mll",mll);
  configMgr->treeMaker->setFloatVariable("Rll",Rll);
  configMgr->treeMaker->setFloatVariable("Ptll",Ptll);
  configMgr->treeMaker->setFloatVariable("dPhiPllMet",dPhiPllMet);
  configMgr->treeMaker->setFloatVariable("dPhill",dPhill);
  configMgr->treeMaker->setFloatVariable("mjj",mjj);
  configMgr->treeMaker->setFloatVariable("Rjj",Rjj);
  configMgr->treeMaker->setFloatVariable("bjet1Pt",bj1Pt);
  configMgr->treeMaker->setFloatVariable("bjet1Eta",bj1Eta);
  configMgr->treeMaker->setFloatVariable("bjet1Phi",bj1Phi);
  configMgr->treeMaker->setFloatVariable("bjet1M",bj1M);
  configMgr->treeMaker->setFloatVariable("bjet1bscore",bj1bscore);
  configMgr->treeMaker->setFloatVariable("Rb1l",Rb1l);
  configMgr->treeMaker->setFloatVariable("Mb1l",Mb1l);
  configMgr->treeMaker->setFloatVariable("bjet2Pt",bj2Pt);
  configMgr->treeMaker->setFloatVariable("bjet2Eta",bj2Eta);
  configMgr->treeMaker->setFloatVariable("bjet2Phi",bj2Phi);
  configMgr->treeMaker->setFloatVariable("bjet2M",bj2M);
  configMgr->treeMaker->setFloatVariable("bjet2bscore",bj2bscore);
  configMgr->treeMaker->setFloatVariable("Rb2l",Rb2l);
  configMgr->treeMaker->setFloatVariable("Mb2l",Mb2l);
  configMgr->treeMaker->setFloatVariable("Rbb",Rbb);
  configMgr->treeMaker->setFloatVariable("Mbb",Mbb);
  configMgr->treeMaker->setFloatVariable("mu",configMgr->obj->evt.avg_mu);
  configMgr->treeMaker->setFloatVariable("actual_mu",configMgr->obj->evt.actual_mu);
  configMgr->treeMaker->setFloatVariable("fatjet1Pt",fatjet1Pt);
  configMgr->treeMaker->setFloatVariable("fatjet1Eta",fatjet1Eta);
  configMgr->treeMaker->setFloatVariable("fatjet1Phi",fatjet1Phi);
  configMgr->treeMaker->setFloatVariable("fatjet1M",fatjet1M);
  configMgr->treeMaker->setFloatVariable("fatjet1D2",fatjet1D2);
  configMgr->treeMaker->setFloatVariable("fatjet1Tau21",fatjet1Tau21);
  configMgr->treeMaker->setFloatVariable("fatjet1Tau32",fatjet1Tau32);
  configMgr->treeMaker->setFloatVariable("fatjet1Wtagged",fatjet1Wtagged);
  configMgr->treeMaker->setFloatVariable("fatjet1Ztagged",fatjet1Ztagged);
  configMgr->treeMaker->setFloatVariable("fatjet1nTrk",fatjet1nTrk);
  configMgr->treeMaker->setDoubleVariable("fatjet1WSF",fatjet1WSF);
  configMgr->treeMaker->setDoubleVariable("fatjet1ZSF",fatjet1ZSF);
  configMgr->treeMaker->setFloatVariable("fatjet2Pt",fatjet2Pt);
  configMgr->treeMaker->setFloatVariable("fatjet2M",fatjet2M);


  // Triggers
  for (const char* chainName : this->triggerChains) {
    configMgr->treeMaker->setBoolVariable(chainName, configMgr->obj->evt.getHLTEvtTrigDec(chainName));
  }

  // Triggers
  for (unsigned int i=0; i<this->singleLepTriggers.size(); i++) {
    TString chainName = this->singleLepTriggers[i];
    TString branchName = this->singleLepTriggerBranches[i];
    auto it = lep1->triggerMap.find(chainName);
    bool match = false;
    if (it != lep1->triggerMap.end()) {
      match = true;
    }
    configMgr->treeMaker->setBoolVariable(branchName.Data(), match);
  }

  // Weights
  configMgr->treeMaker->setDoubleVariable("leptonWeight",          configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP));
  configMgr->treeMaker->setDoubleVariable("bTagWeight",            configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG));
  configMgr->treeMaker->setDoubleVariable("pileupWeight",          configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP));
  configMgr->treeMaker->setDoubleVariable("eventWeight",           configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT));
  configMgr->treeMaker->setDoubleVariable("genWeight",             configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("jvtWeight",             configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT));
  configMgr->treeMaker->setDoubleVariable("genWeightUp",           configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("genWeightDown",         configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("trigWeight_singleLep",  configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::TRIG,0,"singleLep"));
  configMgr->treeMaker->setDoubleVariable("SherpaVjetsNjetsWeight",configMgr->obj->evt.getSherpaVjetsNjetsWeight());

  configMgr->doWeightSystematics();

  // Fill the output tree
  configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  return true;

}
// ------------------------------------------------------------------------------------------ //
bool OneLep::passCuts(ConfigMgr*& configMgr)
{

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  // Fill cutflow histograms
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  // jets selection with pT > 30 GeV
  if( getNJets(configMgr->obj, 30.0) < 1 ) return false;
  configMgr->cutflow->bookCut("cutFlow", "At least one jet with pT>30 GeV", weight);

  // baseline leptons selection - 1L and 2L
  if ( configMgr->obj->baseLeptons.size() < 1 || configMgr->obj->baseLeptons.size() > 2 )   return false;
  configMgr->cutflow->bookCut("cutFlow", "One or two baseline leptons", weight);

  // signal leptons selection - 1L and 2L
  if ( configMgr->obj->signalLeptons.size() < 1 || configMgr->obj->signalLeptons.size() > 2 )   return false;
  configMgr->cutflow->bookCut("cutFlow", "One or two signal leptons", weight);

  // MET > 200 GeV
  if(configMgr->obj->met.Et < 200.0) return false;
  configMgr->cutflow->bookCut("cutFlow","At least MET>200 GeV", weight );

  // mT > 50 GeV
  if( getMt(configMgr->obj, true, 0) < 50.0) return false;
  configMgr->cutflow->bookCut("cutFlow","At least mT>50 GeV", weight );


  return true;
}


// ------------------------------------------------------------------------------------------ //
void OneLep::finalize(ConfigMgr*& configMgr)
{

  (void)configMgr;

}

