#!/bin/bash

########################
# Move output of job to desired structure
########################

#create directory
TEMP_UNMERGED_NAME="user.recast.$TEMP_DSID.atag_${2}_tree.root"
mkdir ${1} && cd ${1} && mkdir ${2} && cd ${2} && mkdir trees && cd trees && mkdir $TEMP_UNMERGED_NAME
mv $TEMP_HOME/submitDir*/data-tree/*.root $TEMP_UNMERGED_NAME

cd $TEMP_HOME
