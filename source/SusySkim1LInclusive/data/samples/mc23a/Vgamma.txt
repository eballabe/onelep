# NAME     : Vgamma
# DATA     : 0
# AF2      : 0
# PRIORITY : 0


mc23_13p6TeV.700770.Sh_2214_eegamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.700771.Sh_2214_mumugamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.700772.Sh_2214_tautaugamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.700773.Sh_2214_enugamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.700774.Sh_2214_munugamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.700775.Sh_2214_taunugamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.700776.Sh_2214_nunugamma.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491
mc23_13p6TeV.701170.Sh_2214_lvgammajj.deriv.DAOD_PHYS.e8514_s4162_r14622_p6491