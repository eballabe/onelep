#ifndef xAODNTUPLEMAKER_OBJECTTOOLS_H
#define xAODNTUPLEMAKER_OBJECTTOOLS_H

///
///
/// Written by Matthew Gignac
/// Email:    matthew.gignac@cern.ch
/// Institue: UBC/TRIUMF
///
/// This class contains general methods for running over xAODs and skims
///

// Rootcore includes
#include "SusySkimMaker/Objects.h"
#include "SusySkimMaker/TriggerTools.h"
// #include "NearbyLepIsoCorrection/NearbyLepIsoCorrection.h"
#include "xAODEgamma/PhotonContainer.h"

// xAOD includes
#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"

// Forward declarations
namespace ST
{
  class SUSYObjDef_xAOD;
  struct SystInfo;
}

class CentralDB;

class ObjectTools
{

 /*
    Work in progress. Dont use
 */

 public:
  void setSystematicSet(const CP::SystematicSet sysSet);

 private:

  ///
  /// Internal flag. Initialized in constructor as false. Set to true when all tools are created.
  ///
  bool m_toolsInit;

  ///
  /// Sub-routine for rebuilding the MET using only the leading lepton
  /// In principle everything is setup in ST, we just need to tell ST which
  /// leptons we want to exclude
  ///
  ConstDataVector<xAOD::IParticleContainer> getInvisibleParticlesForMET(const xAOD::MuonContainer* muon, 
                                                                        const xAOD::ElectronContainer* elec,
                                                                        const xAOD::PhotonContainer* phot,
                                                                        bool requireBaseline=true);

  ///
  /// Initialize ttbar NNLO tool. Done automatically when the weights are requested, user never needs to call this
  ///
  void init_tTbarNNLOTool(int DSID);

  ///
  /// (obsolete! migrated to CP::IsolationCloseByCorrectionTool in the ST)
  /// Initialize NearbyLepIsoCorr tool, when appropriate
  ///
  /// bool init_nearbyLepIsoCorr();

  ///
  /// Object filtering for skims
  ///
  bool passObjectFilter() const;

 public:
  ///
  /// Constructor
  ///
  ObjectTools();

  ///
  /// Destructor
  ///
  ~ObjectTools();

  ///
  /// Data structure to hold properties of the event that we
  /// would want to skim on
  /// Please ensure when you add any variable here, you give it a default value
  /// in the Reset function, which is called for each event
  ///
  struct EventProperties
  {
    int nBaseLeptons;
    int nSignalLeptons;
    int nBasePhotons;
    int nSignalPhotons;

    void Reset()
    {
      nBaseLeptons      = 0;
      nSignalLeptons    = 0;
      nBasePhotons      = 0;
      nSignalPhotons    = 0;
    }
  };

  ///
  /// Resets tools on an event by event basis
  ///
  void Reset_perEvt();

  ///
  /// Resets tools for each systematic
  ///
  void Reset_perSys();

  ///
  /// Initializes all tools and sets  m_toolsInit to true. Advantage of calling this before init is
  /// that the user can configure any tools this class uses before they are initialized in ObjectTools::init
  ///
  void createTools();

  ///
  /// Initialize all tools for running. If m_toolsInit is false, call ObjectTools::createTools.
  ///
  StatusCode init(xAOD::TEvent *event,
                  TreeMaker *&treeMaker,
                  MetaData *&metaData,
                  float nEvents,
                  bool isData,
                  bool isAf2,
                  bool truthOnly,
                  int  dsid,
                  bool doFatjet, 
                  bool doTrackjet);
  ///
  /// Interface to EventObject. Gets SUSY final state PDG code from SUSYTools and then forwards
  /// this information plus the result of the trigger decision to EventObject.
  ///
  bool get_xAOD_event(ST::SUSYObjDef_xAOD &susyObj,
                      const xAOD::EventInfo *eventInfo,
                      const xAOD::TruthEventContainer *truthEvents,
                      const xAOD::VertexContainer *primVertex,
                      const xAOD::JetContainer *jets,
                      const xAOD::MuonContainer *muons,
                      const xAOD::ElectronContainer *electrons,
                      const xAOD::PhotonContainer *photons);

  ///
  /// Rebuild the MET and forward information to MetObject tool.
  ///
   void get_xAOD_met(ST::SUSYObjDef_xAOD& susyObj,
                     xAOD::MissingETContainer* met,
                     xAOD::MissingETContainer* metTrack,
                     xAOD::MissingETContainer* met_leptons_invis,
		     xAOD::MissingETContainer* met_muons_invis,
                     xAOD::MissingETContainer* met_electrons_invis,
                     xAOD::MissingETContainer* met_photons_invis,
                     const xAOD::ElectronContainer* elec,
                     const xAOD::TauJetContainer* taujet,
                     const xAOD::MuonContainer* muon,
                     const xAOD::JetContainer* jet,
                     const xAOD::PhotonContainer* photon,
                     const xAOD::MissingETContainer* met_truth,
                     MetVariable::MetFlavor flavor);
  ///  
  /// Forward information to MuonObject tool. Check if muon passes passBaselineSelection(...)
  ///
  void get_xAOD_muons(xAOD::MuonContainer *muons,
                      const xAOD::VertexContainer *vertices,
                      const xAOD::EventInfo *eventInfo,
                      ST::SUSYObjDef_xAOD &susyObj);

  ///
  /// Forward information to ElectronObject tool. Check if electron passes passBaselineSelection(...)
  ///
  void get_xAOD_electrons(xAOD::ElectronContainer *electrons,
                          const xAOD::VertexContainer *vertices,
                          const xAOD::EventInfo *eventInfo,
                          ST::SUSYObjDef_xAOD &susyObj);
  
  ///
  /// Forward information to MuonObject tool. Check if muon passes passBaselineSelection(...)
  /// Note that this only gets called if the NearbyLepIsoCorr tool is being used!
  ///
  void get_xAOD_muons_NearbyLepIsoCorr(xAOD::MuonContainer *muons);
  ///
  /// Forward information to ElectronObject tool. Check if electron passes passBaselineSelection(...)
  /// Note that this only gets called if the NearbyLepIsoCorr tool is being used!
  ///
  void get_xAOD_electrons_NearbyLepIsoCorr(xAOD::ElectronContainer *electrons);
  ///
  /// Forward information to TauObject tool. Check if tau passes passBaselineSelection(...)
  ///
  void get_xAOD_taus(xAOD::TauJetContainer *taus,
                     const xAOD::VertexContainer *vertices);

  ///
  /// Forward information to JetObject tool. Check if jet passes passBaselineSelection(...)
  ///
  void get_xAOD_jets(xAOD::JetContainer *jets,
                     xAOD::JetContainer *fatjets,
                     xAOD::JetContainer *trackjets,
                     const xAOD::VertexContainer *vertices);
  
  ///
  /// Forward information to PhotonObjet Tool. Check if photon passes passBaselineSelection(...)
  ///
  StatusCode get_xAOD_photons(xAOD::PhotonContainer *photons);

  ///
  /// Check if a particle passes baseline selection.
  /// All objects required to pass 'baseline' selection from SUSYTools
  /// Overlap removal: all objects except taus have to have 'passOR' decoration from SUSYTools
  ///
  bool passBaselineSelection(const xAOD::IParticle *p);

  ///
  /// Methods for filling from truth information
  ///

  void get_xAOD_truth(const xAOD::TruthParticleContainer *truthParticles,
                      const xAOD::TruthParticleContainer *truthElectrons,
                      const xAOD::TruthParticleContainer *truthMuons,
                      const xAOD::TruthParticleContainer *truthTaus,
                      const xAOD::TruthParticleContainer *truthPhotons,
                      const xAOD::TruthParticleContainer *truthNeutrinos,
                      const xAOD::TruthParticleContainer *truthBosonWDP,
                      const xAOD::TruthParticleContainer *truthTopWDP,
                      const xAOD::TruthParticleContainer *truthBSMWDP,
                      const xAOD::TruthParticleContainer *truthBornLep,
                      const xAOD::TruthVertexContainer   *truthVertices,
                      const xAOD::JetContainer *truthJets,
                      const xAOD::JetContainer *truthFatjets,
                      const xAOD::EventInfo *eventInfo);

  void get_xAOD_electrons_truth(xAOD::TruthParticleContainer *electrons,
                                const float ptcut=4000.0,
                                const float etacut=2.47);

  void get_xAOD_muons_truth(xAOD::TruthParticleContainer *muons,
			    const float ptcut=4000.0,
			    const float etacut=2.7);

  void get_xAOD_photons_truth(xAOD::TruthParticleContainer *photons,
                             const float ptcut=10000.0,
                             const float etacut=2.37);

  void get_xAOD_met_truth(xAOD::MissingETContainer *met);

  void get_xAOD_jets_truth(xAOD::JetContainer *jets,
			   const float ptcut=20000.,
			   const float etacut=2.8);

  void get_xAOD_fatjets_truth(xAOD::JetContainer *fatjets,
			      const float ptcut=100000.,
			      const float etacut=2.0);

  bool get_xAOD_event_truth(const xAOD::EventInfo *eventInfo,
                            const xAOD::TruthParticleContainer *truthParticles,
                            const xAOD::TruthEventContainer *truthEvents);

  ///
  /// Apply NearbyLepIsoCorr tool to the leptons
  ///
  bool applyNearbyLepIsoCorr(ST::SUSYObjDef_xAOD& susyObj,
			     xAOD::ElectronContainer* electrons, 
                             xAOD::MuonContainer* muons);

  ///
  /// ttbar NNLO weights
  ///
  double getTtbarNNLOWeight(float topPtInMEV, int DSID, bool doExtended=true);

  ///
  /// Sherpa v2.2 weights
  ///
  float doSherpaVjetsNjetsWeight(const xAOD::EventInfo *eventInfo,
                                 ST::SUSYObjDef_xAOD &susyObj);

  /// Store emulated trigger decisisons
  ///
  std::map<TString,bool> getEmulatedTriggerMap(ST::SUSYObjDef_xAOD &susyObj);

  ///
  /// Do systematic weights. 
  ///
  void doWeights(ST::SUSYObjDef_xAOD &susyObj,
		 xAOD::MuonContainer *muons, 
		 xAOD::ElectronContainer *electrons,
		 xAOD::TauJetContainer *taus,
		 xAOD::JetContainer *jets,
		 xAOD::JetContainer *trackjets,
		 xAOD::PhotonContainer *photons,
		 std::vector<ST::SystInfo> sysWeights);

  ///
  /// Simply add an index decoration so we can retrieve each particle by this idx later
  ///
  void DecorateWithIdx(xAOD::IParticleContainer *p);
  int getIdx(const xAOD::IParticle *p);

  ///
  /// At a weight to objMap.
  /// TODO: The first one should be eventually replaced with the MCCorrections class
  ///
  void addWeightToMap(std::map<int,std::map<TString,float>>& objMap, int idx,TString sys,float SF);
  void addWeightToMap(std::map<TString,float>& objMap, TString sys,float SF);
  void addMCCorrection(std::map<int,MCCorrContainer*>& tripMap,int idx,TString instance,TString sys, float sf,float data_eff=-1.0, float mc_eff=-1.0);


  ///
  /// Get a weight from systematic weight map objMap.
  /// 
  const std::map<TString,float> getWeightsFromMap(std::map<int,std::map<TString,float>>& objMap, int idx);
  MCCorrContainer* getWeightsFromMap(std::map<int,MCCorrContainer*>& tripMap, int idx);
  /// ========================================= //

  ///
  /// General function to call event filtering methods. Calls passSherpaPtSliceOR
  ///
  bool passEventFiltering(const xAOD::TruthParticleContainer *truthParticles, 
                          const xAOD::EventInfo *eventInfo,
                          const xAOD::VertexContainer *vertices);

  ///
  /// Event based cuts at truth level. Checks if inclusive Sherpa W/Z +jets samples overlap.
  ///
  bool passSherpaPtSliceOR(const xAOD::TruthParticleContainer *truthParticles, const xAOD::EventInfo *eventInfo);

  ///
  /// Overlap between inclusive ttbar and mET filtered samples
  ///
  bool passTtbarMetOverlap(const xAOD::TruthParticleContainer *truthParticles, const xAOD::EventInfo *eventInfo);

  ///
  /// Overlap between Z+jet and Z+gamma samples
  ///
  bool passZjetZgammaOverlap(const xAOD::TruthParticleContainer *truthParticles, const xAOD::EventInfo *eventInfo);

  ///
  /// Overlap between inclusive ttbar and Ht filtered samples
  ///
  bool passTtbarHtOverlap(const xAOD::EventInfo *eventInfo);

  ///
  /// Remove the non-WplvWmqq decays from 363360
  ///
  bool passWplvWmqqOverlap(const xAOD::TruthParticleContainer *truthParticles, const xAOD::EventInfo *eventInfo);

  ///
  /// Naive check whether particle overlaps with a true tau - only implemented for check of taujet miscalibration
  ///
  bool isMatchedToTau(const TLorentzVector *particle, const xAOD::TruthParticleContainer *truthParticles);

 
  ///
  /// Wrapper for get_objects below. Called when running in xAOD mode (i.e. xAOD as input)
  ///
  void get_objects(Objects *&obj);


  ///
  /// Forward all objects to Objects class using Objects::readObjects(...). Also calls Objects::classifyObjects(...);
  /// 
  void get_objects(Objects *&obj, 
                   const EventVariable *skimEvt,
                   const MetVector *skimMet,
                   const MuonVector *skimMuons, 
                   const ElectronVector *skimElectrons,
                   const JetVector *skimJets,
                   const JetVector *skimFatjets,
                   const JetVector *skimTrackjets,
                   const TauVector *skimTaus,
                   const PhotonVector *skimPhotons,
                   const TrackVector *skimTracks,
                   const ObjectVector *skimCalo,
                   const TruthEvent *skimTruthEvent,
                   const TruthEvent_Vjets *skimTruthEvent_Vjets,
                   const TruthEvent_VV *skimTruthEvent_VV,
                   const TruthEvent_VVV *skimTruthEvent_VVV,
                   const TruthEvent_TT *skimTruthEvent_TT,
                   const TruthEvent_XX *skimTruthEvent_XX,
                   const TruthEvent_GG *skimTruthEvent_GG,
                   const TruthVector *skimTruth=0);

  ///
  /// Enum for getWeight function. ALL includes all the components below.
  ///
  /// genWeight     ('generator weight' - includes cross section and scales to target luminosity)
  /// pileupWeight  (pileup scale factors)
  /// eventWeight   (generator based weights)
  /// leptonWeight  (lepton scale factors)
  /// triggerWeight (trigger scale factors)
  /// bTagWeight    (b-tagging scale factors)
  ///
  enum WeightType{
    UNKNOWN     = 1 << 0,
    GEN         = 1 << 1,
    PILEUP      = 1 << 2,
    EVT         = 1 << 3,
    LEP         = 1 << 4,
    TRIG        = 1 << 5,
    BTAG        = 1 << 6,
    JVT         = 1 << 7,
    PILEUP_UP   = 1 << 8,
    PILEUP_DOWN = 1 << 9,
    BEAMSPOT    = 1 << 10,
    PHOTON      = 1 << 11,
    //BOSONTAG    = 1 << 11,
    ALL    = PILEUP|EVT|LEP|TRIG|BTAG|BEAMSPOT
  };

  // One to one for now...
  static WeightType getWeightTypeFromString(TString typeString);

  ///
  /// Return scaling weight, according to WeightType. Returns 1.0 if running over data.
  /// Multiple weights can be requested by passing the enums using bitwise or "|" as type
  /// e.g. getWeight(obj, sys, ObjectTools::GEN | ObjectTools::PILEUP | ObjectTools::EVT)
  /// To exclude a certain weight use bitwise xor "^"
  /// e.g. getWeight(obj, sys, ObjectTools::ALL ^ ObjectTools::BTAG)
  ///
  double getWeight(Objects* obj,TString sys="",int type=ALL, 
		   bool useTrackJets=false, TString trigChains="");

  ///
  /// Set luminosity to scale MC samples to [ in pb ]
  ///
  void setLumi(float lumi) { m_lumi=lumi; }

  ///
  /// Set the systematic state that we are processing
  /// Should only be called by ConfigMgr really
  ///
  void setSysState(std::string sysState);

  ///
  /// Retrieve LHE3 weights?
  ///
  void setWriteLHEWeights(bool writeLHE3) { m_eventObject->setWriteLHEWeights(writeLHE3); }

  ///
  /// Calling TriggerTools::LoadEvtTriggers from protected class m_trigTools.
  ///
  void LoadEvtTriggers();

  ///
  /// Object classes
  ///
  EventObject             *m_eventObject;
  MetObject               *m_metObject;
  MuonObject              *m_muonObject;
  ElectronObject          *m_electronObject;
  JetObject               *m_jetObject;
  JetObject               *m_fatjetObject;
  JetObject               *m_trackjetObject;
  TauObject               *m_tauObject;
  PhotonObject            *m_photonObject;
  TrackObject             *m_trackObject;
  TruthObject             *m_truthObject;
  TriggerTools            *m_trigTool;

  ///
  /// Log all meta data with this class and any other class included here.
  ///
  void Log(MetaData* metaData);

 protected:

  ///
  /// Target luminosity to scale MC samples, in pb-1
  ///
  float m_lumi;

  ///
  /// Maps to hold weight systematic variations, key is the object index, so these are object-by-object 
  /// scale factors
  ///
  std::map<int,std::map<TString,float>> m_muonSFs;
  std::map<int,std::map<TString,float>> m_electronSFs;
  std::map<int,std::map<TString,float>> m_photonSFs;
  std::map<int,std::map<TString,float>> m_tauSFs;
  std::map<int,std::map<TString,float>> m_jetSFs;
  std::map<int,std::map<TString,float>> m_jetJvtSFs;
  std::map<int,std::map<TString,float>> m_trackjetSFs;

  // Testing new trigger SFs
  std::map<int,MCCorrContainer*> m_muonTrigEffs;
  std::map<int,MCCorrContainer*> m_eleTrigEffs;
  std::map<int,MCCorrContainer*> m_photonTrigEffs;

  // Global photon trigger SFs
  std::map<TString,float> m_globalDiLepTrigSF;
  std::map<TString,float> m_globalMultiLepTrigSF;
  std::map<TString,float> m_globalPhotonTrigSF;
 
  ///
  /// Keep track of any events we filter from passSherpaPtSliceOR method.
  /// Key is DSID number, and value is a pair with key event number and value the truth level vector boson pT
  ///
  std::map<unsigned int, std::vector< std::pair<unsigned int,float> >> m_sherpaOverlapEvents; 

  // are we running on data?
  bool m_isData;
  bool m_truthonly;
  int  m_dsid;
  
  // Rebuild the MET only with the leading lepton, treat the rest as invisible
  bool m_excludeAdditionalLeptons;

  // Baseline filtering flags, set in CentralDB config file
  bool m_filterBaseline;
  bool m_filterOR;
  bool m_doSampleOR;
  bool m_doSampleOR_ZjetZgam;
  bool m_doSampleOR_WplvWmqq;
  bool m_requireTrigData;
  bool m_requireGRLData;
  int  m_nLepSkimFilter;
  int  m_nPhotonSkimFilter;
  bool m_doTriggerSFs;

  bool m_usePhotonsInMET;
  bool m_useTausInMET;

  bool m_useNearbyLepIsoCorr;
  bool m_doBTagging;
  bool m_saveGlobalDileptonTrigSFs;
  bool m_saveGlobalPhTrigSF;
  TString m_singleEleTrigSFChains;
  TString m_singleMuTrigSFChains2015;
  TString m_singleMuTrigSFChains201618;
  TString m_singleMuTrigSFChains2022;

  // (obsolete! Use SigLepPh.UseSigLepForIsoCloseByOR in ST config)
  // Use baseline vs signal leptons for the nearby correction?
  // std::string m_nearbyLepType;

  //
  EventProperties *m_evtProperties;

  // 
  //NNLOReweighter *m_tTbarNNLOTool; 

  // (obsolete! migrated to CP::IsolationCloseByCorrectionTool in the ST)
  // Nearby lepton isolation correction tool
  // NearLep::IsoCorrection* m_nearbyLepIsoCorr;

  ///
  /// Which systematic we are running over
  ///
  std::string m_sysState;

  std::vector<TString> m_triggerWeightSFs;

};

#endif
