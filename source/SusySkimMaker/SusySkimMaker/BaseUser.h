#ifndef SusySkimMaker_BASEUSER_H
#define SusySkimMaker_BASEUSER_H

// Tells the preprocessor to only include this file once
// We have a problem if we have many ana packages all using 
// this base class 
#pragma once

// RootCore
#include "SusySkimMaker/Observables.h"
#include "SusySkimMaker/ConfigMgr.h"
#include "SusySkimMaker/MergeTool.h"

// ROOT
#include "TFile.h"
#include "TString.h"
#include "TH1.h"

// C++
#include <vector>

class BaseUser : public Observables
{

  private:

    BaseUser(){};

  public:
  
    // Return selectors list as singleton 
    std::vector<BaseUser*> &getList() { 
      static std::unique_ptr< std::vector<BaseUser*> > m_selectorsList{ new std::vector<BaseUser*> }; 
      return *m_selectorsList;
    }

    // Add BaseUser instance to selectors list
    void addToList(BaseUser* instance) { 
      getList().push_back(instance); 
    }

    // Delete copy and move constructors, and assign operators
    // to avoid copies of the singleton to appear accidentally
    BaseUser(BaseUser const&)            = delete;  // Copy constructor
    BaseUser(BaseUser&&)                 = delete;  // Move constructor
    BaseUser& operator=(BaseUser const&) = delete;  // Copy assign
    BaseUser& operator=(BaseUser &&)     = delete;  // Move assign

    // Virtual methods specilised by selctor instances
    virtual void setup(ConfigMgr*& configMgr) = 0;
    virtual bool doAnalysis(ConfigMgr*& configMgr) = 0;
    virtual bool passCuts(ConfigMgr*& configMgr) = 0;
    virtual void finalize(ConfigMgr*& configMgr) = 0;

    virtual bool init_merge(MergeTool*& mergeTool) = 0;
    virtual bool execute_merge(MergeTool*& mergeTool) = 0;

    // Return the selector name 
    TString getSelectorName(){ return m_selectorName; } 

    // Return the RootCore package name in which 
    // this selector resides
    TString getPackageName(){ return m_packageName; }

    // Set and get sample name
    TString getSampleName() { return m_sampleName; }
    void setSampleName(TString sampleName){ m_sampleName = sampleName; }
    
    // Set flags
    void setTruthOnly(bool truthOnly){     m_truthOnly   = truthOnly; }
    void setDisableCuts(bool disableCuts){ m_disableCuts = disableCuts; }

  protected:

    // Protected constructor/destructor as per singleton pattern 
    BaseUser(TString packageName,TString selectorName);
    virtual ~BaseUser(){};

    // Protected data members storing selector and package name
    TString m_selectorName;
    TString m_packageName;
    TString m_sampleName;

    // Default set of flags that can be used inside the selector
    bool m_truthOnly;
    bool m_disableCuts;
  
};

// Global implementation, which allow the user to get 
// a BaseUser child class
BaseUser* getAnalysisSelector(TString name);

#endif


