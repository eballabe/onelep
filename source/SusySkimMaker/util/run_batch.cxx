#include <cstdlib>
#include <string>

// ROOT
#include "TChain.h"

// RootCore
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/ArgParser.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/StatusCodeCheck.h"

using namespace std;

int main(int argc, char** argv)
{

  const char* APP_NAME = "run_batch";

  TString baseDir           = "";
  TString outputDir         = "";
  TString selector          = "";
  TString groupSet          = "";
  TString tag               = "";
  TString prefix            = "user.*";
  TString deepConfig        = "";
  TString includeFiles      = "";
  TString excludeFiles      = "";
  TString batchStringfilter = "";
  TString treeDir   = "trees";
  bool noSubmit     = false;
  int priority      = -1;
  int setMaxQueuedJobs = -1;
  int setMaxEventsPerJob = 1000000;
  TString batchType = "TORQUE";
  bool splitLargeFiles = false;
  bool resubmitBatchJobs = false;
  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if( arg.parse(argv[i],"-d","Path to directory containing all samples") )
      baseDir = argv[++i];
    else if (arg.parse(argv[i], "-outputDir","Name of directory to put the output trees"))
      outputDir=argv[++i];
    else if (arg.parse(argv[i], "-selector","Selector name"))
      selector=argv[++i];
    else if (arg.parse(argv[i], "-treeDir","Name of directory to put the output trees"))
      treeDir=argv[++i];
    else if (arg.parse(argv[i],"-includeFiles","Specific files to include" ) )
      includeFiles = argv[++i];
    else if (arg.parse(argv[i],"-excludeFiles","Specific files to exclude" ) )
      excludeFiles = argv[++i];
    else if (arg.parse(argv[i], "-priority","Select samples only with this priority, specified as a property in sample text files"))
      priority=atoi(argv[++i]);
    else if (arg.parse(argv[i],"-tag","Tag for production you wish to process" ))
      tag = argv[++i];
    else if (arg.parse(argv[i],"-deepConfig","Deep configuration file" ))
      deepConfig = argv[++i];
    else if (arg.parse(argv[i],"-filter","Filter out any samples which don't contain this parameter (i.e. if you only want to submit a single DSID use '-filter DSID' " ))
      batchStringfilter = argv[++i];
    else if (arg.parse(argv[i],"-groupSet","Group set name" ) )
      groupSet = argv[++i];
    else if (arg.parse(argv[i],"-maxJobs","Max number of jobs that can be running + queued as a given time, only for TORQUE submissions" ) )
      setMaxQueuedJobs = atoi(argv[++i]);
    else if (arg.parse(argv[i],"-maxEvents","Max number of events per job" ) )
      setMaxEventsPerJob = atoi(argv[++i]);
    else if (arg.parse(argv[i], "-batchType","Which batch system? Supported: LRZ, TORQUE"))
      batchType = argv[++i];
    else if (arg.parse(argv[i], "--noSubmit","Don't submit to batch, just print out the input and output samples"))
      noSubmit=true;
    else if (arg.parse(argv[i], "--resubmit","Resubmit failed jobs"))
      resubmitBatchJobs=true;
    else if (arg.parse(argv[i], "--splitLargeFiles","Split large files into multiple sub jobs"))
      splitLargeFiles=true;
    else
    {
      std::cout << "Unknown argument" << argv[i] << std::endl;
      return 0;
    }
  }

  // Absoutely required for all executables
  CHECK( PkgDiscovery::Init( deepConfig ) );

  SampleHandler* sh = new SampleHandler();
  sh->includeFiles(includeFiles);
  sh->excludeFiles(excludeFiles);
  sh->setSamplePriority(priority);
  sh->noSubmit(noSubmit);
  sh->setBatchStringfilter(batchStringfilter);
  sh->setMaxQueuedJobs(setMaxQueuedJobs);
  sh->setMaxEventsPerJob(setMaxEventsPerJob);
  sh->initialize(outputDir,tag,groupSet,prefix);
  sh->setSplitLargeFiles(splitLargeFiles);
  sh->resubmitBatchJobs(resubmitBatchJobs);

  TString hostname = TString(gSystem->GetFromPipe("hostname -f"));

  if (hostname.Contains("lxe31.cos.lrz.de")) {
    std::cout << "Submitting from lxe31 - switching batch system to LRZ" << std::endl;
    batchType = "LRZ";
  }
  if (hostname.Contains("gar-ws-etp")) {
    std::cout << "Submitting from gar-ws-etp* - switching batch system to ETP" << std::endl;
    batchType = "ETP";
  }

  if (batchType == "TORQUE") {
    sh->submitByGroupSet(selector,SampleHandler::BATCH_TYPE::TORQUE);
  }
  else if (batchType == "CONDOR") {
    sh->submitByGroupSet(selector,SampleHandler::BATCH_TYPE::CONDOR);
  }
  else if (batchType == "LRZ") {
    sh->submitByGroupSet(selector,SampleHandler::BATCH_TYPE::LRZ);
  }
  else if (batchType == "ETP") {
    sh->submitByGroupSet(selector,SampleHandler::BATCH_TYPE::ETP);
  }
  else if (batchType == "LXPLUS") {
    sh->submitByGroupSet(selector,SampleHandler::BATCH_TYPE::LXPLUS);
  }
  else {
    std::cout << "Unsupported batch system: " << batchType << std::endl;
  }

  delete sh;

  return 0;

}
