#include <cstdlib>
#include <string>

// ROOT
#include "TChain.h"

// RootCore
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/ArgParser.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/StatusCodeCheck.h"

using namespace std;

int main(int argc, char** argv)
{

  const char* APP_NAME = "run_downloadPRW";

  TString baseDir        = "";
  TString deepConfig     = "";
  TString tag            = "";
  TString pTag           = "";
  TString groupSet       = "";
  TString includeFiles   = "";
  TString excludeFiles   = "";
  bool noSubmit          = false;
  int priority           = -1;

  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if( arg.parse(argv[i],"-d","Path to base directory. Will create three directories inside: trees, skims, and merged") )
      baseDir = argv[++i];
    else if (arg.parse(argv[i],"-deepConfig","Deep configuration file" ))
      deepConfig = argv[++i];
    else if (arg.parse(argv[i],"-groupSet","Group set name" ) )
      groupSet = argv[++i];
    else if (arg.parse(argv[i],"-tag","Tag for production you wish to process" ))
      tag = argv[++i];
    else if (arg.parse(argv[i],"-ptag","p-tag for the PRW profiles you want to download. Required!" ))
      pTag = argv[++i];
    else if (arg.parse(argv[i],"-includeFiles","Specific files to include" ) )
      includeFiles = argv[++i];
    else if (arg.parse(argv[i],"-excludeFiles","Specific files to exclude" ) )
      excludeFiles = argv[++i];
    else if (arg.parse(argv[i], "-priority","Select samples only with this priority, specified as a property in sample text files"))
      priority=atoi(argv[++i]);
    else if (arg.parse(argv[i], "--noSubmit","Don't submit, just print out the input and output samples"))
      noSubmit=true;
    else
    {
      std::cout << "Unknown argument" << argv[i] << std::endl;
      return 0;
    }
  }

  // Absoutely required for all executables
  CHECK( PkgDiscovery::Init( deepConfig ) );

  SampleHandler* sh = new SampleHandler();

  // Other useful flags
  sh->noSubmit(noSubmit);
  sh->includeFiles(includeFiles);
  sh->excludeFiles(excludeFiles);
  sh->setSamplePriority(priority);
  sh->setPTag(pTag);
  sh->initialize(baseDir,tag,groupSet,"user.*");

  // Do it
  sh->downloadPRW();

  return 0;

}
