
########################## READ BEFORE EDITING #########################
# Take note of the following:
#  1) Any line containing a '#' will be ignored
#     - Note that this means you cannot put comments at the end of the line!
#  2) Separate a field and its value by ":"
#  3) For booleans, use an interger: false==0, true==1
#  4) For paths, they will be expanded, so please always specify with respect to $ROOTCOREBIN env
########################################################################

#
# DxAOD Kernel name: This specifies which Bookkeeper data to extract
# and ultimately will be used to normalize the samples to the total sum of weights
#
#DxAODKernel : SUSY16KernelSkim
DxAODKernel : JETM4Kernel

# 
# Important directories which define where to look 
# for text files needed by the framework (if running in that mode)
#
#
SysFileDir         : $ROOTCOREBIN/data/SusySkim1LInclusive/systematics/
GroupSetDir        : $ROOTCOREBIN/data/SusySkim1LInclusive/samples/

SusyToolsConfigFile : SusySkimMaker/SUSYTools_SusySkimMaker_Default.conf

#
# Pile-up reweighting information
#
LumiCalc     : SusySkimMaker/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-005.root
LumiCalc     : SusySkimMaker/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root

PRWGeneral   : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc15c_latest.root
PRWGeneral   : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root
# PRWGeneral   : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc16a_latest.root

# Shut off PRW
DisablePRW : 0

# Ignore the GRL
IgnoreGRL : 0

#
# Number of events from event counter histograms
# Note really used much anymore, but here for completeness and flexiability
#

NumEvtPath : $ROOTCOREBIN/data/SusySkimMaker/evtCounters/T_02_11trunk/nEvents.root

#
# Good run list information
#
Grl : SusySkimMaker/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml
Grl : SusySkimMaker/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml

#
# Cross section path
# Alternatively you can read them from $ROOTCOREBIN/data/SUSYTools/mc15_13TeV/ if your local
# SUSYTools version is more up to date then the cvmfs version
#
CrossSection : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/xsdb/mc15_13TeV/

#
# MET Rebuilding, true or false
#
ExcludeAdditionalLeptons : 0

#
# Filtering options
#
FilterBaseline     : 1
FilterOR           : 1
SkimNLeptons       : 1
DoSampleOR         : 0
DoSampleORZjetZgam : 0

# Filter out the decays that shouldnt be in the Sherpa 2.2.1 WplvWmqq sample (363360)
DoSampleORWplvWmqq : 1

# Data specific filtering
RequireTrigInData : 1
RequireGRL : 1

# Misc configurations
#
UnitsFromMEV        : 0.001
SaveTruthInfo       : 1
WriteDetailedSkim   : 0

#
# Scale factor control
DoTriggerSFs : 1


# Truth level configurations
#
EmulateTruthBtagging : 0
SaveTruthEvtInfo: 0
SaveTruthEvtInfoFull: 0
