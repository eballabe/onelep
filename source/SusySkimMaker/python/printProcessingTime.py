#!/usr/bin/env python

# TODO: 
# - Load MetaData class
# - Draw all the histograms
# - Extract mean and RMS, and print it out
# - Allow this to be called from SampleHandler

import ROOT
import os,sys,argparse

ROOT.gROOT.ProcessLine(".x $ROOTCOREDIR/scripts/load_packages.C");
from ROOT import MetaData

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-f', dest='file_path',help='Path to the file you want to check')
args = parser.parse_args()
file_path=args.file_path

f = ROOT.TFile.Open( file_path , "READ" )
if not f or f.IsZombie():
      raise "Couldn't open file %s" % fileName

# Get the main event tree from the file:
t = f.Get( "MetaTree" )
if not t:
      raise "Couldn't find 'MetaTree; in file %s" % fileName

metaData = MetaData()
t.SetBranchAddress('metaData',metaData)

# Only checks the first element, doesn't work with merged files
# if the user want to check each of the MetaData
t.GetEntry(0)

#
for i in metaData.timeLog:
    print "Method name %-40s with rate %12.3f events/ms" % (i.first,i.second.GetMean())






