
// RootCore includes
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/Systematics.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "AsgMessaging/StatusCode.h"
#include "SusySkimMaker/MsgLog.h"
#include "PathResolver/PathResolver.h"

// ROOT includes
#include "TSystem.h"

// ------------------------------------------------------------- //
Systematics::Systematics() : m_sysFileDir("")
{

  m_sysInfoWeights.clear();
  m_sysInfoKinematics.clear();
  m_sysKinematics_skims.clear();
  m_sysKinematics_trees.clear();
  m_excludedSystematics.clear();
  m_triggerSFChains.clear();
  m_states.clear();

}
// ------------------------------------------------------------- //
StatusCode Systematics::OpenFile(TString sysSetName)
{

  // User doesn't want a subset
  if( sysSetName.IsWhitespace() ){
    return StatusCode::SUCCESS;
  }

  // Directory where user is storing systematic text files 
  CentralDB::retrieve(CentralDBFields::SYSFILEDIR,m_sysFileDir);

  // If the user provided a path in their steering file, 
  // include the path, otherwise it's assumed that the 
  // non-trivial sysSetName is the full path 
  TString path_to_sysSet = "";
  if( !m_sysFileDir.IsWhitespace() ){
    path_to_sysSet = m_sysFileDir + "/";
    path_to_sysSet += sysSetName;
  }
  else{
    path_to_sysSet = sysSetName;
  }

  // Expand out any env variables
  path_to_sysSet = gSystem->ExpandPathName( path_to_sysSet.Data() );

  // If that didn't work, try PathResolver
  if(!std::ifstream (path_to_sysSet).good()) {
    path_to_sysSet = PathResolverFindCalibFile(path_to_sysSet.Data());
  }

  // Open
  m_file.open ( path_to_sysSet.Data() );

  // Check file is valid
  if( !m_file.is_open() ){
    MsgLog::ERROR("Systematics::OpenFile","Cannot open systematic file %s", path_to_sysSet.Data() );
    return StatusCode::FAILURE;
  }

  //
  MsgLog::INFO("Systematics::OpenFile","Successfully opened systematic file %s", path_to_sysSet.Data() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------- //
StatusCode Systematics::initDefaultSet(int writeTrees, int writeSkims)
{

  if(writeTrees<=2 || writeSkims>0){

    //
    MsgLog::INFO("Systematics::initDefaultSet","Adding in default systematic set...");

    ST::SystInfo infodef;
    CP::SystematicSet defaultSet;

    // Nominal set
    infodef.systset           = defaultSet;
    infodef.affectsKinematics = true;
    infodef.affectsWeights    = true;
    infodef.affectsType       = ST::SystObjType::Unknown;

    // Save globally
    m_sysInfoKinematics.push_back(infodef);

    if(writeSkims>0) m_sysKinematics_skims.push_back("");
    if(writeTrees>0) m_sysKinematics_trees.push_back("");

  }

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------- //
StatusCode Systematics::init(ST::SUSYObjDef_xAOD& susyObj, int writeTrees, int writeSkims, TString sysSetName, bool isData)
{

  const char* APP_NAME = "Systematics";

  //
  MsgLog::INFO("Systematics::init", "Starting initialization for systematics class... ");

  // Add default set into global containers
  CHECK( initDefaultSet(writeTrees,writeSkims) );

  // At this point, we are done with any outputs not requiring systematics
  if( (writeSkims<2 && writeTrees<2) ) return StatusCode::SUCCESS;

  // Recommended list
  std::vector<ST::SystInfo> sysInfoList = susyObj.getSystInfoList();

  // For data, the only "syst" we might want to consider is LOOSE_NOMINAL
  if( isData) sysInfoList.clear();

  // Optional custom user-defined copy of the nominal "NoSys" but with a looser preselection.
  // The user would then need to define additional cuts in the selector which are applied
  // for all sys variations except for LOOSE_NOMINAL.
  // (and include LOOSE_NOMINAL in the systematics text file)
  ST::SystInfo nominalCopy;
  CP::SystematicSet defaultSet("LOOSE_NOMINAL");
  nominalCopy.systset = defaultSet;
  nominalCopy.affectsKinematics = true;
  nominalCopy.affectsWeights = false;
  nominalCopy.affectsType = ST::SystObjType::Unknown;
  nominalCopy.affectedWeights.clear();
  sysInfoList.push_back(nominalCopy);

  // Open text file, if requested
  CHECK( OpenFile( sysSetName ) );

  for(unsigned int i=0; i<sysInfoList.size(); i++){
      
    //
    TString sysName = sysInfoList[i].systset.name();

    std::cout<<"Adding systematcis "<<sysName.Data()<<std::endl;
    
    // We don't need to consider the nominal set, if it exists in the SUSYTools list
    if( sysName.IsWhitespace() ) continue;
    
    //
    bool goodSys    = true;
    TString sysType = "";

    // User wants a subset of the full systematic list
    // Or label/group some systematics to a common group
    if( m_file.is_open() ){

      // Now false until proven true
      goodSys=false;

      //
      TString sys;
      while( m_file ){

        sys.ReadLine(m_file);

        // Remove whitespace
        sys.ReplaceAll(" ","");

        // Ignore commented lines
        if( sys.Contains("#") && !sys.Contains("TYPE",TString::ECaseCompare::kIgnoreCase) ) continue;

        // Find state
        if( sys.Contains("#TYPE:", TString::ECaseCompare::kIgnoreCase ) ){

          // Blank type provided
          if( sys.Length()<=6 ){
            sysType = "";
            continue;
          }

          // The user has provided some type
          // No need to parse the file, so continue;
          sysType = ((TObjString*)sys.Tokenize(":")->At(1))->String();  
          sysType.ReplaceAll(" ","");
          continue;

        }

        if( sys == sysName ){
          goodSys=true;
          break;
        }

      }

      // Reset to start of file
      m_file.clear() ;
      m_file.seekg(0, std::ios::beg) ;
      
      // Only select user requested systematics
      if( !goodSys ){
        m_excludedSystematics.push_back( sysInfoList[i] );
        continue;
      }
      
    } // m_file selection 
    
    //
    if( !goodSys ) continue;

    // Systematics affecting only weights
    if( sysInfoList[i].affectsWeights ){
      
      // Full info
      m_sysInfoWeights.push_back( sysInfoList[i] );

      //
      //ObjectTools objTools;
      ObjectTools::WeightType weightType = ObjectTools::WeightType::ALL;
      //
      if( !sysType.IsWhitespace() ){
        weightType = ObjectTools::getWeightTypeFromString( sysType );
      }

      // User classification, if any
      auto it = m_weightSysMap.find( weightType );

      if( it == m_weightSysMap.end() ){
        std::vector<TString> vec;
        vec.clear();
        //
        vec.push_back(sysName);
        m_weightSysMap.insert( std::pair<ObjectTools::WeightType,std::vector<TString>>(weightType,vec) );
      } 
      else{
        it->second.push_back(sysName);
      }

    } // Weights
    
    // Systematics affecting only kinematics
    if( sysInfoList[i].affectsKinematics ){ 
      m_sysInfoKinematics.push_back( sysInfoList[i] );

      // Lists
      if(writeSkims>=2) m_sysKinematics_skims.push_back( sysName );
      if(writeTrees>=2) m_sysKinematics_trees.push_back( sysName );

    }
    
  }
 
  // Print out the above configuration 
  this->print();

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------- //
void Systematics::print()
{

  // Weights
  if( m_sysInfoWeights.size()>0 ){
    MsgLog::INFO("\nSystematics::print"," Printing systematics affecting weights only...");

    for(auto& s : m_sysInfoWeights){
      MsgLog::INFO("Systematics::print","    >>>> %s ",s.systset.name().c_str() );
    }
  }

  // Kinematics
  if( m_sysInfoKinematics.size()>0 ){
    MsgLog::INFO("\nSystematics::print"," Printing systematics affecting kinematics...");

    for(auto& s : m_sysInfoKinematics){
      MsgLog::INFO("Systematics::print","    >>>> %s ",s.systset.name().c_str() );
    }
  }

  // Excluded
  if( m_excludedSystematics.size()>0 ){
    MsgLog::WARNING("\nSystematics::print"," Printing excluded systematics...");

    for(auto& s : m_excludedSystematics){
      MsgLog::WARNING("Systematics::print","    >>>> %s ",s.systset.name().c_str() );
    }
  }


}
// ------------------------------------------------------------- //
void Systematics::addSysBranches(TreeMaker*& treeMaker, TString nomTree)
{

  // Nothing to do
  if( m_sysInfoWeights.size()==0 ) return;

  TString pState = treeMaker->getTreeState();
  treeMaker->setTreeState( nomTree ); 

  for( auto& type : m_weightSysMap ){

    //
    std::cout << "<Systematics::addSysBranches> INFO Adding " << type.second.size() << " systematic branches of type: " << getBranchPrefix(type.first) << std::endl;
    /*
    // Trigger systematics depend on which chains we
    // are using. Required input from the user through ConfigMgr
    if( type.first==ObjectTools::WeightType::TRIG ){
      for( auto& trig : m_triggerSFChains ){

        std::vector<TString> splitTrigSys;
        splitTrigSys.clear();

        for( auto& allTrigSys : type.second ){
          if( consistentTriggerAndSystematic(trig,allTrigSys) ) splitTrigSys.push_back( allTrigSys );
        }

        std::cout << "<Systematics::addSysBranches> INFO Adding above systematic for trigger chain " << trig << std::endl;
        treeMaker->addFloatVariables( getBranchPrefix(type.first,trig) ,splitTrigSys, 1.0 );
      } 
    } 
    else{
      treeMaker->addFloatVariables( getBranchPrefix(type.first), type.second, 1.0 );
    }
    */
    treeMaker->addFloatVariables( getBranchPrefix(type.first), type.second, 1.0 );
  }

  // Pileup 
  treeMaker->addFloatVariable("pileupWeightUp",1.0);
  treeMaker->addFloatVariable("pileupWeightDown",1.0);

  // Return to state it was in
  treeMaker->setTreeState( pState );

  // Cache this for setting later on
  m_states.push_back( nomTree );

}
// ------------------------------------------------------------- //
void Systematics::fillSysBranches(TreeMaker*& treeMaker,Objects* obj,ObjectTools* objTools, bool useTrackJets, TString trigChains)
{

  TString pState = treeMaker->getTreeState();

  // Loop over all states called by add method
  for(auto& state : m_states){

    // Globally associate this to a particular state
    treeMaker->setTreeState( state );

    for( auto& type : m_weightSysMap ){
      for( auto& weight : type.second ){
        treeMaker->setFloatVariable( getBranchPrefix(type.first,weight) ,objTools->getWeight(obj,weight,type.first,useTrackJets,trigChains) );
      }
    }

    // Pileup weights: note that the systematic name is irrelevant here
    treeMaker->setFloatVariable("pileupWeightUp", objTools->getWeight(obj,"",ObjectTools::WeightType::PILEUP_UP) );
    treeMaker->setFloatVariable("pileupWeightDown", objTools->getWeight(obj,"",ObjectTools::WeightType::PILEUP_DOWN));

  } // m_states

  // Return to state it was in
  treeMaker->setTreeState( pState );

}
// ------------------------------------------------------------- //
bool Systematics::consistentTriggerAndSystematic(TString trigger, TString systematic)
{

  int chain = 0;
  // TODO: How to match without assuming 2L?
  if( trigger.Contains("_mu") || trigger.Contains("_2mu")  ) chain=1;
  if( trigger.Contains("_e")  || trigger.Contains("_2e")   ) chain=2;
  if( trigger.Contains("_g")  || trigger.Contains("_2g")   ) chain=3;

  if( chain==1 && systematic.Contains("MUON_EFF_Trig")  ) return true;
  if( chain==2 && systematic.Contains("EL_EFF_Trigger") ) return true;
  if( chain==3 && systematic.Contains("PH_EFF_TRIGGER") ) return true;

  // Could not determine the trigger chain, 
  // Write out all them instead of not
  if( chain==0 ) return true;

   // Different streams!
   return false;

}
// ------------------------------------------------------------- //
bool Systematics::isTriggerSystematic(TString sys)
{

  // False until proven otherwise
  bool isTriggerSys = false;

  // No systematics
  if( m_weightSysMap.size()==0 ) return false;

  // 
  auto it = m_weightSysMap.find( ObjectTools::WeightType::TRIG );

  // Didn't find any trigger systematics
  if( it == m_weightSysMap.end() ){

    // Check if its associated with an inclusive systematic,
    // since we are storing ALL when the user doesn't provide a 
    // detailed systematic classification
    auto inc = m_weightSysMap.find( ObjectTools::WeightType::ALL );

    // Nothing to do with triggers found in this class
    if( inc == m_weightSysMap.end() )
      return isTriggerSys;
    else{
      for(auto& s : inc->second){
	if( s==sys ){
	  std::cout << "Found a match, when systematic is ALL" << std::endl;
	  isTriggerSys=true;
	}
      }
    }
  }
  else{
    for(auto& s : it->second){
      if( s==sys ){
	std::cout << "Found a match, when systematic is TRIG" << std::endl;
	isTriggerSys=true;
      }
    }
  }
  
  return isTriggerSys;
  
}
// ------------------------------------------------------------- //
TString Systematics::getBranchPrefix(ObjectTools::WeightType weightType, TString unique)
{

  TString prefix = "";

  if     ( weightType==ObjectTools::WeightType::LEP      ) prefix = "leptonWeight";  
  else if( weightType==ObjectTools::WeightType::BTAG     ) prefix = "bTagWeight";
  else if( weightType==ObjectTools::WeightType::TRIG     ) prefix = "trigWeight"; 
  else if( weightType==ObjectTools::WeightType::JVT      ) prefix = "jvtWeight";
  else if( weightType==ObjectTools::WeightType::PHOTON   ) prefix = "photonWeight";
  //else if( weightType==ObjectTools::WeightType::BOSONTAG ) prefix = "bosonTagWeight";

  if( !unique.IsWhitespace() ){
    if( prefix.IsWhitespace() ) prefix = unique;
    else                        prefix += "_"+unique;
  }

  return prefix;


}
// ------------------------------------------------------------- //
Systematics::~Systematics()
{

  if( m_file.is_open() ) m_file.close();

}
