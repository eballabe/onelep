#include "SusySkimMaker/TruthObject.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/Timer.h"
#include "AsgMessaging/StatusCode.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "TruthDecayContainer/ProcClassifier.h"

TruthObject::TruthObject() : m_convertFromMeV(0.001),
			     m_dsid(0),
			     m_saveTruthEvtInfoFull(false),
                             m_dressTruthLeptons(false),
			     m_truthVector(0),
                             m_truthEvent (0),
			     m_deltaRJetBHadron(0.4),
			     m_decayHandle(0),
			     m_bosonRwgtTool(0),
			     m_truthEvent_Vjets(0),
			     m_truthEvent_VV(0),
			     m_truthEvent_VVV(0),
			     m_truthEvent_TT(0),
			     m_truthEvent_XX(0),
			     m_truthEvent_GG(0)

{

  /// TODO:
  //    - Allow user to configure what type of properties the truth particles can have:
  //         - pT, eta, status, etc

}
// --------------------------------------------------------------------------------------------------------------- //
StatusCode TruthObject::init(TreeMaker*& treeMaker, int dsid)
{

  const char* APP_NAME = "TruthObject";

  m_dsid     = dsid;
  m_saveTruthEvtInfoFull = CentralDB::retrieve(CentralDBFields::SAVETRUTHEVTFULL);
  m_dressTruthLeptons    = CentralDB::retrieve(CentralDBFields::DRESSTRUTHLEPTONS);
  MsgLog::INFO("TruthObject::init","DSID %i", dsid);
  MsgLog::INFO("TruthObject::init","saveTruthEvtInfoFull %i", m_saveTruthEvtInfoFull);
  for(auto& sysName : treeMaker->getSysVector()){
    // Only consider nominal for this class
    if(sysName != "") continue;

    // Toggle saving of truth particles
    if( CentralDB::retrieve(CentralDBFields::SAVETRUTHPARTICLES) ){
      m_truthVector = new TruthVector();
    }

    // Toggle saving of truth event info
    if( CentralDB::retrieve(CentralDBFields::SAVETRUTHEVT) ){
      m_truthEvent  = new TruthEvent();

      // In case of storing more dedicated decay chain info?
      if( m_saveTruthEvtInfoFull ){
	if(ProcClassifier::IsWjets_Sherpa(m_dsid) || ProcClassifier::IsZjets_Sherpa(m_dsid) || ProcClassifier::IsGammaJets_Sherpa(m_dsid)){ // V+jets
	  m_truthEvent_Vjets = new TruthEvent_Vjets();
	  MsgLog::INFO("TruthObject::init","Initialized truthEvent_Vjets" );
	}
	if(ProcClassifier::IsVV(m_dsid) || ProcClassifier::IsWt(m_dsid) || ProcClassifier::IsZt(m_dsid)){    // VV (incl. t+V)
	  m_truthEvent_VV    = new TruthEvent_VV();
	  MsgLog::INFO("TruthObject::init","Initialized truthEvent_VV" );
	}
	if(ProcClassifier::IsVVV(m_dsid)){    // VVV
	  m_truthEvent_VVV    = new TruthEvent_VVV();
	  MsgLog::INFO("TruthObject::init","Initialized truthEvent_VVV" );
	}
	if(ProcClassifier::IsTT(m_dsid) || ProcClassifier::IsTTPlusX(m_dsid)){  // ttbar (+X)
	  m_truthEvent_TT    = new TruthEvent_TT();
	  MsgLog::INFO("TruthObject::init","Initialized truthEvent_TT" );
	}
	if(ProcClassifier::IsXX(m_dsid) || ProcClassifier::IsXXGGMZh(m_dsid)){ // EWKino pair production (gauge boson mediated, direct decay only)
	  m_truthEvent_XX    = new TruthEvent_XX();
	  MsgLog::INFO("TruthObject::init","Initialized truthEvent_XX" );
	}
	if(ProcClassifier::IsGG(m_dsid)){  // Gluino pair production (only for gauge boson mediated decay, upto 1-step)
	  m_truthEvent_GG    = new TruthEvent_GG();
	  MsgLog::INFO("TruthObject::init","Initialized truthEvent_GG" );
	}
      }

   }

    // Get tree
    TTree* tree = treeMaker->getTree("skim",sysName);

    if(tree==NULL) continue;
    else{

      if( m_truthVector ){
        MsgLog::INFO("TruthObject::init","Adding a branch 'truthParticles' to skim tree: %s ", tree->GetName() );
        tree->Branch("truthParticles",&m_truthVector);
      }

      if( m_truthEvent ){
        MsgLog::INFO("TruthObject::init","Adding a branch 'truthEvent' to skim tree: %s ", tree->GetName() );
        tree->Branch("truthEvent",&m_truthEvent);

	if(m_truthEvent_Vjets) tree->Branch("truthEvent_Vjets",   "TruthEvent_Vjets",   &m_truthEvent_Vjets);
	if(m_truthEvent_VV)    tree->Branch("truthEvent_VV",      "TruthEvent_VV",      &m_truthEvent_VV   );
	if(m_truthEvent_VVV)   tree->Branch("truthEvent_VVV",     "TruthEvent_VVV",     &m_truthEvent_VVV   );
	if(m_truthEvent_TT)    tree->Branch("truthEvent_TT",      "TruthEvent_TT",      &m_truthEvent_TT   );
	if(m_truthEvent_XX)    tree->Branch("truthEvent_XX",      "TruthEvent_XX",      &m_truthEvent_XX   );
	if(m_truthEvent_GG)    tree->Branch("truthEvent_GG",      "TruthEvent_GG",      &m_truthEvent_GG   );
      }
    }

  }

  //
  CHECK( init_tools() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------------------------- //
StatusCode TruthObject::init_tools()
{

  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);
  CentralDB::retrieve(CentralDBFields::DRJETBHADRON,m_deltaRJetBHadron);

  // Init TruthDecayContainer::DecayHandle
  m_decayHandle = new DecayHandle();

  if( !m_decayHandle->init(m_convertFromMeV).isSuccess() ){
    // Retry with the DSID input if the generator info in the FileMetaData is unknown
    CHECK( m_decayHandle->init(m_dsid, m_convertFromMeV) );
  }

  // Init TruthDecayContainer::BosonPolReweightingTool
  m_bosonRwgtTool = new BosonPolReweightingTool();
  CHECK( m_bosonRwgtTool->init(m_dsid) );

  // Return gracefully
  return StatusCode::SUCCESS;

}

StatusCode TruthObject::getBornMass_Sherpa_Phys(const xAOD::TruthParticleContainer* truthBornLep, Float_t& bornMass, Int_t leptonPDGID) {


  if(truthBornLep->size() != 2){
    std::cout<<" = "<<truthBornLep->size()<<std::endl;
    MsgLog::ERROR("TruthObject::getBornMass_Sherpa_Phys","Size of bornMass collection is not 2. It is %i",(int)truthBornLep->size());
    return StatusCode::FAILURE;
  }

  if(abs((*truthBornLep)[0]->pdgId()) != leptonPDGID or abs((*truthBornLep)[0]->pdgId()) != leptonPDGID){
    MsgLog::ERROR("TruthObject::getBornMass_Sherpa_Phys","Lepton 1 has id %i and lepton 2 has id %i. Not compatible with sought ID %i",(*truthBornLep)[0]->pdgId(),(*truthBornLep)[1]->pdgId(),leptonPDGID);
    return StatusCode::FAILURE;
  }


  //Calculate invariant mass
  TLorentzVector lepton1;
  lepton1.SetPxPyPzE((*truthBornLep)[0]->px(),(*truthBornLep)[0]->py(),
                      (*truthBornLep)[0]->pz(),(*truthBornLep)[0]->e());
  TLorentzVector lepton2;
  lepton2.SetPxPyPzE((*truthBornLep)[1]->px(),(*truthBornLep)[1]->py(),
                      (*truthBornLep)[1]->pz(),(*truthBornLep)[1]->e());


  bornMass = (lepton1 + lepton2).M();


  return StatusCode::SUCCESS;
}

StatusCode TruthObject::getBornMass_Sherpa(const xAOD::TruthVertexContainer* truthVertices, Float_t& bornMass, Int_t leptonPDGID) {

  xAOD::TruthVertexContainer::const_iterator truthV_itr; // Start iterating over truth container

  //Look for vertex with outgoing leptons, incoming partons, no FSR photon
  bool foundTheVertex = false;
  std::cout<<" truthVertices->size() = "<<truthVertices->size()<<std::endl;

  for (truthV_itr = truthVertices->begin(); truthV_itr != truthVertices->end(); ++truthV_itr ) {

    std::cout<<" (*truthV_itr) = "<<(*truthV_itr)<<std::endl;


    if ((*truthV_itr)->nOutgoingParticles() < 2)
      continue;

    bool foundLepton = false;
    bool foundAntiLepton = false;
    unsigned int leptonIdx = 9999;
    unsigned int antiLeptonIdx = 9999;
    bool foundPhoton = false;

    //Look for photon and leptons among outgoing particles
    for (unsigned int iOut=0; iOut < (*truthV_itr)->nOutgoingParticles(); iOut++) {
      if ((*truthV_itr)->outgoingParticle(iOut) && (*truthV_itr)->outgoingParticle(iOut)->pdgId() == 22)
        foundPhoton = true;

      if ((*truthV_itr)->outgoingParticle(iOut) && (*truthV_itr)->outgoingParticle(iOut)->pdgId() == leptonPDGID) {
        if (!foundLepton) {
          foundLepton = true;
          leptonIdx = iOut;
        }
        else {
          std::cout << "DileptonNTUPMaker::getBornMass_Sherpa: Found the lepton again??\n";
          return StatusCode::FAILURE;
        }
      }

      if ((*truthV_itr)->outgoingParticle(iOut) && (*truthV_itr)->outgoingParticle(iOut)->pdgId() == -leptonPDGID) {
        if (!foundAntiLepton) {
          foundAntiLepton = true;
          antiLeptonIdx = iOut;
        }
        else {
          std::cout << "DileptonNTUPMaker::getBornMass_Sherpa: Found the antilepton again??\n";
          return StatusCode::FAILURE;
        }
      }

    } //end loop over outgoing particles

    bool foundPartons = false;

    //Look for partons among incoming particles
    for (unsigned int iIn=0; iIn < (*truthV_itr)->nIncomingParticles(); iIn++) {

      if ((*truthV_itr)->incomingParticle(iIn) && (TMath::Abs((*truthV_itr)->incomingParticle(iIn)->pdgId()) == 21 || TMath::Abs((*truthV_itr)->incomingParticle(iIn)->pdgId()) < 6))
        foundPartons = true;
    } //end loop over incoming particles


    if ( !(foundLepton && foundAntiLepton && !foundPhoton && foundPartons) )
      continue; //this is not the right vertex

    //OK, we have the right vertex
    foundTheVertex = true;

    //Calculate invariant mass
    TLorentzVector lepton;
    lepton.SetPtEtaPhiE((*truthV_itr)->outgoingParticle(leptonIdx)->pt(),(*truthV_itr)->outgoingParticle(leptonIdx)->eta(),
                        (*truthV_itr)->outgoingParticle(leptonIdx)->phi(),(*truthV_itr)->outgoingParticle(leptonIdx)->e());
    TLorentzVector antiLepton;
    antiLepton.SetPtEtaPhiE((*truthV_itr)->outgoingParticle(antiLeptonIdx)->pt(),(*truthV_itr)->outgoingParticle(antiLeptonIdx)->eta(),
                            (*truthV_itr)->outgoingParticle(antiLeptonIdx)->phi(),(*truthV_itr)->outgoingParticle(antiLeptonIdx)->e());


    bornMass = (lepton + antiLepton).M();
  }

  if (!foundTheVertex)
    return StatusCode::FAILURE;

  return StatusCode::SUCCESS;
}


// ------------------------------------------------------------------------------------------- //
void TruthObject::classifyBosons(const xAOD::TruthParticleContainer* truthParticles,
				 const xAOD::EventInfo* eventInfo)
{
  // MG This method will be completely removed soon.

  //
  // This code is designed run over Sherpa samples
  // I'm not sure if it will work over other generators
  // Use with caution if not running over Sherpa
  //

  // Only for simulation
  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return;

  // Only for samples with valid TruthParticleContainer
  if( !truthParticles ) {
    MsgLog::WARNING("TruthObject::classifyBosons","Invalid truthParticle container!");
    return;
  }

  // Make a shallow copy, since we want to decorate
  std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > truthP = xAOD::shallowCopyContainer( *truthParticles );

  // Sherpa status constraint
  const static int kInterestingStatus = 3;

  //  ->  Sherpa for W or Zs. I.e. WplnuZnunu samples, will store the specified final state objects : W and Zs
  for( const auto& p : *truthP.first ){
    if( p->status() != kInterestingStatus ) continue;

    if( fabs(p->pdgId()) == 24 ){
      m_truthEvent->Wdecays.push_back( TruthEvent::WdecayMode::W_UNCLASSIFIED );
    }
    else if( fabs(p->pdgId()) == 22 || fabs(p->pdgId()) == 23 ){
      m_truthEvent->Zdecays.push_back( TruthEvent::ZdecayMode::Z_UNCLASSIFIED );
    }

    // TODO: Add some m_dsid matching to get W decay type
    //       Maybe do via sample name??

  } // Loop over truth particles


  //printf("Found %i W and %i Z bosons \n", int(m_truthEvent->Wdecays.size()),int(m_truthEvent->Zdecays.size()) );

  // Found something already above
  if( m_truthEvent->Wdecays.size()!=0 || m_truthEvent->Zdecays.size()!=0 ){
    delete truthP.first;
    delete truthP.second;
    return;
  }

  // Initalization
  for( const auto& p : *truthP.first ){
    p->auxdata<int>("used") = 0;
  }

  // Begin loop
  for( const auto& particle1 : *truthP.first){

    // Skip particles that were already matched
    if( particle1->auxdata<int>("used")==1 ){
      continue;
    }

    // Sherpa status requirement
    if( particle1->status() != kInterestingStatus ) continue;

    //
    int pdgId1 =  particle1->pdgId();

    // Classification of the leptonic decay
    if( fabs(pdgId1)>=11 && fabs(pdgId1)<=16 ){

      // Now search for something interesting
      for( const auto& particle2 : *truthP.first ){

        // Don't double count
        if( particle1 == particle2 ) continue;

        // Final state particles only
        if( particle2->status() != kInterestingStatus ) continue;

        // Particles that haven't been used only
        if( particle2->auxdata<int>("used")==1 ) continue;

        int pdgId2 = particle2->pdgId();

        // Always true for boson decays
        if(pdgId1 * pdgId2 > 0) continue;

        // W-decays
        if( fabs( fabs(pdgId1)- fabs(pdgId2) ) == 1 ){

	  //
	  TruthEvent::WdecayMode mode = TruthEvent::WdecayMode::W_NONE;

	  // Classify the W decay now
	  if( fabs(pdgId1)==11 || fabs(pdgId2)==11 || fabs(pdgId1)==13 || fabs(pdgId2)==13 )
	    mode = TruthEvent::WdecayMode::W_LLEP;
	  if( fabs(pdgId1)==15 || fabs(pdgId2)==15 )
	    mode = TruthEvent::WdecayMode::W_TAU;

	  //
	  m_truthEvent->Wdecays.push_back(mode);

          /*
          bool doit = true;
          if( !w1 ){
            w1 = new TLorentzVector();
            w1->SetPtEtaPhiM( (particle1->p4()+particle2->p4()).Pt(),
                              (particle1->p4()+particle2->p4()).Eta(),
                              (particle1->p4()+particle2->p4()).Phi(),
                              (particle1->p4()+particle2->p4()).M() );
            doit=false;
          }

          if( !w2 && doit ){
            w2 = new TLorentzVector();
            w2->SetPtEtaPhiM( (particle1->p4()+particle2->p4()).Pt(),
                              (particle1->p4()+particle2->p4()).Eta(),
                              (particle1->p4()+particle2->p4()).Phi(),
                              (particle1->p4()+particle2->p4()).M() );
          }
          */

	  //
	  particle1->auxdata<int>("used") = 1;
	  particle2->auxdata<int>("used") = 1;
          break;
        }

        // Z-decays
        if( pdgId1 == -pdgId2 ){

	  //
	  TruthEvent::ZdecayMode mode = TruthEvent::ZdecayMode::Z_NONE;

	  // Classify the Z decay now
	  if( fabs(pdgId1)==12 || fabs(pdgId1)==14 || fabs(pdgId1)==16 )
	    mode = TruthEvent::ZdecayMode::Z_INVISIBLE;
	  if( fabs(pdgId1)==11 || fabs(pdgId1)==13 )
	    mode = TruthEvent::ZdecayMode::Z_LLEP;
	  if( fabs(pdgId1)==15 )
	    mode = TruthEvent::ZdecayMode::Z_TAU;

	  //
	  m_truthEvent->Zdecays.push_back( mode );

	  //
	  particle1->auxdata<int>("used") = 1;
	  particle2->auxdata<int>("used") = 1;
          break;
        }

      } // Second truth particle loop
    } // Leptonic decays of the bosons

/*


  M.G: Quark decays disabled for now, since all hadronic samples
       are generated with intermediate bosons, and never by final state.
       Including these at the moment present challenges, since the initial
       colliding partons are included in the Sherpa truth record,
       and for example, we find the Z in the qq->Z->WW->lvlv process,
       when we are only interested in the WW final states.


    // Now lets consider quark decays of the bosons
    else if( fabs(pdgId1)>=1 && fabs(pdgId1)<=6) {

      // Now search for something interesting
      for( const auto& particle2 : *truthP.first ){

        // Don't double count
        if( particle1 == particle2 ) continue;

        // Final state particles only
        if( particle2->status() != kInterestingStatus ) continue;

	// Only unused particles
	if( particle2->auxdata<int>("used")==1 ) continue;

	// Boson and quark decays constraints
        int pdgId2 = particle2->pdgId();

        // Always true for boson decays
        if( pdgId1 * pdgId2 > 0 ) continue;

        // Quarks
	if( fabs(pdgId2)<1 || fabs(pdgId2)>6 ) continue;

	// Z-decays. No flavour changing neutral currents!
	if( pdgId1 == -pdgId2 ){

	  // Should we want to quantify this a bit more, we can
	  // At the moment, leave it general as hadronic
	  TruthEvent::ZdecayMode mode = TruthEvent::ZdecayMode::Z_HADRONIC;

	  //m_truthEvent->Zdecays.push_back( mode );

          if( !z1 ){
            z1 = new TLorentzVector();
            z1->SetPtEtaPhiM( (particle1->p4()+particle2->p4()).Pt(),
                              (particle1->p4()+particle2->p4()).Eta(),
                              (particle1->p4()+particle2->p4()).Phi(),
                              (particle1->p4()+particle2->p4()).M() );
          }

	  //
	  particle1->auxdata<int>("used") = 1;
	  particle2->auxdata<int>("used") = 1;
          break;

	} // Z's

	// W-decays are a bit more complicated,
	// since we can have cross generation coupling...
	// Lets just ensure that we have a change in flavor

	if( fabs( fabs(pdgId1)-fabs(pdgId2) ) >=1 ){

	  // Should we want to quantify this a bit more, we can
	  // At the moment, leave it general as hadronic
	  // Will be nice to understand HF or LF decays
	  TruthEvent::WdecayMode mode = TruthEvent::WdecayMode::W_HADRONIC;

	  m_truthEvent->Wdecays.push_back( mode );

	  //
	  particle1->auxdata<int>("used") = 1;
	  particle2->auxdata<int>("used") = 1;
          break;

	} // W's

	// The rest are probably from ISR/FSR gluino splitting, etc
	continue;

      }	// Second truth particle loop
    } // Quark decays
*/

  } // Full truth container

/*
  if( m_truthEvent->Wdecays.size()==0 && m_truthEvent->Zdecays.size()==0 ){
    std::cout << "Didn't find any bosons, listing truth record..." << std::endl;
    for( const auto& particle1 : *truthP.first){
      std::cout << "Particle pdg ID: " << particle1->pdgId() << " and status " << particle1->status() << std::endl;
    }
  }
*/


/*
  if( w1 && w2 && z1 ){
    printf("\n");
    printf("Z boson mass: %f and pT %f and eta %f and phi %f \n",z1->M(),z1->Pt(),z1->Eta(),z1->Phi() );
    printf("WW  mass:     %f and pT %f and eta %f and phi %f \n",(*w1+*w2).M(),(*w1+*w2).Pt(),(*w1+*w2).Eta(),(*w1+*w2).Phi() );
    printf("W1 boson mass: %f and pT %f \n",w1->M(),w1->Pt() );
    printf("W2 boson mass: %f and pT %f \n",w2->M(),w2->Pt() );
  }

  if( w1 ) delete w1;
  if( w2 ) delete w2;
  if( z1 ) delete z1;
*/

  delete truthP.first;
  delete truthP.second;

}
// --------------------------------------------------------------------------------------------------------------- //
void TruthObject::fillTruthEventContainer(const xAOD::TruthParticleContainer* truthParticles,
                                          const xAOD::TruthParticleContainer* truthElectrons,
                                          const xAOD::TruthParticleContainer* truthMuons,
                                          const xAOD::TruthParticleContainer* truthTaus,
                                          const xAOD::TruthParticleContainer* truthPhotons,
                                          const xAOD::TruthParticleContainer* truthNeutrinos, 
                                          const xAOD::TruthParticleContainer* truthBosonWDP,
                                          const xAOD::TruthParticleContainer* truthTopWDP,
                                          const xAOD::TruthParticleContainer* truthBSMWDP,
                                          const xAOD::TruthParticleContainer* truthBornLep,
                                          const xAOD::TruthVertexContainer*   truthVertices,
                                          const xAOD::JetContainer* truthJets,
                                          const xAOD::JetContainer* truthFatjets,
                                          const xAOD::EventInfo* eventInfo)
{

  //
  if(!m_truthEvent) return;

  Timer::Instance()->Start( "TruthObject::fillTruthEventContainer" );

  // most derivations don't have the TruthParticles container anymore, which is
  // required by the functions below, so skip these in that case and avoid the
  // warnings about the missing TruthParticle container
  // TODO: maybe some functionality can be restored by used the TRUTH3-style
  // containers (TruthBSM, ...) instead?
  std::map<TString,TLorentzVector*> ttbarTLVs;
  std::map<TString,TLorentzVector*> tauTLVs;
  int TauMode1 = 0;
  int TauMode2 = 0;
  Float_t bornMass = -1;
  Int_t leptonPDGID = -999;
  TruthEvent::TTbarDecayMode decayMode = TruthEvent::TTbarDecayMode::NONE;
  double truthMll = 0.0;
  if (truthParticles){
    // Mostly for diboson, should also work for single Sherpa diboson
    classifyBosons(truthParticles,eventInfo);


    // Find ttbar TLorentzVectors
    int pdgId11 = 0;
    int pdgId12 = 0;
    int pdgId21 = 0;
    int pdgId22 = 0;

    //

    ttbarTLVs.clear();
    tauTLVs.clear();

    //
    if( !TruthFinder_tau(truthParticles,eventInfo,tauTLVs) ){
      MsgLog::WARNING("TruthObject::fillTruthEventContainer","Couldn not classify taus in TruthFinder_tau method!" );
    }

    if( !TruthFinder_tt(truthParticles,eventInfo,ttbarTLVs,pdgId11,
                        pdgId12,pdgId21,pdgId22,TauMode1,TauMode2) ){
      MsgLog::WARNING("TruthObject::fillTruthEventContainer","Could not classify ttbar system in TruthFinder_tt!");
    }

    // ttbar decay mode
    decayMode = GetTTbarMode( pdgId11,pdgId21,TauMode1,TauMode2 );

    // Get the truth mll for N2->Z*+N1->ll+N1 decay
    // (also handles on-shell Z->ll)
    truthMll = getN2TruthMll(truthParticles,eventInfo);
  }

  TString kernel = "";
  CentralDB::retrieve(CentralDBFields::DXAODKERNEL, kernel);
  // If DAOD_PHYS: TruthVertices are not avaialable so get born leptons directly from seperate BornLeptons collection 
  if(kernel.Contains("PHYS")){
    if(isSherpaZllMC(eventInfo, leptonPDGID)){
      if(getBornMass_Sherpa_Phys(truthBornLep, bornMass, leptonPDGID) != StatusCode::SUCCESS) {
        MsgLog::ERROR("TruthObject::fillTruthEventContainer","could not correctly extract born mass for this Sherpa sample!");
      }
    }
  }else{
    if(isSherpaZllMC(eventInfo, leptonPDGID)){
      if(getBornMass_Sherpa(truthVertices, bornMass, leptonPDGID) != StatusCode::SUCCESS) {
        MsgLog::ERROR("TruthObject::fillTruthEventContainer","could not correctly extract born mass for this Sherpa sample!");
      }
    }
  }


  // Save and write out
  m_truthEvent->bornMass = bornMass;
  m_truthEvent->TTbarTLVs = ttbarTLVs;
  m_truthEvent->TauTLVs   = tauTLVs;
  m_truthEvent->decayMode = decayMode;
  m_truthEvent->Tau1decayMode = TauMode1;
  m_truthEvent->Tau2decayMode = TauMode2;
  m_truthEvent->truthMll = truthMll * m_convertFromMeV; // GeV
  m_truthEvent->polWeight = 1.;

  // Save truth fat jets
  m_truthEvent->truthFatjets.clear();
  if(truthFatjets){
    for(auto truthFatjet : *truthFatjets){
      TLorentzVector this_fatjet(0,0,0,0);
      this_fatjet.SetPtEtaPhiM( truthFatjet->pt() * m_convertFromMeV,
				truthFatjet->eta(),
				truthFatjet->phi(),
				truthFatjet->m() * m_convertFromMeV );
      m_truthEvent->truthFatjets.push_back(this_fatjet);
    }
  }

  // Count the truth jets
  m_truthEvent->nAntiKt4TruthJets20=0;
  for(auto truthJet : *truthJets){
    if(truthJet->pt()/1000. > 20.) m_truthEvent->nAntiKt4TruthJets20++;
  }
  // Save info for DecayModeVector for truth tau from W
  // Choosing the first tau from W found; maybe find a better way to select the 'right' one
  int nTruthTauFromW = 0;
  m_truthEvent->TruthTauFromW_DMV.clear();
  if (truthTaus)
  {
    for (auto truthTau : *truthTaus)
    {
      if (truthTau->auxdata<unsigned int>("classifierParticleOrigin") == 12)
      {
        nTruthTauFromW++;
        if (nTruthTauFromW == 1 && truthTau->isAvailable<std::vector<int>>("DecayModeVector"))
        {
          m_truthEvent->TruthTauFromW_DMV = truthTau->auxdata<std::vector<int>>("DecayModeVector");
        }
      }
    }
  }
  m_truthEvent->nTruthTauFromW = nTruthTauFromW;

  ////////////////////////////////////////////////////////////////////////////////////
  // In case you want to retrieve the full decay chain and the document line partons
  ////////////////////////////////////////////////////////////////////////////////////

  if( m_saveTruthEvtInfoFull ){
        
    m_decayHandle->loadContainers(truthParticles, truthElectrons, truthMuons, truthTaus, truthPhotons, truthNeutrinos, truthBosonWDP, truthTopWDP, truthBSMWDP, truthJets);

    // Fill if the sample is W+jets ...
    if     (ProcClassifier::IsWjets_Sherpa(m_dsid) || ProcClassifier::IsZjets_Sherpa(m_dsid) || ProcClassifier::IsGammaJets_Sherpa(m_dsid)) 
      m_decayHandle->GetDecayChain_Vjets(m_truthEvent_Vjets);

    // Fill if the sample is WW/WZ/ZZ or t+W/Z (t->bW) ...
    else if(ProcClassifier::IsVV(m_dsid) || ProcClassifier::IsWt(m_dsid) || ProcClassifier::IsZt(m_dsid))
      m_decayHandle->GetDecayChain_VV(m_truthEvent_VV);

    // Fill if the sample is VVV
    else if(ProcClassifier::IsVVV(m_dsid))
      m_decayHandle->GetDecayChain_VVV(m_truthEvent_VVV);
    
    // Fill if the sample is ttbar (+X) ...
    else if(m_truthEvent_TT)    
      m_decayHandle->GetDecayChain_TT(m_truthEvent_TT);

    // Fill if the sample is EW gaugino pair prod.
    else if(m_truthEvent_XX)    {
      m_decayHandle->GetDecayChain_XX(m_truthEvent_XX);

      // W/Z polarization correction (for ones decaying from EWKinos if the sample is not generated using MadSpin)
      // Assuming wino production & bino LSP, tan(beta)=10 and mu>0
      if(!ProcClassifier::IsXX_MadSpin(m_dsid))
	m_truthEvent->polWeight = m_bosonRwgtTool->getCombinedPolWeight(m_truthEvent_XX);
     
    }

    // Fill if the sample is gluino pair prod.
    else if(m_truthEvent_GG)    
      m_decayHandle->GetDecayChain_GG(m_truthEvent_GG);

    //
    // document line partons
    //
    std::vector<TLorentzVector> addPartons; 
    std::vector<int> addPartons_pdg; 
    m_decayHandle->RetrieveDocumentLines(addPartons, addPartons_pdg, 20.);

    m_truthEvent->partons.clear();
    for(int ip=0, n=addPartons.size(); ip<n; ++ip){
      m_truthEvent->partons.push_back(std::pair<TLorentzVector,int>(addPartons[ip],addPartons_pdg[ip]));
    }

  }

  //
  Timer::Instance()->End( "TruthObject::fillTruthEventContainer" );

}
// --------------------------------------------------------------------------------------------------------------- //
void TruthObject::fillTruthParticleContainer(const xAOD::TruthParticleContainer* truthParticles,
                                             const xAOD::JetContainer* truthJets)
{


  if( !m_truthVector ) return;

  if( !truthParticles ) {
    MsgLog::WARNING("TruthObject::fillTruthParticleContainer","Invalid truthParticle container!");
    return;
  }


  //
  for( const auto& truth_xAOD : *truthParticles ){

    // Ignore Geant4 barcode offset
    //if( truth_xAOD->barcode() > 200000 ) continue;

    bool keep = false;
    // All Status 1 particles
    if (truth_xAOD->status() == 1)
      keep = true;

    // keep all (standard) SUSY particles, DSID range based on http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
    if (truth_xAOD->absPdgId() >= 1000001 && truth_xAOD->absPdgId() <= 2000015)
      keep = true;

    //keep truth status 3 leptons for sherpa samples
    if (truth_xAOD->status() == 3 && truth_xAOD->isLepton()) {
        keep = true;
    } 

    //Find all truth 3 and truth 20 status particles 
    if (truth_xAOD->status() == 20) {
        keep = true;
    } 


    // keep only the particles that do not "decay" into themselves
    if (!keep) continue;
    for( unsigned int child=0; child<truth_xAOD->nChildren(); child++ ){
      auto c = truth_xAOD->child(child);
      if (!c) continue;
      if (abs(c->pdgId()) == abs(truth_xAOD->pdgId())){
        keep = false;
        break;
      }
    }
    if (!keep) continue;



    int parentPdgID = 0;
    int parentBarcode = 0;
    for( unsigned int par=0; par<truth_xAOD->nParents(); par++ ){
      if( !truth_xAOD->parent(par) ) continue;
      if( abs(truth_xAOD->parent(par)->pdgId())>1000001 && abs(truth_xAOD->parent(par)->pdgId())<2000040 ){
        parentPdgID   = truth_xAOD->parent(par)->pdgId();
        parentBarcode = truth_xAOD->parent(par)->barcode();
        break;
      }
    }

    //
    xAOD::TruthParticle::FourMom_t bare    = truth_xAOD->p4();
    xAOD::TruthParticle::FourMom_t dressed = truth_xAOD->p4();

    // Dress leptons only if the user requests it
    // Store this as default
    if( m_dressTruthLeptons ){
      if( abs(truth_xAOD->pdgId())>=11 && abs(truth_xAOD->pdgId())<=16 ){
        for( const auto& photon : *truthParticles ){
          // consider only status 1 photons
          if( abs(photon->pdgId())==22 && photon->status()==1  ){
            // photons close to leptons
            if(truth_xAOD->p4().DeltaR( photon->p4() ) > 0.1 ) continue;
            // only add in photons originting from leptons
            bool fromLepton = false;
            for( unsigned int par=0; par<photon->nParents(); par++ ){
              auto p = truth_xAOD->parent(par);
              if(!p) continue;
              if( abs(p->pdgId())>=11 && abs(p->pdgId())<=16 ){
                fromLepton = true;
                break;
              }
            }
            if( fromLepton ){
              dressed += photon->p4();
            }
          }
        }
      }
    }


    // Save
    TruthVariable* t = new TruthVariable();

    // Dressed lepton
    t->SetPtEtaPhiM( dressed.Pt() * m_convertFromMeV ,
                     dressed.Eta(),
                     dressed.Phi(),
                     dressed.M() * m_convertFromMeV );

  
    // Save the bare particle
    t->bareTLV.SetPtEtaPhiM( bare.Pt() * m_convertFromMeV ,
                             bare.Eta(),
                             bare.Phi(),
                             bare.M() * m_convertFromMeV );

    //std::cout << "Truth particle Pt = " << bare.Pt()*m_convertFromMeV << std::endl;

    // Charge
    t->q = truth_xAOD->e();

    // Truth particle info
    t->pdgId     = truth_xAOD->pdgId();
    t->status    = truth_xAOD->status();
    t->barcode   = truth_xAOD->barcode();

    // Decay Vtx info
    //Below lines are an EXOT5problem - replace with 0 
    //t->prodVtxPerp  = truth_xAOD->hasProdVtx() ? truth_xAOD->prodVtx()->perp() : 0.0;
    //t->decayVtxPerp = truth_xAOD->hasDecayVtx() ? truth_xAOD->decayVtx()->perp() : 0.0;

    t->prodVtxPerp  = 0.0;
    t->decayVtxPerp = 0.0;

    // Save the parent information
    t->parentPdgId   = parentPdgID;
    t->parentBarcode = parentBarcode;

    // Save
    m_truthVector->push_back(t);

  } // Loop over xAOD::TruthParticleContainer



  // Save Truth jets
  for( const auto& jet : *truthJets ){

    //if( jet->pt() < 15000.0 ) continue;

    TruthVariable* t = new TruthVariable();

    // TLV
    t->SetPtEtaPhiM( jet->pt() * m_convertFromMeV ,
                     jet->eta(),
                     jet->phi(),
                     jet->m() * m_convertFromMeV );
    //
    t->truthTLV.SetPtEtaPhiM( jet->pt() * m_convertFromMeV ,
                     jet->eta(),
                     jet->phi(),
                     jet->m() * m_convertFromMeV );

    
    
    //
    t->isAntiKt4Jet = true;

    //
    t->bjet = isTruthBJet(jet,truthParticles);

    t->pdgId = cacc_PartonTruthLabelID.isAvailable( *jet ) ? cacc_PartonTruthLabelID( *jet ) : 0;

    // Dummy
    // Would be nice to associate this
    // with the ghost matching
    t->barcode = 0;

    m_truthVector->push_back(t);

  }


}
// --------------------------------------------------------------------------------------------------------------- //
bool TruthObject::TruthFinder_tau(const xAOD::TruthParticleContainer* truthParticles,
				  const xAOD::EventInfo* eventInfo,
				  std::map<TString,TLorentzVector*>& tauTLVs)
{
  // Only for simulation
  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return true;

  // Only for samples with valid TruthParticleContainer
  if( !truthParticles ) {
    MsgLog::WARNING("TruthObject::TruthFinder_tau","Invalid truthParticle container!"); 
    return false;
  }

  TLorentzVector* hadtau = new TLorentzVector();
  TLorentzVector* etau = new TLorentzVector();
  TLorentzVector* mtau = new TLorentzVector();
  TLorentzVector* corhadtau = new TLorentzVector();

  //TLorentzVector  htau,etau,mtau,hadTauTLV,nuTLV;
  // Initialize
  hadtau->SetPxPyPzE(0.,0.,0.,0.);
  etau->SetPxPyPzE(0.,0.,0.,0.);
  mtau->SetPxPyPzE(0.,0.,0.,0.);
  corhadtau->SetPxPyPzE(0.,0.,0.,0.);

  std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > truthP_shallowCopy = xAOD::shallowCopyContainer( *truthParticles );

  xAOD::TruthParticleContainer::iterator truthP_itr  = (truthP_shallowCopy.first)->begin();
  xAOD::TruthParticleContainer::iterator truthP_end  = (truthP_shallowCopy.first)->end();

  int TauMode = -1;

  float  hadtau_pt_tmp = 0;
  float  etau_pt_tmp = 0;
  float  mtau_pt_tmp = 0;

  for( ; truthP_itr != truthP_end; ++truthP_itr ) {

    // get most energetic tau
    xAOD::TruthParticle* truth = (xAOD::TruthParticle*)(*truthP_itr);
    int pdgid   = truth->pdgId();
    int status  = truth->status();

    if( !(abs(pdgid) == 15) ) continue;
    if( !(status == 2 || status == 10902 || status==195) ) continue;//good status tau

    int n_child = 0;// number of children except tau itself
    TauMode = 0;//Not defined
    for(unsigned int i=0; i<truth->nChildren(); i++){

      int pdgid_truth = truth->child(i)->pdgId();

      if(  abs(pdgid_truth) == 15 ) continue;
      n_child++;
      TauMode = 3; // Set hadronic decay at first

      if( abs(pdgid_truth) == 11 ){//->el
	TauMode = 1;
	break;
      }
      else if( abs(pdgid_truth) == 13 ){//->mu
	TauMode = 2;
	break;
      }
    }// above looping over all children of truth.


    if(TauMode==3)//hadronic tau if no child as el or mu
      {
	if( truth->pt() < hadtau_pt_tmp ) continue;
	hadtau_pt_tmp = truth->pt();
	hadtau->SetPxPyPzE( truth->px()* m_convertFromMeV , truth->py()* m_convertFromMeV , truth->pz()* m_convertFromMeV ,truth->e()* m_convertFromMeV  );

	//// -. therefore as a fix, the visible TLV will be calculated again by subtracting the neutrino from the undecayed tau

	  for(unsigned int i=0; i<truth->nChildren(); i++){
	    int pdgid_truth = truth->child(i)->pdgId();
	    //std::cout<<"dol TruthObject:  i= "<< i<<" pdgId()= "<<truth->child(i)->pdgId()<< "  ";
	    //std::cout<<"px()= "<<truth->child(i)->px()<< "  ";
	    //std::cout<<"py()= "<<truth->child(i)->py()<< "  ";
	    //std::cout<<"pz()= "<<truth->child(i)->pz()<< "  ";
	    //std::cout<<"e()= "<<truth->child(i)->e()<< "  " << std::endl;

	    if( abs(pdgid_truth) == 16)
	      {
		TLorentzVector  TAUTLV,HADTAUTLV,NUTLV;
		TAUTLV.SetPxPyPzE( truth->px()* m_convertFromMeV , truth->py()* m_convertFromMeV , truth->pz()* m_convertFromMeV ,truth->e()* m_convertFromMeV );
		NUTLV.SetPxPyPzE( truth->child(i)->px()* m_convertFromMeV , truth->child(i)->py()* m_convertFromMeV , truth->child(i)->pz()* m_convertFromMeV ,truth->child(i)->e()* m_convertFromMeV );
		HADTAUTLV = TAUTLV - NUTLV;
		corhadtau->SetPtEtaPhiM(HADTAUTLV.Pt(), HADTAUTLV.Eta(), HADTAUTLV.Phi(), HADTAUTLV.M() );//already in GeV
	      }
	  }
      }
    if(TauMode==1)//el
      {
	if( truth->pt() < etau_pt_tmp ) continue;
	etau_pt_tmp = truth->pt();
	etau->SetPxPyPzE( truth->px()* m_convertFromMeV , truth->py()* m_convertFromMeV , truth->pz()* m_convertFromMeV ,truth->e()* m_convertFromMeV  );
      }
    if(TauMode==2)//mu
      {
	if( truth->pt() < mtau_pt_tmp ) continue;
	mtau_pt_tmp = truth->pt();
	mtau->SetPxPyPzE( truth->px()* m_convertFromMeV , truth->py()* m_convertFromMeV , truth->pz()* m_convertFromMeV ,truth->e()* m_convertFromMeV  );
      }
  }

  // Clean up
  delete truthP_shallowCopy.first;
  delete truthP_shallowCopy.second;

  // Save all the TLVs into a map
  tauTLVs.insert( std::pair<TString,TLorentzVector*>("hadtau",hadtau) );//hadronic tau
  tauTLVs.insert( std::pair<TString,TLorentzVector*>("etau",etau) );//tau->el
  tauTLVs.insert( std::pair<TString,TLorentzVector*>("mtau",mtau) );//tau->mu
  tauTLVs.insert( std::pair<TString,TLorentzVector*>("corhadtau",corhadtau) );//corrected hadronic tau
  //if the kinematic is zero, meaning not find

  return true;

}

// --------------------------------------------------------------------------------------------------------------- //
bool TruthObject::TruthFinder_tt(const xAOD::TruthParticleContainer* truthParticles,
				 const xAOD::EventInfo* eventInfo,
				 std::map<TString,TLorentzVector*>& ttbarTLVs,
				 int &pdgId11, int &pdgId12,
				 int &pdgId21, int &pdgId22,
				 int &TauMode1, int &TauMode2)
{

  // Only for simulation
  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return true;
  if(!truthParticles) return true;

  // Only for samples with valid TruthParticleContainer
  if( !truthParticles ) {
    MsgLog::WARNING("TruthObject::TruthFinder_tt","Invalid truthParticle container!");
    return false;
  }

  // Ensure we are running over the ttbar sample
  unsigned int cN = eventInfo->mcChannelNumber();
  if( cN != 110401 &&
      cN != 410000 &&
      cN != 407009 &&
      cN != 407010 &&
      cN != 407011 &&
      cN != 407012 &&
      cN != 407322 &&
      cN != 407323    ) return true;


  TLorentzVector* t1 = new TLorentzVector();
  TLorentzVector* t2 = new TLorentzVector();
  TLorentzVector* w1 = new TLorentzVector();
  TLorentzVector* w2 = new TLorentzVector();
  TLorentzVector* b1 = new TLorentzVector();
  TLorentzVector* b2 = new TLorentzVector();
  TLorentzVector* l11 = new TLorentzVector();
  TLorentzVector* l12 = new TLorentzVector();
  TLorentzVector* l21 = new TLorentzVector();
  TLorentzVector* l22 = new TLorentzVector();

  // Initialize
  t1->SetPxPyPzE(0.,0.,0.,0.);
  t2->SetPxPyPzE(0.,0.,0.,0.);
  w1->SetPxPyPzE(0.,0.,0.,0.);
  w2->SetPxPyPzE(0.,0.,0.,0.);
  b1->SetPxPyPzE(0.,0.,0.,0.);
  b2->SetPxPyPzE(0.,0.,0.,0.);
  l11->SetPxPyPzE(0.,0.,0.,0.);
  l21->SetPxPyPzE(0.,0.,0.,0.);
  l12->SetPxPyPzE(0.,0.,0.,0.);
  l22->SetPxPyPzE(0.,0.,0.,0.);
  pdgId11 = 0;
  pdgId12 = 0;
  pdgId21 = 0;
  pdgId22 = 0;
  TauMode1 = 0;
  TauMode2 = 0;

  std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > truthP_shallowCopy = xAOD::shallowCopyContainer( *truthParticles );

  xAOD::TruthParticleContainer::iterator truthP_itr  = (truthP_shallowCopy.first)->begin();
  xAOD::TruthParticleContainer::iterator truthP_end  = (truthP_shallowCopy.first)->end();

  bool get_top = false;
  bool get_topbar = false;

  xAOD::TruthParticle* top;
  xAOD::TruthParticle* topbar;
  for( ; truthP_itr != truthP_end; ++truthP_itr ) {

    if(get_top && get_topbar) break;

    xAOD::TruthParticle* truth = (xAOD::TruthParticle*)(*truthP_itr);
    int pdgid   = truth->pdgId();

    if( pdgid!=+6 && pdgid!=-6 ) continue;

    if(pdgid == +6){
      //-----------------
      // Set top
      //-----------------
      top    = truth;
      get_top = true;

      if(top->nChildren()<1){
	std::cout << "Top has no child."<< std::endl;
	return false;
      }

      // Check top children
      while(true){

	bool isFound = false;
	for(unsigned int ich = 0; ich< top->nChildren(); ich++ ){

	  int pdgid_ch_top = top->child(ich)->pdgId();

	  if( pdgid_ch_top == +6 ){
	    top = (xAOD::TruthParticle*)top->child(ich);
	    isFound = true;
	  }
	}
	if(!isFound) break;
      }
      // Set top TLV
      t1->SetPxPyPzE( top->px() * m_convertFromMeV,
                      top->py() * m_convertFromMeV,
                      top->pz() * m_convertFromMeV,
                      top->e() * m_convertFromMeV );

      //-----------------
      // Set W and b
      //-----------------
      int ich_w_t = -1; int ich_b_t = -1;
      for(unsigned int ich=0 ; ich< top->nChildren(); ich++){
	int pdgid = top->child(ich)->pdgId();
	if(pdgid == +24) ich_w_t = ich;
	if( pdgid ==  +5 || pdgid == +3 || pdgid == +1 ) ich_b_t = ich;
      }

      if( ich_w_t<0 || ich_b_t<0){
	std::cout<<"Error! Cannot find W,b " << std::endl;
	return false;
      }

      // Set W and b TLV
      xAOD::TruthParticle* w_t = (xAOD::TruthParticle*)top->child(ich_w_t);
      xAOD::TruthParticle* b_t = (xAOD::TruthParticle*)top->child(ich_b_t);

      w1->SetPxPyPzE( w_t->px() * m_convertFromMeV,
		      w_t->py() * m_convertFromMeV,
		      w_t->pz() * m_convertFromMeV,
		      w_t->e() * m_convertFromMeV );

      b1->SetPxPyPzE( b_t->px() * m_convertFromMeV,
		      b_t->py() * m_convertFromMeV,
		      b_t->pz() * m_convertFromMeV,
		      b_t->e() * m_convertFromMeV );

      //---------------------------------
      // Check W children
      //---------------------------------
      if(w_t->nChildren()<1) std::cout << "W has no child."<< std::endl;
      while(true){

	bool isFound = false;
	for(unsigned int ich = 0; ich< w_t->nChildren(); ich++ ){
	  int pdgid_ch_w_t = w_t->child(ich)->pdgId();

	  if( pdgid_ch_w_t == +24 ){
	    w_t = (xAOD::TruthParticle*)w_t->child(ich);
	    isFound = true;
	  }
	}
	if(!isFound) break;
      }

      //----------------------------------
      // Identify W decay
      //----------------------------------
      int index_lep_t    = -1; int index_nu_t     = -1;
      int index_q_t      = -1; int index_qbar_t   = -1;
      for(unsigned int ich=0; ich < w_t->nChildren(); ich++){
	int pdgid = w_t->child(ich)->pdgId();
	if( pdgid == -11 || pdgid == -13 || pdgid == -15 ) index_lep_t = ich;
	if( pdgid == +12 || pdgid == +14 || pdgid == +16 ) index_nu_t  = ich;
	if( pdgid ==  +2 || pdgid ==  +4 || pdgid ==  +6 ) index_q_t   = ich;
	if( pdgid ==  -1 || pdgid ==  -3 || pdgid ==  -5 ) index_qbar_t= ich;
      }

      int decaymode_t    = 0;
      if( index_lep_t    >= 0 && index_nu_t      >= 0 ) decaymode_t    = +1; //leptonic decay
      if( index_q_t      >= 0 && index_qbar_t    >= 0 ) decaymode_t    = -1; //hadronic decay

      if( decaymode_t == 0 ){

	std::cout<<"Error! cannot decide decay mode of W+"<< std::endl;
	std::cout<<index_lep_t<<" "<<index_nu_t<<" "<<index_q_t<<" "<<index_qbar_t <<std::endl;

	return false;
      }

      if(index_lep_t>=0){
	l11->SetPxPyPzE( w_t->child(index_lep_t)->px() * m_convertFromMeV,
			 w_t->child(index_lep_t)->py() * m_convertFromMeV,
			 w_t->child(index_lep_t)->pz() * m_convertFromMeV,
			 w_t->child(index_lep_t)->e() * m_convertFromMeV );

	l12->SetPxPyPzE( w_t->child(index_nu_t)->px() * m_convertFromMeV,
			 w_t->child(index_nu_t)->py() * m_convertFromMeV,
			 w_t->child(index_nu_t)->pz() * m_convertFromMeV,
			 w_t->child(index_nu_t)->e()  * m_convertFromMeV );

	pdgId11 = w_t->child(index_lep_t)->pdgId();
	pdgId12 = w_t->child(index_nu_t)->pdgId();
      }
      else if(index_q_t>=0){
	l11->SetPxPyPzE( w_t->child(index_q_t)->px() * m_convertFromMeV,
			 w_t->child(index_q_t)->py() * m_convertFromMeV,
			 w_t->child(index_q_t)->pz() * m_convertFromMeV,
			 w_t->child(index_q_t)->e() * m_convertFromMeV );

	l12->SetPxPyPzE( w_t->child(index_qbar_t)->px() * m_convertFromMeV,
			 w_t->child(index_qbar_t)->py() * m_convertFromMeV,
			 w_t->child(index_qbar_t)->pz() * m_convertFromMeV,
			 w_t->child(index_qbar_t)->e() * m_convertFromMeV );

	pdgId11 = w_t->child(index_q_t)->pdgId();
	pdgId12 = w_t->child(index_qbar_t)->pdgId();
      }
      else{
	std::cout<<"Impossible top decay"<<std::endl;
	return false;
      }
      // Set W children
      xAOD::TruthParticle* ch11_w_t = (xAOD::TruthParticle*)w_t->child( (index_lep_t>=0) ? index_lep_t : index_q_t);
      //xAOD::TruthParticle* ch12_w_t = (xAOD::TruthParticle*)w_t->child( (index_nu_t>=0) ? index_nu_t : index_qbar_t);

      //------------------------------
      // Identify Tau- decay mode
      //------------------------------
      if(pdgId11 == -15){

	bool tauOK = false;
	index_lep_t = -1;
	xAOD::TruthParticleContainer::iterator truthP2_itr  = (truthP_shallowCopy.first)->begin();
	xAOD::TruthParticleContainer::iterator truthP2_end  = (truthP_shallowCopy.first)->end();

	// Get tau with status=2
	for( ; truthP2_itr != truthP2_end; ++truthP2_itr ) {
	  xAOD::TruthParticle* truthP2 = (xAOD::TruthParticle*)(*truthP2_itr);

	  int status = truthP2->status();
	  int pdgId  = truthP2->pdgId();

	  bool isGood = false;
	  if( status == 2 || status == 10902) isGood = true; // T.Saito
	  //	  if( status == 2 ) isGood = true;
	  if( pdgId==-15 and isGood ){
	    tauOK = true;
	    ch11_w_t = truthP2;
	    break;
	  }

	}
	if(!tauOK){
	  std::cout<<"Cannot find Tau-"<<std::endl;
	  return false;
	}
      }

      if(pdgId11 == -15){
	int pdgid_lep_t    =  ch11_w_t->pdgId();
	while(true){
	  bool isFound = false;
	  for(unsigned int ich = 0; ich<ch11_w_t->nChildren(); ich++ ){

	    int pdgid_ch_ch11_w_t = ch11_w_t->child(ich)->pdgId();
	    if( pdgid_ch_ch11_w_t == pdgid_lep_t ){
	      ch11_w_t = (xAOD::TruthParticle*)ch11_w_t->child(ich);
	      isFound = true;
	    }
	  }
	  if(!isFound) break;
	}

	TauMode1 = 3; // Set hadronic decay at first

	for(unsigned int i=0; i<ch11_w_t->nChildren(); i++){

	  if( abs(ch11_w_t->pdgId()) == 24 ){
	    // This is W.
	    int myPdgId  = ch11_w_t->pdgId();
	    while(true){
	      bool isFound = false;
	      for(unsigned int ich = 0; ich<ch11_w_t->nChildren(); ich++ ){
		int pdgid_ch11_w_t = ch11_w_t->child(ich)->pdgId();
		if( pdgid_ch11_w_t == myPdgId ){
		  ch11_w_t = (xAOD::TruthParticle*)ch11_w_t->child(ich);
		  isFound = true;
		}
	      }
	      if(!isFound) break;
	    }
	    break;
	  }
	}
	for(unsigned int i=0; i<ch11_w_t->nChildren(); i++){

	  int pdgid_ch11_w_t = ch11_w_t->child(i)->pdgId();

	  if( abs(pdgid_ch11_w_t) == 11 ){
	    TauMode1 = 1;
	    break;
	  }
	  else if( abs(ch11_w_t->child(i)->pdgId()) == 13 ){
	    TauMode1 = 2;
	    break;
	  }
	}
      }
      else{
	TauMode1 = 0;
      }
    }
    else if(pdgid==-6){
      //-----------------
      // Set topbar
      //-----------------
      topbar = truth;
      get_topbar = true;

      if(topbar->nChildren()<1){
	std::cout << "Top has no child."<< std::endl;
	return false;
      }

      while(true){

	bool isFound = false;
	for(unsigned int ich = 0; ich< topbar->nChildren(); ich++ ){

	  int pdgid_ch_topbar = topbar->child(ich)->pdgId();

	  if( pdgid_ch_topbar == +6 ){
	    topbar = (xAOD::TruthParticle*)topbar->child(ich);
	    isFound = true;
	  }
	}
	if(!isFound) break;
      }

      t2->SetPxPyPzE( topbar->px() * m_convertFromMeV,
                      topbar->py() * m_convertFromMeV,
                      topbar->pz() * m_convertFromMeV,
                      topbar->e() * m_convertFromMeV );

      //-----------------
      // Set W and b
      //-----------------
      int ich_w_tbar = -1; int ich_b_tbar = -1;
      for(unsigned int ich=0 ; ich< topbar->nChildren(); ich++){
	int pdgid = topbar->child(ich)->pdgId();
	if(pdgid == -24) ich_w_tbar = ich;
	if( pdgid ==  -5 || pdgid == -3 || pdgid == -1 ) ich_b_tbar = ich;
      }

      if( ich_w_tbar<0 || ich_b_tbar<0){
	std::cout<<"Error! Cannot find W,b " << std::endl;
	return false;
      }

      // Set W and b TLV
      xAOD::TruthParticle* w_tbar = (xAOD::TruthParticle*)topbar->child(ich_w_tbar);
      xAOD::TruthParticle* b_tbar = (xAOD::TruthParticle*)topbar->child(ich_b_tbar);

      w2->SetPxPyPzE( w_tbar->px() * m_convertFromMeV,
		      w_tbar->py() * m_convertFromMeV,
		      w_tbar->pz() * m_convertFromMeV,
		      w_tbar->e() * m_convertFromMeV );

      b2->SetPxPyPzE( b_tbar->px() * m_convertFromMeV,
		      b_tbar->py() * m_convertFromMeV,
		      b_tbar->pz() * m_convertFromMeV,
		      b_tbar->e() * m_convertFromMeV );

      //---------------------------------
      // Check W children
      //---------------------------------
      if(w_tbar->nChildren()<1) std::cout << "W has no child."<< std::endl;
      while(true){

	bool isFound = false;
	for(unsigned int ich = 0; ich< w_tbar->nChildren(); ich++ ){
	  int pdgid_ch_w_tbar = w_tbar->child(ich)->pdgId();

	  if( pdgid_ch_w_tbar == +24 ){
	    w_tbar = (xAOD::TruthParticle*)w_tbar->child(ich);
	    isFound = true;
	  }
	}
	if(!isFound) break;
      }

      //----------------------------------
      // Identify W decay
      //----------------------------------
      int index_lep_tbar    = -1; int index_nu_tbar     = -1;
      int index_q_tbar      = -1; int index_qbar_tbar   = -1;
      for(unsigned int ich=0; ich < w_tbar->nChildren(); ich++){
	int pdgid = w_tbar->child(ich)->pdgId();
	if( pdgid == +11 || pdgid == +13 || pdgid == +15 ) index_lep_tbar = ich;
	if( pdgid == -12 || pdgid == -14 || pdgid == -16 ) index_nu_tbar  = ich;
	if( pdgid ==  -2 || pdgid ==  -4 || pdgid ==  -6 ) index_q_tbar   = ich;
	if( pdgid ==  +1 || pdgid ==  +3 || pdgid ==  +5 ) index_qbar_tbar= ich;
      }

      int decaymode_tbar    = 0;
      if( index_lep_tbar    >= 0 && index_nu_tbar      >= 0 ) decaymode_tbar    = +1; //leptonic decay
      if( index_q_tbar      >= 0 && index_qbar_tbar    >= 0 ) decaymode_tbar    = -1; //hadronic decay

      if( decaymode_tbar == 0 ){

	std::cout<<"Error! cannot decide decay mode of W+,W-"<< std::endl;
	std::cout<<index_lep_tbar<<" "<<index_nu_tbar<<" "<<index_q_tbar<<" "<<index_qbar_tbar <<std::endl;

	return false;
      }

      if(index_lep_tbar>=0){
	l21->SetPxPyPzE( w_tbar->child(index_lep_tbar)->px() * m_convertFromMeV,
			 w_tbar->child(index_lep_tbar)->py() * m_convertFromMeV,
			 w_tbar->child(index_lep_tbar)->pz() * m_convertFromMeV,
			 w_tbar->child(index_lep_tbar)->e() * m_convertFromMeV );

	l22->SetPxPyPzE( w_tbar->child(index_nu_tbar)->px() * m_convertFromMeV,
			 w_tbar->child(index_nu_tbar)->py() * m_convertFromMeV,
			 w_tbar->child(index_nu_tbar)->pz() * m_convertFromMeV,
			 w_tbar->child(index_nu_tbar)->e() * m_convertFromMeV );

	pdgId21 = w_tbar->child(index_lep_tbar)->pdgId();
	pdgId22 = w_tbar->child(index_nu_tbar)->pdgId();

      }
      else if(index_q_tbar>=0){
	l21->SetPxPyPzE( w_tbar->child(index_q_tbar)->px() * m_convertFromMeV,
			 w_tbar->child(index_q_tbar)->py() * m_convertFromMeV,
			 w_tbar->child(index_q_tbar)->pz() * m_convertFromMeV,
			 w_tbar->child(index_q_tbar)->e() * m_convertFromMeV );

	l22->SetPxPyPzE( w_tbar->child(index_qbar_tbar)->px() * m_convertFromMeV,
			 w_tbar->child(index_qbar_tbar)->py() * m_convertFromMeV,
			 w_tbar->child(index_qbar_tbar)->pz() * m_convertFromMeV,
			 w_tbar->child(index_qbar_tbar)->e() * m_convertFromMeV );

	pdgId21 = w_tbar->child(index_q_tbar)->pdgId();
	pdgId22 = w_tbar->child(index_qbar_tbar)->pdgId();
      }
      else{
	std::cout<<"Impossible topbar decay"<<std::endl;
	return false;
      }
      xAOD::TruthParticle* ch21_w_tbar = (xAOD::TruthParticle*)w_tbar->child( (index_lep_tbar>=0) ? index_lep_tbar : index_q_tbar);
      //xAOD::TruthParticle* ch22_w_tbar = (xAOD::TruthParticle*)w_tbar->child( (index_nu_tbar>=0) ? index_nu_tbar : index_qbar_tbar);

      //------------------------------
      // Identify Tau+ decay mode
      //------------------------------
      if(pdgId21 == +15){
	bool tauOK = false;

	xAOD::TruthParticleContainer::iterator truthP2_itr  = (truthP_shallowCopy.first)->begin();
	xAOD::TruthParticleContainer::iterator truthP2_end  = (truthP_shallowCopy.first)->end();
	for( ; truthP2_itr != truthP2_end; ++truthP2_itr ) {
	  xAOD::TruthParticle* truthP2 = (xAOD::TruthParticle*)(*truthP2_itr);

	  int status = truthP2->status();
	  int pdgId  = truthP2->pdgId();

	  bool isGood = false;
	  if( status == 2 || status == 10902) isGood = true; // T.Saito
	  //	  if( status == 2 ) isGood = true;
	  if( pdgId==+15 and isGood ){
	    tauOK = true;
	    ch21_w_tbar = truthP2;
	    break;
	  }

	}
	if(!tauOK){
	  std::cout<<"Cannot find Tau+"<<std::endl;
	  return false;
	}
      }

      if(pdgId21 == +15){
	int pdgid_lep_tbar    =  ch21_w_tbar->pdgId();
	while(true){
	  bool isFound = false;
	  for(unsigned int ich = 0; ich<ch21_w_tbar->nChildren(); ich++ ){

	    int pdgid_ch_ch21_w_tbar = ch21_w_tbar->child(ich)->pdgId();
	    if( pdgid_ch_ch21_w_tbar == pdgid_lep_tbar ){
	      ch21_w_tbar = (xAOD::TruthParticle*)ch21_w_tbar->child(ich);
	      isFound = true;
	    }
	  }
	  if(!isFound) break;
	}

	TauMode2 = 3; // Set hadronic decay at first

	for(unsigned int i=0; i<ch21_w_tbar->nChildren(); i++){

	  if( abs(ch21_w_tbar->pdgId()) == 24 ){
	    // This is W.
	    int myPdgId  = ch21_w_tbar->pdgId();
	    while(true){
	      bool isFound = false;
	      for(unsigned int ich = 0; ich<ch21_w_tbar->nChildren(); ich++ ){
		int pdgid_ch21_w_tbar = ch21_w_tbar->child(ich)->pdgId();
		if( pdgid_ch21_w_tbar == myPdgId ){
		  ch21_w_tbar = (xAOD::TruthParticle*)ch21_w_tbar->child(ich);
		  isFound = true;
		}
	      }
	      if(!isFound) break;
	    }
	    break;
	  }
	}
	for(unsigned int i=0; i<ch21_w_tbar->nChildren(); i++){

	  int pdgid_ch21_w_tbar = ch21_w_tbar->child(i)->pdgId();

	  if( abs(pdgid_ch21_w_tbar) == 11 ){
	    TauMode2 = 1;
	    break;
	  }
	  else if( abs(pdgid_ch21_w_tbar) == 13 ){
	    TauMode2 = 2;
	    break;
	  }
	}
      }
      else{
	TauMode2 = 0;
      }

    }

  }


  if(!get_top || !get_topbar){
    std::cout<<"Could not find ttbar."<<std::endl;
    return false;
  }
  //  std::cout<<"pdgId11 = "<< pdgId11 << ", pdgId12 = " << pdgId12 << ", pdgId21 = "<< pdgId21 << ", pdgId22 = "<< pdgId22<< ", TauMode1 = "<< TauMode1 << ", TauMode2 = "<< TauMode2 <<std::endl;

  // Clean up
  delete truthP_shallowCopy.first;
  delete truthP_shallowCopy.second;

  // Save all the TLVs into a map
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("top",t1) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("antitop",t2) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("w1",w1) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("w2",w2) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("b1",b1) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("b2",b2) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("l11",l11) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("l12",l12) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("l21",l21) );
  ttbarTLVs.insert( std::pair<TString,TLorentzVector*>("l22",l22) );

  return true;

}
// --------------------------------------------------------------------------------------------------------------- //
TruthEvent::TTbarDecayMode TruthObject::GetTTbarMode(int pdgId11,int pdgId21,int TauMode1,int TauMode2)
{

  TruthEvent::TTbarDecayMode DecayMode = TruthEvent::TTbarDecayMode::NONE;

  int nLep=0;
  int nHad=0;
  int nTauhad=0;
  int nTaulep=0;
  //
  CountMode(pdgId11, TauMode1, nLep, nHad, nTaulep, nTauhad);
  CountMode(pdgId21, TauMode2, nLep, nHad, nTaulep, nTauhad);
  //
  if     (nLep==2)                  DecayMode = TruthEvent::TTbarDecayMode::LEPLEP;
  else if(nLep==1    && nTaulep==1) DecayMode = TruthEvent::TTbarDecayMode::LEPLTAU;
  else if(nTaulep==2              ) DecayMode = TruthEvent::TTbarDecayMode::LTAULTAU;
  else if(nLep==1    && nHad==1   ) DecayMode = TruthEvent::TTbarDecayMode::LEPHAD;
  else if(nLep==1    && nTauhad==1) DecayMode = TruthEvent::TTbarDecayMode::LEPHTAU;
  else if(nTaulep==1 && nHad==1   ) DecayMode = TruthEvent::TTbarDecayMode::LTAUHAD;
  else if(nTaulep==1 && nTauhad==1) DecayMode = TruthEvent::TTbarDecayMode::LTAUHTAU;
  else if(nHad==2                 ) DecayMode = TruthEvent::TTbarDecayMode::HADHAD;
  else if(nHad==1    && nTauhad==1) DecayMode = TruthEvent::TTbarDecayMode::HADHTAU;
  else if(nTauhad==2              ) DecayMode = TruthEvent::TTbarDecayMode::HTAUHTAU;
  else{
    DecayMode = TruthEvent::TTbarDecayMode::UNKNOWN;
    std::cout << "<TruthObject::GetTTbarMode> WARNING: Imposible TTbar Decay " << std::endl;
  }

  return DecayMode;

}
// --------------------------------------------------------------------------------------------------------------- //
void TruthObject::CountMode(int pdgId, int TauMode, int& nLep, int& nHad, int& nTaulep, int& nTauhad)
{

  if(abs(pdgId)==11 || abs(pdgId)==13) nLep++;
  else if(abs(pdgId)==15){
    if(TauMode==1 || TauMode==2) nTaulep++;
    else if(TauMode==3) nTauhad++;
    else std::cout << "TauMode is Inconsistent with pdgId" << std::endl;
  }
  else nHad++;

}
// -------------------------------------------------------------------- //
double TruthObject::getN2TruthMll(const xAOD::TruthParticleContainer* truthP, const xAOD::EventInfo* eventInfo)
{

  double truthMll = -999999;

  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return truthMll;

  // Only for samples with valid TruthParticleContainer
  if( !truthP ) {
    MsgLog::WARNING("TruthObject::getN2TruthMll","Invalid truthParticle container!");
    return truthMll;
  }

  const xAOD::TruthParticle* lep1(0);
  const xAOD::TruthParticle* lep2(0);

  for (const auto& tp : *truthP) {

    // Check if the particle is an N2 or a Z.
    // Note! For some reason, above around mll = 53.8 GeV,
    // the Z is classified as the parent of the leptons,
    // while below that, the N2 is classified as the parent
    if( tp->absPdgId() != 1000023 && tp->absPdgId() != 23){
      continue;
    }

    unsigned int nChildren = tp->nChildren();

    for(unsigned int iChild = 0; iChild < nChildren; ++iChild){

      const xAOD::TruthParticle* child = tp->child(iChild);
      int pdgIDChild = child->absPdgId();

      if(pdgIDChild != 11 && pdgIDChild != 13 && pdgIDChild != 15){
        continue;
      }

      if     (!lep1) lep1 = child;
      else if(!lep2) lep2 = child;
      else           break;
    }
  }

  // this block will only be entered if two leptons with an N2 or Z as parent
  // are present in the event
  //
  // Note that the case of more than one N2 or Z in the event is not handled here!
  if(lep1 && lep2){
    truthMll = (lep1->p4()+lep2->p4()).M();
  }

  return truthMll; // note this is still in MeV!
}
// -------------------------------------------------------------------- //
bool TruthObject::isTruthBJet(const xAOD::Jet* jet,const xAOD::TruthParticleContainer* truthParticles)
{

  // Only for samples with valid TruthParticleContainer
  if( !truthParticles ) {
    MsgLog::WARNING("TruthObject::isTruthBJet","Invalid truthParticle container!");
    return false;
  }

  //
  // Loop over truth particle container
  // and check if the jet is matched to
  // any b-hadron within a cone of m_deltaRJetBHadron

  for( const auto& truthP : *truthParticles ){

    // B-ttom hadron requirement first
    if( !truthP->isBottomHadron() ) continue;

    // DeltaR matching between truth jet and bottom hadron
    if( xAOD::P4Helpers::deltaR( jet,truthP ) > m_deltaRJetBHadron ) continue;

    //
    return true;

  }

  // Means we didn't find anything
  return false;


}
// -------------------------------------------------------------------- //
void TruthObject::Reset()
{

  // Clear the truth vector
  // And free up the memory
  if( m_truthVector ){
    for( auto truthP : *m_truthVector ){
      delete truthP;
    }
    m_truthVector->clear();
  }

  // Truth event
  if( m_truthEvent ){

    // Reset decay mode to NONE
    m_truthEvent->decayMode = TruthEvent::TTbarDecayMode::NONE;

    // Clear vectors
    m_truthEvent->Wdecays.clear();
    m_truthEvent->Zdecays.clear();

    // Clean up memory
    for(std::map<TString,TLorentzVector*>::iterator itr = m_truthEvent->TTbarTLVs.begin(); itr != m_truthEvent->TTbarTLVs.end(); itr++){
      delete itr->second;
    }
    m_truthEvent->TTbarTLVs.clear();

    for(std::map<TString,TLorentzVector*>::iterator itr = m_truthEvent->TauTLVs.begin(); itr != m_truthEvent->TauTLVs.end(); itr++){
      delete itr->second;
    }
    m_truthEvent->TauTLVs.clear();

    m_truthEvent->truthFatjets.clear();
    m_truthEvent->partons.clear();
  }

  // Clear truth decay containers
  if(m_truthEvent_Vjets){ m_truthEvent_Vjets->clear();  }
  if(m_truthEvent_VV)   { m_truthEvent_VV->clear();     }
  if(m_truthEvent_VVV)  { m_truthEvent_VVV->clear();     }
  if(m_truthEvent_TT)   { m_truthEvent_TT->clear();     }
  if(m_truthEvent_GG)   { m_truthEvent_GG->clear();     }
  if(m_truthEvent_XX)   { m_truthEvent_XX->clear();     }



}
// -------------------------------------------------------------------- //
const TruthVector* TruthObject::getObj(TString sysName)
{

  if(sysName == "") return m_truthVector;
  else return NULL;

}
// -------------------------------------------------------------------- //
const TruthEvent* TruthObject::getTruthEvent(TString sysName)
{

  if(sysName == "") return m_truthEvent;
  else return NULL;

}
// -------------------------------------------------------------------- //
const TruthEvent_Vjets* TruthObject::getTruthEvent_Vjets(TString sysName)
{

  if(sysName == "") return m_truthEvent_Vjets;
  else return NULL;

}

// -------------------------------------------------------------------- //
const TruthEvent_VV* TruthObject::getTruthEvent_VV(TString sysName)
{

  if(sysName == "") return m_truthEvent_VV;
  else return NULL;

}
// -------------------------------------------------------------------- //
const TruthEvent_VVV* TruthObject::getTruthEvent_VVV(TString sysName)
{

  if(sysName == "") return m_truthEvent_VVV;
  else return NULL;

}
// -------------------------------------------------------------------- //
const TruthEvent_TT* TruthObject::getTruthEvent_TT(TString sysName)
{

  if(sysName == "") return m_truthEvent_TT;
  else return NULL;

}
// -------------------------------------------------------------------- //
const TruthEvent_XX* TruthObject::getTruthEvent_XX(TString sysName)
{

  if(sysName == "") return m_truthEvent_XX;
  else return NULL;

}
// -------------------------------------------------------------------- //
const TruthEvent_GG* TruthObject::getTruthEvent_GG(TString sysName)
{

  if(sysName == "") return m_truthEvent_GG;
  else return NULL;

}
// -------------------------------------------------------------------- //
TruthObject::~TruthObject()
{
  this->Reset();
  if(m_decayHandle)          delete m_decayHandle;
  if(m_truthEvent_Vjets)     delete m_truthEvent_Vjets;
  if(m_truthEvent_VV)        delete m_truthEvent_VV;
  if(m_truthEvent_VVV)       delete m_truthEvent_VVV;
  if(m_truthEvent_TT)        delete m_truthEvent_TT;
  if(m_truthEvent_XX)        delete m_truthEvent_XX;
  if(m_truthEvent_GG)        delete m_truthEvent_GG;
  if(m_bosonRwgtTool)        delete m_bosonRwgtTool;
}

bool TruthObject::isSherpaZllMC(const xAOD::EventInfo* eventInfo, int& pdgId){

  unsigned int datasetID = eventInfo->mcChannelNumber();

 
  if( (datasetID >= 364114) && (datasetID <= 364127) ){ // mass-inclusive Zee
    pdgId = 11;
    return true;
  }

  if( (datasetID >= 366309) && (datasetID <= 366315) ){ // mass-filtered Zee
    pdgId = 11;
    return true;
  }

  if( (datasetID >= 364100) && (datasetID <= 364113) ){ // mass-inclusive Zmumu
    pdgId = 13;
    return true;
  }

  if( (datasetID >= 366300) && (datasetID <= 366308) ){ // mass-filtered Zmumu
    pdgId = 13;
    return true;
  }

  if( (datasetID >= 364128) && (datasetID <= 364141) ){ // mass-inclusive Ztautau
    pdgId = 15;
    return true;
  }

  if( ((datasetID >= 700452) && (datasetID <= 700454)) || ((datasetID >= 700636) && (datasetID <= 700638))  ){ // mass-enhanced Zee, Sherpa 2.2.11
    pdgId = 11;
    return true;
  }

  if( ((datasetID >= 700455) && (datasetID <= 700457)) || ((datasetID >= 700639) && (datasetID <= 700641)) ){ // mass-enhanced Zmumu, Sherpa 2.2.11
    pdgId = 13;
    return true;
  }

  if( ((datasetID >= 700458) && (datasetID <= 700460)) || ((datasetID >= 700642) && (datasetID <= 700644))  ){ // mass-enhanced Ztautau, Sherpa 2.2.11
    pdgId = 15;
    return true;
  }

  if( ((datasetID >= 700320) && (datasetID <= 700322)) || ((datasetID >= 700615) && (datasetID <= 700617)) ){ // mass-inclusive Zee, Sherpa 2.2.11
    pdgId = 11;
    return true;
  }

  if( ((datasetID >= 700323) && (datasetID <= 700325)) || ((datasetID >= 700618) && (datasetID <= 700620))){ // mass-inclusive Zmumu, Sherpa 2.2.11
    pdgId = 13;
    return true;
  }

    if( ((datasetID >= 700326) && (datasetID <= 700328)) || ((datasetID >= 700621) && (datasetID <= 700623))){ // mass-inclusive Ztautau, Sherpa 2.2.11
    pdgId = 15;
    return true;
  }

  if( (datasetID >= 700467) && (datasetID <= 700469) ){ // low-mass Zee, Sherpa 2.2.11
    pdgId = 11;
    return true;
  }

  if( (datasetID >= 700470) && (datasetID <= 700472) ){ // low-mass Zmumu, Sherpa 2.2.11
    pdgId = 13;
    return true;
  }

  if( (datasetID >= 700473) && (datasetID <= 700475) ){ // low-mass Ztautau, Sherpa 2.2.11
    pdgId = 15;
    return true;
  }

  return false;
}

