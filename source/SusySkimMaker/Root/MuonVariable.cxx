#include "SusySkimMaker/MuonVariable.h"

MuonVariable::MuonVariable()
{

  setDefault(etcone30,0.0);
  setDefault(quality,-1);
  setDefault(lowPtWp,false);
  setDefault(bad,false);
  setDefault(cosmic,false);
  setDefault(nPrecLayers,-1);
  setDefault(nPrecHoleLayers,-1);
  setDefault(nGoodPrecLayers,-1);
  setDefault(idPt,-1.0);
  setDefault(idTheta,-99.0);
  setDefault(mePt,-1.0);
  setDefault(meTheta,-99.0);
  setDefault(idQoverPErr,-1.0);
  setDefault(meQoverPErr,-1.0);
  setDefault(cbQoverPErr,-1.0);
  setDefault(matchQ,-1.0);
  setDefault(ParamEnergyLoss,-1.0);
  setDefault(MeasEnergyLoss,-1.0);

}
// -------------------------------------------------------------------------------- //
MuonVariable::MuonVariable(const MuonVariable &rhs):
  LeptonVariable(rhs),
  etcone30(rhs.etcone30),
  quality(rhs.quality),
  lowPtWp(rhs.lowPtWp),
  bad(rhs.bad),
  cosmic(rhs.cosmic),
  nPrecLayers(rhs.nPrecLayers),
  nPrecHoleLayers(rhs.nPrecLayers),
  nGoodPrecLayers(rhs.nGoodPrecLayers),
  idPt(rhs.idPt),
  idTheta(rhs.idTheta),
  mePt(rhs.mePt),
  meTheta(rhs.meTheta),
  idQoverPErr(rhs.idQoverPErr),
  meQoverPErr(rhs.meQoverPErr),
  cbQoverPErr(rhs.cbQoverPErr), 
  matchQ(rhs.matchQ),
  ParamEnergyLoss(rhs.ParamEnergyLoss),
  MeasEnergyLoss(rhs.MeasEnergyLoss)
{

}
// -------------------------------------------------------------------------------- //
MuonVariable& MuonVariable::operator=(const MuonVariable &rhs)
{
  if (this != &rhs) {
    LeptonVariable::operator=(rhs);
    etcone30    = rhs.etcone30;
    quality     = rhs.quality;
    lowPtWp     = rhs.lowPtWp;
    bad         = rhs.bad;
    cosmic         = rhs.cosmic;
    nPrecLayers = rhs.nPrecLayers;
    nPrecHoleLayers = rhs.nPrecLayers;
    nGoodPrecLayers = rhs.nGoodPrecLayers;
    idPt = rhs.idPt;
    idTheta = rhs.idTheta;
    mePt = rhs.mePt;
    meTheta = rhs.meTheta;
    idQoverPErr = rhs.idQoverPErr;
    meQoverPErr = rhs.meQoverPErr;
    cbQoverPErr = rhs.cbQoverPErr;
    matchQ      = rhs.matchQ;
    ParamEnergyLoss = rhs.ParamEnergyLoss;
    MeasEnergyLoss = rhs.MeasEnergyLoss;
  }
  return *this;
}
// -------------------------------------------------------------------------------- //
