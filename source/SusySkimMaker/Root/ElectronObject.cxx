#include "SusySkimMaker/ElectronObject.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/Constants.h"
#include "SusySkimMaker/TrackObject.h"
#include "SusySkimMaker/Timer.h"

#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "SusySkimMaker/StatusCodeCheck.h"

ElectronObject::ElectronObject() : m_convertFromMeV(1.00), 
                                   m_elec_llhveryloose(0),
                                   m_elec_llhloose(0),
                                   m_elec_llhlooseBL(0),
                                   m_elec_llhmedium(0),
                                   m_elec_llhtight(0),
				   m_isData(false),
				   m_baseObject(0),
                                   m_trackObject(0),
                                   m_writeDetailedSkim(false)
{
  m_electronVectorMap.clear();
}
// ------------------------------------------------------------------------- //
StatusCode ElectronObject::init(TreeMaker*& treeMaker, bool isData)
{

  const char* APP_NAME = "ElectronObject";

  m_isData = isData;

  for( auto& sysName : treeMaker->getSysVector() ){
    ElectronVector* ele  = new ElectronVector();
    // Save electron vector into map
    m_electronVectorMap.insert( std::pair<TString,ElectronVector*>(sysName,ele) );
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      Info("ElectronObject::init","Adding a branch electrons to skim tree: %s ", sysTree->GetName() );
      std::map<TString,ElectronVector*>::iterator eleItr = m_electronVectorMap.find(sysName);
      sysTree->Branch("electrons",&eleItr->second);
    }
  }

  //
  CHECK( init_tools() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode ElectronObject::init_tools()
{

  const char* APP_NAME = "ElectronObject";

  // Fields from DB
  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);
  CentralDB::retrieve(CentralDBFields::WRITEDETAILEDSKIM,m_writeDetailedSkim);

  // Electron selector tools to get medium and tight
  m_elec_llhtight = new AsgElectronLikelihoodTool("ElectronObject_EleSelLikelihood_TIGHT");
  CHECK( m_elec_llhtight->setProperty("WorkingPoint", "TightLHElectron") );
  CHECK( m_elec_llhtight->initialize() );
 
  m_elec_llhmedium = new AsgElectronLikelihoodTool("ElectronObject_EleSelLikelihood_MEDIUM");
  CHECK( m_elec_llhmedium->setProperty("WorkingPoint", "MediumLHElectron") );
  CHECK( m_elec_llhmedium->initialize() );

  m_elec_llhlooseBL = new AsgElectronLikelihoodTool("ElectronObject_EleSelLikelihood_LOOSEBL");
  CHECK( m_elec_llhlooseBL->setProperty("WorkingPoint", "LooseBLLHElectron") );
  CHECK( m_elec_llhlooseBL->initialize() );

  m_elec_llhloose = new AsgElectronLikelihoodTool("ElectronObject_EleSelLikelihood_LOOSE");
  CHECK( m_elec_llhloose->setProperty("WorkingPoint", "LooseLHElectron") );
  CHECK( m_elec_llhloose->initialize() );
 
  m_elec_llhveryloose = new AsgElectronLikelihoodTool("ElectronObject_EleSelLikelihood_VERYLOOSE");
  CHECK( m_elec_llhveryloose->setProperty("WorkingPoint", "VeryLooseLHElectron") );
  CHECK( m_elec_llhveryloose->initialize() );

  // Isolation tools
  m_baseObject = new BaseObject();
  CHECK( m_baseObject->initialize("ElectronWP") );

  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
void ElectronObject::fillElectronContainer(xAOD::Electron* ele_xAOD, 
                                           const xAOD::VertexContainer* primVertex,
                                           const xAOD::EventInfo* eventInfo,
                                           const std::map<TString,bool> trigMap,
                                           const std::map<TString,float> recoEffSF,
                                           const MCCorrContainer* trigEff,
                                           std::string sys_name)
{

  Timer::Instance()->Start( "ElectronObject::fillElectronContainer" );

  std::map<TString,ElectronVector*>::iterator it = m_electronVectorMap.find(sys_name);

  if( it==m_electronVectorMap.end() ){
    std::cout << "<ElectronObject::fillElectronContainer> ERROR Request to get electron for unknown systematic: " << sys_name << std::endl;
    return;
  }

  // Output container
  ElectronVariable* ele = new ElectronVariable();

  //
  const xAOD::TrackParticle* track = ele_xAOD->trackParticle();

  // TLV
  ele->SetPtEtaPhiM( ele_xAOD->pt() * m_convertFromMeV,
		     ele_xAOD->eta(),
		     ele_xAOD->phi(),
		     ele_xAOD->m() * m_convertFromMeV );

  if( m_trackObject ){
    ele->trkLink = m_trackObject->fillTrack(track,primVertex,eventInfo,TrackVariable::TrackType::ELECTRONLINK,sys_name);
  }

  // Charge
  ele->q = ele_xAOD->charge();

  // SUSYTools definitions
  ele->signal  = cacc_signal( *ele_xAOD );
  ele->passOR  = cacc_passOR( *ele_xAOD );
  //ele->baseline_EWKcomb = cacc_baseline_EWKcomb.isAvailable( *ele_xAOD ) ? cacc_baseline_EWKcomb( *ele_xAOD ) : false;

  //
  // Isolation from selector tools
  //

  // Turn off PLVWPs if the file doesn't contain the PLV variables
  if(!cacc_promptLepVeto.isAvailable( *ele_xAOD ) && !m_baseObject->havePLVWPsRemoved)
    m_baseObject->disablePLVWPs(xAOD::Type::Electron);

  // Turn off PLIVWPs if the file doesn't contain the PLIV variables
  if(!cacc_promptLepImprovedVetoElectrons.isAvailable( *ele_xAOD ) && !m_baseObject->havePLIVWPsRemoved)
    m_baseObject->disablePLIVWPs(xAOD::Type::Electron);

  // Calculate the LowPtPLV score if it can be and not yet
  // (See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PromptLeptonTaggerIFF)
  if(cacc_promptLepVeto.isAvailable( *ele_xAOD ) && !cacc_LowPtPLV.isAvailable( *ele_xAOD ))
    m_baseObject->isoLowPtPLVTool->augmentPLV(*ele_xAOD);
  const asg::AcceptData isIsoSel    = m_baseObject->isoTool->accept(*ele_xAOD);
  
  ele->IsoHighPtCaloOnly          = isIsoSel.getCutResult("HighPtCaloOnly"); 
  ele->IsoTightTrackOnly_VarRad   = isIsoSel.getCutResult("TightTrackOnly_VarRad");
  ele->IsoTightTrackOnly_FixedRad = isIsoSel.getCutResult("TightTrackOnly_FixedRad");
  ele->IsoLoose_VarRad            = isIsoSel.getCutResult("Loose_VarRad");
  ele->IsoTight_VarRad            = isIsoSel.getCutResult("Tight_VarRad");

  if(!m_baseObject->havePLVWPsRemoved){
    ele->IsoPLVLoose                = isIsoSel.getCutResult("PLVLoose");
    ele->IsoPLVTight                = isIsoSel.getCutResult("PLVTight");
  }

  
  
  // Retrieve & set isolation from xAODs
  // ptcone

  float ptcone20 = 0.0;
  float ptcone30 = 0.0;
  float ptcone40 = 0.0;  
  ele_xAOD->isolationValue(ptcone20,xAOD::Iso::ptcone20);
  ele_xAOD->isolationValue(ptcone30,xAOD::Iso::ptcone30);
  ele_xAOD->isolationValue(ptcone40,xAOD::Iso::ptcone40);
  ele->ptcone20 = ptcone20 * m_convertFromMeV;
  ele->ptcone30 = ptcone30 * m_convertFromMeV;
  ele->ptcone40 = ptcone40 * m_convertFromMeV;

  // topoetcone
  float topoetcone20 =0.0;
  float topoetcone30 =0.0;
  float topoetcone40 =0.0;
  ele_xAOD->isolationValue(topoetcone20,xAOD::Iso::topoetcone20);
  ele_xAOD->isolationValue(topoetcone30,xAOD::Iso::topoetcone30);
  ele_xAOD->isolationValue(topoetcone40,xAOD::Iso::topoetcone40);
  ele->topoetcone20 = topoetcone20 * m_convertFromMeV;
  ele->topoetcone30 = topoetcone30 * m_convertFromMeV;
  ele->topoetcone40 = topoetcone40 * m_convertFromMeV;

  //  ptvarcone
  ele->ptvarcone20 = cacc_ptvarcone20.isAvailable( *ele_xAOD ) ? 
    cacc_ptvarcone20(*ele_xAOD) * m_convertFromMeV : -1.0;
  ele->ptvarcone30 = cacc_ptvarcone30.isAvailable( *ele_xAOD ) ? 
    cacc_ptvarcone30(*ele_xAOD) * m_convertFromMeV : -1.0;
  ele->ptvarcone40 = cacc_ptvarcone40.isAvailable( *ele_xAOD ) ? 
    cacc_ptvarcone40(*ele_xAOD) * m_convertFromMeV : -1.0;

  // TTVA ptvarcone
 
 ele->ptvarcone30_TightTTVA_pt1000 = 
    cacc_ptvarcone30_TightTTVA_pt1000.isAvailable(*ele_xAOD) ?
    cacc_ptvarcone30_TightTTVA_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;
  ele->ptvarcone30_TightTTVA_pt500 =   
    cacc_ptvarcone30_TightTTVA_pt500.isAvailable(*ele_xAOD) ?    
    cacc_ptvarcone30_TightTTVA_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;
  
  ele->ptvarcone20_TightTTVA_pt1000 = 
    cacc_ptvarcone20_TightTTVA_pt1000.isAvailable(*ele_xAOD) ?
    cacc_ptvarcone20_TightTTVA_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;
  ele->ptvarcone20_TightTTVA_pt500 =   
    cacc_ptvarcone20_TightTTVA_pt500.isAvailable(*ele_xAOD) ?    
    cacc_ptvarcone20_TightTTVA_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000 =   
    cacc_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000.isAvailable(*ele_xAOD) ?    
    cacc_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;

  // TTVA ptcone
 
  ele->ptcone20_TightTTVALooseCone_pt500 =   
    cacc_ptcone20_TightTTVALooseCone_pt500.isAvailable(*ele_xAOD) ?    
    cacc_ptcone20_TightTTVALooseCone_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->ptcone20_TightTTVALooseCone_pt1000 =   
    cacc_ptcone20_TightTTVALooseCone_pt1000.isAvailable(*ele_xAOD) ?    
    cacc_ptcone20_TightTTVALooseCone_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->ptcone20_TightTTVA_pt500 =   
    cacc_ptcone20_TightTTVA_pt500.isAvailable(*ele_xAOD) ?    
    cacc_ptcone20_TightTTVA_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->ptcone20_TightTTVA_pt1000 =   
    cacc_ptcone20_TightTTVA_pt1000.isAvailable(*ele_xAOD) ?    
    cacc_ptcone20_TightTTVA_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000 =   
    cacc_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000.isAvailable(*ele_xAOD) ?    
    cacc_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;
  
  // PFlow isocone
  /**
ele->neflowisol20 = 
    cacc_neflowisol20.isAvailable(*ele_xAOD) ?
    cacc_neflowisol20(*ele_xAOD) * m_convertFromMeV : -1.0;
  */
  // PID

  //
  // Grab eID from accessors whenever available, otherwise fallback on tool
  //

  // Tight
  if( cacc_lhtight.isAvailable(*ele_xAOD) ){
    ele->tightllh = cacc_lhtight(*ele_xAOD);
  }
  else{
    ele->tightllh = (bool)m_elec_llhtight->accept(ele_xAOD);
  }

  // Medium
  if( cacc_lhmedium.isAvailable(*ele_xAOD) ){
    ele->mediumllh = cacc_lhmedium(*ele_xAOD);
  }
  else{
    ele->mediumllh = (bool)m_elec_llhmedium->accept(ele_xAOD);
  }

  // Loose+BL
  if( cacc_lhlooseBL.isAvailable(*ele_xAOD) ){
    ele->looseBLllh = cacc_lhlooseBL(*ele_xAOD);
  }
  else{
    ele->looseBLllh = (bool)m_elec_llhlooseBL->accept(ele_xAOD);
  }

  // Loose
  if( cacc_lhloose.isAvailable(*ele_xAOD) ){
    ele->loosellh = cacc_lhloose(*ele_xAOD);
  }
  else{
    ele->loosellh = (bool)m_elec_llhloose->accept(ele_xAOD);
  }

  // VeryLoose
  if( cacc_lhveryloose.isAvailable(*ele_xAOD) ){
    ele->veryloosellh = cacc_lhveryloose(*ele_xAOD);
  }
  else{
    ele->veryloosellh = (bool)m_elec_llhveryloose->accept(ele_xAOD);
  }

  // TightDNN
  if( cacc_dnntight.isAvailable(*ele_xAOD) ){
    ele->tightdnn = cacc_dnntight(*ele_xAOD);
  }

  // MediumDNN
  if( cacc_dnnmedium.isAvailable(*ele_xAOD) ){
    ele->mediumdnn = cacc_dnnmedium(*ele_xAOD);
  }

  // LooseDNN
  if( cacc_dnnloose.isAvailable(*ele_xAOD) ){
    ele->loosednn = cacc_dnnloose(*ele_xAOD);
  }

  // Pixel requirements
  ele->nPixHitsPlusDeadSensors = TrackObject::getNPixHitsPlusDeadSensors(track);
  ele->passBL                  = TrackObject::getPassBL(track);

  // Impact parameter variables
  ele->d0     = TrackObject::getD0(track);
  ele->z0     = TrackObject::getZ0(track,primVertex);
  ele->d0Err  = TrackObject::getD0Err(track,eventInfo);
  ele->z0Err  = TrackObject::getZ0Err(track);
  ele->pTErr  = TrackObject::getPtErr(track);

  // Primary author of the electron
  ele->author = ele_xAOD->author();

  ele->egMotherType = cacc_firstEgMotherTruthType.isAvailable( *ele_xAOD ) ? cacc_firstEgMotherTruthType( *ele_xAOD ) : -1;
  ele->egMotherOrigin = cacc_firstEgMotherTruthOrigin.isAvailable( *ele_xAOD ) ? cacc_firstEgMotherTruthOrigin( *ele_xAOD ) : -1;
  ele->egMotherPdgId = cacc_firstEgMotherPdgId.isAvailable( *ele_xAOD ) ? cacc_firstEgMotherPdgId( *ele_xAOD ) : -1;
  

  unsigned int IFFtype(99);
  if(!m_isData)CHECK(m_baseObject->IFFClassifier->classify(*ele_xAOD,IFFtype));
  ele->iff_type = IFFtype;

  ele->ambiguityType = cacc_ambiguityType.isAvailable( *ele_xAOD ) ? cacc_ambiguityType( *ele_xAOD ) : -1;
  ele->addAmbiguity = cacc_addAmbiguity.isAvailable( *ele_xAOD ) ? cacc_addAmbiguity( *ele_xAOD ) : -99;

  // Electron Charge Flip Tagger Tool
  ele->ECIDS = cacc_electronsECIDS.isAvailable( *ele_xAOD ) ? cacc_electronsECIDS( *ele_xAOD ) : false;
  ele->ECIDScore = cacc_electronsECIDSResult.isAvailable( *ele_xAOD ) ? cacc_electronsECIDSResult( *ele_xAOD ) : -1.0;

  // Pixel and IBL Hits
  ele_xAOD->trackParticle()->summaryValue(ele->nIBLHits, xAOD::numberOfInnermostPixelLayerHits);
  ele_xAOD->trackParticle()->summaryValue(ele->nPixHits, xAOD::numberOfPixelHits);

  // Trigger decision
  ele->triggerMap = trigMap;

  // Trigger efficiencies
  if(trigEff) ele->trigEff = *trigEff;

  // Efficiency scale factors for all weight systematics
  ele->recoSF = recoEffSF;

  // For truth info
  bool filledTruthTypeOrigin = false;

  // Truth TLV 
  const xAOD::TruthParticle* truthEle = xAOD::TruthHelpers::getTruthParticle( *ele_xAOD );
  if( truthEle ){
    ele->truthTLV.SetPtEtaPhiM( truthEle->pt() * m_convertFromMeV,
                                truthEle->eta(),
                                truthEle->phi(),
                                Constants::ELECTRON_MASS * m_convertFromMeV );

    ele->truthQ = truthEle->charge();
    ele->barcode = truthEle->barcode();

    if( truthEle->isAvailable<unsigned int>("classifierParticleType") && truthEle->isAvailable<unsigned int>("classifierParticleOrigin") ){
      filledTruthTypeOrigin = true;
      ele->type   = truthEle->auxdata<unsigned int>("classifierParticleType");
      ele->origin = truthEle->auxdata<unsigned int>("classifierParticleOrigin");
    }
  }

  // If there was no truth particle associated to the reco object OR if the
  // classifierParticleType was UnknownElectron, let's try checking the reco object's
  // truth information.
  if( !filledTruthTypeOrigin || ele->type == 1 ){
    ele->type   = xAOD::TruthHelpers::getParticleTruthType(*ele_xAOD); 
    ele->origin = xAOD::TruthHelpers::getParticleTruthOrigin(*ele_xAOD);
  }

  // PLV variables
  ele->promptLepVeto_score =
    cacc_promptLepVeto.isAvailable( *ele_xAOD ) ? cacc_promptLepVeto( *ele_xAOD )  : 0;
  ele->LowPtPLV_score =
    cacc_LowPtPLV.isAvailable( *ele_xAOD ) ? cacc_LowPtPLV( *ele_xAOD )  : 0;

  // Detailed electron information
  fillDetailedElectronVariables(ele_xAOD, ele);

  // Save electron
  it->second->push_back(ele);

  Timer::Instance()->End( "ElectronObject::fillElectronContainer" );

}
// ------------------------------------------------------------------------- //
void ElectronObject::fillDetailedElectronVariables(xAOD::Electron* ele_xAOD,ElectronVariable*& ele)
{

  // Don't fill unless user wants
  if( !m_writeDetailedSkim ) return;

  // Electron track variables
  const xAOD::TrackParticle* track = ele_xAOD->trackParticle();
  if(track)
    ele->trackTLV.SetPtEtaPhiM( track->pt() * m_convertFromMeV,
				track->eta(),
				track->phi(),
				Constants::ELECTRON_MASS * m_convertFromMeV );

  // PromptLeptonTagger variables
  ele->plt_input_TrackJetNTrack =
    cacc_promptLepInput_TrackJetNTrack.isAvailable( *ele_xAOD ) ?  cacc_promptLepInput_TrackJetNTrack( *ele_xAOD ) : 0;
  ele->plt_input_DRlj =
    cacc_promptLepInput_DRlj.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_DRlj( *ele_xAOD ) : 0;
  ele->plt_input_rnnip =
    cacc_promptLepInput_rnnip.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_rnnip( *ele_xAOD ) : 0;
  ele->plt_input_DL1mu =
    cacc_promptLepInput_DL1mu.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_DL1mu( *ele_xAOD ) : 0;
  ele->plt_input_PtRel =
    cacc_promptLepInput_PtRel.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_PtRel( *ele_xAOD ) : 0;
  ele->plt_input_PtFrac =
    cacc_promptLepInput_PtFrac.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_PtFrac( *ele_xAOD ) : 0; 
  ele->plt_input_Topoetcone30Rel =
    cacc_promptLepInput_Topoetcone30Rel.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_Topoetcone30Rel( *ele_xAOD ) : 0; 
  ele->plt_input_Ptvarcone30Rel =
    cacc_promptLepInput_Ptvarcone30Rel.isAvailable( *ele_xAOD ) ? cacc_promptLepInput_Ptvarcone30Rel( *ele_xAOD ) : 0; 


  
}
// ------------------------------------------------------------------------- //
void ElectronObject::fillElectronContainerTruth(xAOD::TruthParticle* ele_xAOD, std::string sys_name /*=""*/)
{

  std::map<TString,ElectronVector*>::iterator it = m_electronVectorMap.find(sys_name);

  if( it==m_electronVectorMap.end() ){
    std::cout << "<ElectronObject::fillElectronContainer> Request to get electron for unknown systematic: " << sys_name << std::endl;
    return;
  }

  ElectronVariable* ele = new ElectronVariable();

  // TLV
  ele->SetPtEtaPhiM( ele_xAOD->pt() * m_convertFromMeV,
		     ele_xAOD->eta(),
		     ele_xAOD->phi(),
		     ele_xAOD->m() * m_convertFromMeV );

  // Charge
  ele->q = ele_xAOD->charge();

  // TODO: defaults for all variables, using truth information when it makes sense to do so

  // Set isolation and impact parameters to perfect values
  // for truth electron
  ele->ptcone20 = 0.0;
  ele->ptcone30 = 0.0;
  ele->ptcone40 = 0.0; 
  ele->topoetcone20 =0.0;
  ele->topoetcone30 =0.0;
  ele->topoetcone40 =0.0;
  ele->ptvarcone20  = 0.0;
  ele->ptvarcone30  = 0.0;
  ele->ptvarcone40  = 0.0;
  ele->medium = true;
  ele->tight  = true;
  ele->d0     = 0.0;
  ele->z0     = 0.0;
  ele->d0Err  = 0.0;
  ele->z0Err  = 0.0;

  // TODO: make signal like selection somwhere
  ele->signal = true;
  ele->passOR = true;
  
  ele->type      = 2;
  ele->origin    = ele_xAOD->auxdata<int>("motherID");

  if( ele_xAOD->isAvailable<unsigned int>("classifierParticleType") && ele_xAOD->isAvailable<unsigned int>("classifierParticleOrigin") ){
      ele->type   = ele_xAOD->auxdata<unsigned int>("classifierParticleType");
      ele->origin = ele_xAOD->auxdata<unsigned int>("classifierParticleOrigin");
  }
  
  // set scale factor to 1.0
  std::map<TString, float> recoSF;
  recoSF[""] = 1.0;
  ele->recoSF = recoSF;


  it->second->push_back(ele);


} 
// ------------------------------------------------------------------------- //
void ElectronObject::fillElectronContainer_NearbyLepIsoCorr(xAOD::Electron* ele_xAOD, unsigned int index_baseline_el, std::string sys_name)
{

  Timer::Instance()->Start( "ElectronObject::fillElectronContainer_NearbyLepIsoCorr" );
  
  std::map<TString,ElectronVector*>::iterator it = m_electronVectorMap.find(sys_name);
  
  if( it==m_electronVectorMap.end() ){
    std::cout << "<ElectronObject::fillElectronContainer_NearbyLepIsoCorr> ERROR Request to get electron for unknown systematic: " << sys_name << std::endl;
    return;
  }

  // Get a reference to the ElectronVariable
  ElectronVariable* ele = it->second->at(index_baseline_el);

  // Redefine signal criteria to be pre-isolation signal && corrected isolation
  ele->signal  = cacc_signal( *ele_xAOD ) && ele_xAOD->auxdata<char>("isol_corr");

  // Isolation from selector tools
  const asg::AcceptData  isIsoSel         = m_baseObject->isoTool->accept(*ele_xAOD);
  
  //Old WPs
  ele->corr_IsoFCHighPtCaloOnly        = isIsoSel.getCutResult("FCHighPtCaloOnly"); 
  ele->corr_IsoFCLoose                 = isIsoSel.getCutResult("FCLoose"); 
  ele->corr_IsoFCTight                 = isIsoSel.getCutResult("FCTight"); 
  ele->corr_IsoFCLoose_FixedRad        = isIsoSel.getCutResult("FCLoose_FixedRad");
  ele->corr_IsoFCTight_FixedRad        = isIsoSel.getCutResult("FCTight_FixedRad");
 
  
  ele->corr_IsoHighPtCaloOnly          = isIsoSel.getCutResult("HighPtCaloOnly"); 
  ele->corr_IsoTightTrackOnly_VarRad   = isIsoSel.getCutResult("TightTrackOnly_VarRad");
  ele->corr_IsoTightTrackOnly_FixedRad = isIsoSel.getCutResult("TightTrackOnly_FixedRad");
  ele->corr_IsoLoose_VarRad            = isIsoSel.getCutResult("Loose_VarRad");
  ele->corr_IsoTight_VarRad            = isIsoSel.getCutResult("Tight_VarRad");
  //ele->corr_IsoPflowLoose_VarRad       = isIsoSel.getCutResult("PflowLoose");  
  //ele->corr_IsoPflowTight_VarRad       = isIsoSel.getCutResult("PflowTight");  
  // if(!m_baseObject->havePLVWPsRemoved){
  //   ele->IsoPLVLoose                = isIsoSel.getCutResult("PLVLoose");
  //   ele->IsoPLVTight                = isIsoSel.getCutResult("PLVTight");
  // }
  if(!m_baseObject->havePLIVWPsRemoved){
    ele->IsoPLImprovedTight            = isIsoSel.getCutResult("PLImprovedTight");
    ele->IsoPLImprovedVeryTight        = isIsoSel.getCutResult("PLImprovedVeryTight");
  }  
 
  // Retrieve & set isolation from xAODs
  // ptvarcone
  float ptcone20 = 0.0;
  float ptcone30 = 0.0;
  float ptcone40 = 0.0;  
  ele_xAOD->isolationValue(ptcone20,xAOD::Iso::ptcone20);
  ele_xAOD->isolationValue(ptcone30,xAOD::Iso::ptcone30);
  ele_xAOD->isolationValue(ptcone40,xAOD::Iso::ptcone40);
  ele->corr_ptcone20 = ptcone20 * m_convertFromMeV;
  ele->corr_ptcone30 = ptcone30 * m_convertFromMeV;
  ele->corr_ptcone40 = ptcone40 * m_convertFromMeV;

  // topoetcone
  float topoetcone20 = 0.0;
  float topoetcone30 = 0.0;
  float topoetcone40 = 0.0;
  ele_xAOD->isolationValue(topoetcone20,xAOD::Iso::topoetcone20);
  ele_xAOD->isolationValue(topoetcone30,xAOD::Iso::topoetcone30);
  ele_xAOD->isolationValue(topoetcone40,xAOD::Iso::topoetcone40);
  ele->corr_topoetcone20 = topoetcone20 * m_convertFromMeV;
  ele->corr_topoetcone30 = topoetcone30 * m_convertFromMeV;
  ele->corr_topoetcone40 = topoetcone40 * m_convertFromMeV;

  // ptvarcone
  ele->corr_ptvarcone20 = cacc_corr_ptvarcone20.isAvailable( *ele_xAOD ) ? 
    cacc_corr_ptvarcone20(*ele_xAOD) * m_convertFromMeV: -1.0;
  ele->corr_ptvarcone30 = cacc_corr_ptvarcone30.isAvailable( *ele_xAOD ) ? 
    cacc_corr_ptvarcone30(*ele_xAOD) * m_convertFromMeV: -1.0;
  ele->corr_ptvarcone40 = cacc_corr_ptvarcone40.isAvailable( *ele_xAOD ) ? 
    cacc_corr_ptvarcone40(*ele_xAOD) * m_convertFromMeV: -1.0;

  // TTVA ptvarcone
  
  ele->corr_ptvarcone30_TightTTVA_pt1000 = 
    cacc_corr_ptvarcone30_TightTTVA_pt1000.isAvailable(*ele_xAOD) ?
    cacc_corr_ptvarcone30_TightTTVA_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;
  // ele->corr_ptvarcone30_TightTTVA_pt500 =   
  //   cacc_corr_ptvarcone30_TightTTVA_pt500.isAvailable(*ele_xAOD) ?    
  //   cacc_corr_ptvarcone30_TightTTVA_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;
  
  ele->corr_ptvarcone20_TightTTVA_pt1000 = 
    cacc_corr_ptvarcone20_TightTTVA_pt1000.isAvailable(*ele_xAOD) ?
    cacc_corr_ptvarcone20_TightTTVA_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;
  ele->corr_ptvarcone20_TightTTVA_pt500 =   
    cacc_corr_ptvarcone20_TightTTVA_pt500.isAvailable(*ele_xAOD) ?    
    cacc_corr_ptvarcone20_TightTTVA_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;

  // ele->corr_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000 =   
  //   cacc_corr_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000.isAvailable(*ele_xAOD) ?    
  //   cacc_corr_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;

  // TTVA ptcone
  ele->corr_ptcone20_TightTTVALooseCone_pt500 =   
    cacc_corr_ptcone20_TightTTVALooseCone_pt500.isAvailable(*ele_xAOD) ?    
    cacc_corr_ptcone20_TightTTVALooseCone_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->corr_ptcone20_TightTTVALooseCone_pt1000 =   
    cacc_corr_ptcone20_TightTTVALooseCone_pt1000.isAvailable(*ele_xAOD) ?    
    cacc_corr_ptcone20_TightTTVALooseCone_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->corr_ptcone20_TightTTVA_pt500 =   
    cacc_corr_ptcone20_TightTTVA_pt500.isAvailable(*ele_xAOD) ?    
    cacc_corr_ptcone20_TightTTVA_pt500(*ele_xAOD) * m_convertFromMeV : -1.0;

  ele->corr_ptcone20_TightTTVA_pt1000 =   
    cacc_corr_ptcone20_TightTTVA_pt1000.isAvailable(*ele_xAOD) ?    
    cacc_corr_ptcone20_TightTTVA_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;

  // ele->corr_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000 =   
  //   cacc_corr_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000.isAvailable(*ele_xAOD) ?    
  //   cacc_corr_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000(*ele_xAOD) * m_convertFromMeV : -1.0;
  
  // pflow isolation
  /***
  ele->corr_neflowisol20 =   
    cacc_corr_neflowisol20.isAvailable(*ele_xAOD) ?    
    cacc_corr_neflowisol20(*ele_xAOD) * m_convertFromMeV : -1.0;
  */
  // Save electron
  it->second->at(index_baseline_el) = ele;

  Timer::Instance()->End( "ElectronObject::fillElectronContainer_NearbyLepIsoCorr" );

}
// ------------------------------------------------------------------------- //
const ElectronVector* ElectronObject::getObj(TString sysName)
{

  std::map<TString,ElectronVector*>::iterator it = m_electronVectorMap.find(sysName);

  if(it==m_electronVectorMap.end()){
    std::cout << "<ElectronObject::getObj> WARNING Cannot get electron vector for systematic: " << sysName << std::endl;
    return NULL;
  }

  return it->second;
}
// ------------------------------------------------------------------------- //
void ElectronObject::Reset()
{

  std::map<TString,ElectronVector*>::iterator it;
  for(it = m_electronVectorMap.begin(); it != m_electronVectorMap.end(); it++){

    // Free up memory
    for (ElectronVector::iterator eleItr = it->second->begin(); eleItr != it->second->end(); eleItr++) {
      if( (*eleItr)->trkLink ) delete (*eleItr)->trkLink;
      //(*eleItr)->trkLinks.clear();      
      delete *eleItr;
    }

    it->second->clear();
  
  }
 
}
// ------------------------------------------------------------------------- //
ElectronObject::~ElectronObject()
{

  Reset();

  if( m_elec_llhtight     ) delete m_elec_llhtight;
  if( m_elec_llhmedium    ) delete m_elec_llhmedium;
  if( m_elec_llhlooseBL   ) delete m_elec_llhlooseBL;
  if( m_elec_llhloose     ) delete m_elec_llhloose;
  if( m_elec_llhveryloose ) delete m_elec_llhveryloose;
  if( m_baseObject        ) delete m_baseObject;


}
