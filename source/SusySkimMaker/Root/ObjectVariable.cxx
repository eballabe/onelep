#include "SusySkimMaker/ObjectVariable.h"
#include "TString.h"


ObjectVariable::ObjectVariable() 
{
  setDefault(barcode,0);
}
// ------------------------------------------------------------------------------------- //
ObjectVariable::ObjectVariable(const ObjectVariable &rhs):
  TLorentzVector(rhs),
  triggerMap(rhs.triggerMap),
  truthTLV(rhs.truthTLV),
  barcode(rhs.barcode)
{

}
// ------------------------------------------------------------------------------------- //
ObjectVariable& ObjectVariable::operator=(const ObjectVariable &rhs)
{
  if (this != &rhs) {
    TLorentzVector::operator=(rhs);
    triggerMap   = rhs.triggerMap;
    truthTLV     = rhs.truthTLV;
    barcode      = rhs.barcode;
  }
  return *this;
}
// ------------------------------------------------------------------------------------- //
bool ObjectVariable::trigMatchResult(const TString trigChain)
{

  auto it = this->triggerMap.find(trigChain);

  // Found the chain
  if( it != this->triggerMap.end() ){
    return it->second;
  }
  // Didn't find the chain
  else{
    return false;
  }

}
// ------------------------------------------------------------------------------------- //
Double_t ObjectVariable::DeltaR(const TLorentzVector & v, const Bool_t useRapidity)
{
  if(useRapidity){
    Double_t drap = Rapidity()-v.Rapidity();
    Double_t dphi = TVector2::Phi_mpi_pi(Phi()-v.Phi());
    return TMath::Sqrt( drap*drap+dphi*dphi );
  } else {
    Double_t deta = Eta()-v.Eta();
    Double_t dphi = TVector2::Phi_mpi_pi(Phi()-v.Phi());
    return TMath::Sqrt( deta*deta+dphi*dphi );
  }   
}
