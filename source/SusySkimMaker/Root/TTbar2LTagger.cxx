#include "SusySkimMaker/TTbar2LTagger.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
/// solutions of bblnln six constrains
//////////////////////////////////////////////////////////////////////////////////////////////////

void TTbar2LTagger::solve2l(Double_t pl1a[4], Double_t pl2a[4], Double_t pl1b[4], Double_t pl2b[4], Double_t pxm, 
		   Double_t pym)
{ 
  // C************************************************************
  // C     routine to solve the momenta of two invisible particles
  // C     at the end of two decay chains each of which is the
  // C     cascade of two two-body decays, using the constraints
  // C     from the masses of intermediate particles, and the
  // C     measured ptmiss
  // C     The chains are:
  // C
  // C        chi(1) -> sl(1) l2a
  // C                ->  a(1) l1a
  // C
  // C        chi(2) -> sl(2) l2b
  // C                ->  a(2) l1b
  // C                            
  // C        pxm = pxa(1)+pxa(2)
  // C        pym=  pya(1)+pya(2)
  // C
  // C     G. Polesello   August 2003
  // C***********************************************************
  
  Double_t ao[6],bo[6],c[6],d[6];
  Double_t ma;
  //
  Double_t a1,a2,a3,a4,a5,a6;
  Double_t b1,b2,b3,b4,b5,b6;
  // Double_t pxa[4],pxb[4],pya[4],pyb[4],pza[4],pzb[4];
  
  const Double_t GeV = 1.;//000.0;
  ma = 0.0*GeV;

  for(Int_t i=0;i<4;i++) {
    xsol_solve[i] = 0.0;
    ysol_solve[i] = 0.0;
  }
  nsol_solve = 0;

  //
  //    get p4 and p3   as linear fuctions of p1 and p2
  //
  //     p4=a1*p1+a2*p2+a3
  //     p3=a4*p1+a5*p2+a6
  //
  single(pl1a,pl2a);
  for (Int_t i = 0;i<6;i++) ao[i] = a_single[i];
  
  a1=ao[0];
  a2=ao[1];
  a3=ao[2];
  a4=ao[3];
  a5=ao[4];
  a6=ao[5];
  //
  //     same for second leg
  //
  single(pl1b,pl2b);
  for (Int_t i = 0;i<6;i++) bo[i] = a_single[i];
  //
  //     incorporate constraint
  //     pxm=p1a+p1b   pym=p2a+p2b
  //
  b1=-bo[0];
  b2=-bo[1];
  b3=bo[0]*pxm+bo[1]*pym+bo[2];
  b4=-bo[3];
  b5=-bo[4];
  b6=bo[3]*pxm+bo[4]*pym+bo[5];
  //
  //     incorporate constraint
  //
  //     ma**2=p4**2-p1**2-p2**2-p3**2   ->
  //
  //     c1*p1**2+c2*p2**2+c3*p1*p2+c4*p1+c5*p2+c6=0
  //
  c[0]=pow(a1,2)-pow(a4,2)-1.0;
  c[1]=pow(a2,2)-pow(a5,2)-1.0;
  c[2]=2.0*(a1*a2-a4*a5);
  c[3]=2.0*(a1*a3-a4*a6);
  c[4]=2.0*(a2*a3-a5*a6);
  c[5]=(pow(a3,2)-pow(a6,2))-pow(ma,2);
  //
  //     same for second leg, but as a function of p1 and p2 for
  //     first leg, using ptmiss constraint
  //
  d[0]=pow(b1,2)-pow(b4,2)-1.0;
  d[1]=pow(b2,2)-pow(b5,2)-1.0;
  d[2]=2.0*(b1*b2-b4*b5);
  d[3]=2.0*(b1*b3-b4*b6)+2.0*pxm;
  d[4]=2.0*(b2*b3-b5*b6)+2.0*pym;
  d[5]=(pow(b3,2)-pow(b6,2))-pow(ma,2)-pow(pxm,2)-pow(pym,2);
  //
  //     solve system of 2 quadratic eq. in two unknowns
  //
  solve2d2(c,d);
  //
  //     got solutions for pxA and pyA, now calculate all others
  //     and output 3-momenta of two neutralinos
  //
  nsol = nsol_solve;
  for(Int_t i=0;i<nsol_solve;i++) {
    pxa[i]=xsol_solve[i];
    pya[i]=ysol_solve[i];
    pza[i]=ao[3]*pxa[i]+ao[4]*pya[i]+ao[5];
    pxb[i]=pxm-pxa[i];
    pyb[i]=pym-pya[i];
    pzb[i]=bo[3]*pxb[i]+bo[4]*pyb[i]+bo[5];
  }
}



//*********************************************
void TTbar2LTagger::single(Double_t pl1[4], Double_t pl2[4])
{
  //*********************************************
  //
  //     in decay   chi -> sl l2
  //                        ->  a l1
  //                            
  //     with a invisible, and all masses known,
  //     calculate coefficients a such that:
  //
  //     ea =a[0]*pxa+a[1]*pya+a[2]
  //     pza=a[3]*pxa+a[4]*pya+a[5]
  //     
  //     exploiting mass-shell condition for chi, sl, a
  //
  //     G. Polesello, August 2003
  //*************************************************
  
  Double_t mchi,mchi2,msl,msl2,ma,ma2;
  Double_t M1,M2;
  Double_t aa,bb,cc,dd,kk;
  Double_t aak,bbk,cck,ddk;
  //
  
  for (Int_t i = 0;i<6;i++) a_single[i] = 0.0;
  
  const Double_t GeV = 1.;//000.0;
  
  mchi = 172.5*GeV;
  msl = 80.4*GeV;
  ma = 0.0*GeV;

  ma2=pow(ma,2);
  mchi2=pow(mchi,2);
  msl2=pow(msl,2);
  //

  Double_t pr4=pl1[3]*pl2[3]-pl1[0]*pl2[0]-pl1[1]*pl2[1]-pl1[2]*pl2[2];
  
  M1=msl2-ma2;
  M2=mchi2-ma2-2.0*pr4;
  //
  aa=2.0*pl1[3];
  bb=-2.0*pl1[2];
  cc=2.0*(pl1[3]+pl2[3]);
  dd=-2.0*(pl1[2]+pl2[2]);
  //
  kk=4.0*(pl2[3]*pl1[2]-pl2[2]*pl1[3]);
  //
  aak=aa/kk;
  bbk=bb/kk;
  cck=cc/kk;
  ddk=dd/kk;
  //
  a_single[0]=2.0*pl1[0]*ddk-2.0*(pl1[0]+pl2[0])*bbk;
  a_single[1]=2.0*pl1[1]*ddk-2.0*(pl1[1]+pl2[1])*bbk;
  a_single[2]=M1*ddk-M2*bbk;
  a_single[3]=2.0*(pl1[0]+pl2[0])*aak-2.0*pl1[0]*cck;
  a_single[4]=2.0*(pl1[1]+pl2[1])*aak-2.0*pl1[1]*cck;
  a_single[5]=M2*aak-M1*cck;
  //
}




//***************************************************
void TTbar2LTagger::solve2d2(Double_t c[6], Double_t d[6]) 
{
  //***************************************************
  //     Solve a system of two second degree
  //     equations in two unknowns. 
  //***************************************************
  
  Double_t f1,f2,f3,f4,f5;
  Double_t g1,g2,g3,g4,g5;
  //
  //     First Eliminate quadratic term in y from
  //     first equation by linear combination with first equation
  //
  f1=(d[1]*c[0]-c[1]*d[0]);
  f2=(d[1]*c[2]-c[1]*d[2]);
  f3=(d[1]*c[3]-c[1]*d[3]);
  f4=(d[1]*c[4]-c[1]*d[4]);
  f5=(d[1]*c[5]-c[1]*d[5]);
  //
  //     solve first equation for p2 and substitute into
  //     second equation: get 4th degree equation
  //
  //     g1*p1**4+g2*p1**3+g3*p1**2+g4*p1+g5=0
  //
  g1=d[0]*(pow(f2,2))+d[1]*(pow(f1,2))-d[2]*f1*f2;
  
  g2=2.*d[0]*f2*f4+2.*d[1]*f1*f3-d[2]*(f1*f4+f3*f2)
    +d[3]*(pow(f2,2))-d[4]*(f1*f2);
  
  g3=d[0]*(pow(f4,2))+d[1]*(pow(f3,2)+2.*f1*f5)-d[2]*(f3*f4+f5*f2)
    +2.*d[3]*f4*f2-d[4]*(f1*f4+f3*f2)+d[5]*(pow(f2,2));
  
  g4=2.*d[1]*f3*f5-d[2]*f4*f5
    +d[3]*(pow(f4,2))-d[4]*(f3*f4+f5*f2)+2.*d[5]*f4*f2;
  
  g5=d[1]*(pow(f5,2))-d[4]*f4*f5+d[5]*(pow(f4,2));
  //
  //    now solve quartic equation with appropirate cernlib routine
  //

  TComplex* z1 = new TComplex();
  TComplex* z2 = new TComplex();  
  TComplex* z3 = new TComplex();
  TComplex* z4 = new TComplex();

  r8poly4_root(g1,g2,g3,g4,g5,z1,z2,z3,z4);

  nsol_solve = 0; 

  if (z1->Im() == 0.0) {
    xsol_solve[nsol_solve]=z1->Re();
    ysol_solve[nsol_solve]=-(f1*pow(xsol_solve[nsol_solve],2)
			     +f3*xsol_solve[nsol_solve]+f5)/(f2*xsol_solve[nsol_solve]+f4);
    nsol_solve++;
  }
  if (z2->Im() == 0.0) {
    xsol_solve[nsol_solve]=z2->Re();
    ysol_solve[nsol_solve]=-(f1*pow(xsol_solve[nsol_solve],2)
			     +f3*xsol_solve[nsol_solve]+f5)/(f2*xsol_solve[nsol_solve]+f4);
    nsol_solve++;
  }
  if (z3->Im() == 0.0) {
    xsol_solve[nsol_solve]=z3->Re();
    ysol_solve[nsol_solve]=-(f1*pow(xsol_solve[nsol_solve],2)
			     +f3*xsol_solve[nsol_solve]+f5)/(f2*xsol_solve[nsol_solve]+f4);
    nsol_solve++;
  }
  if (z4->Im() == 0.0) {
    xsol_solve[nsol_solve]=z4->Re();
    ysol_solve[nsol_solve]=-(f1*pow(xsol_solve[nsol_solve],2)
			     +f3*xsol_solve[nsol_solve]+f5)/(f2*xsol_solve[nsol_solve]+f4);
    nsol_solve++;
  } 

  // NB Vadym - this next bit returns roots with smallest imaginary part if no real solution
  /*
  if (nsol_solve == 0) {
    if ((fabs(z1->Im()) < fabs(z2->Im())) && (fabs(z1->Im()) < fabs(z3->Im())) && (fabs(z1->Im()) < fabs(z4->Im()))) {
      xsol_solve[0]=z1->Re();
    } else if ((fabs(z2->Im()) < fabs(z1->Im())) && (fabs(z2->Im()) < fabs(z3->Im())) && (fabs(z2->Im()) < fabs(z4->Im()))) {
      xsol_solve[0]=z2->Re();
    } else if ((fabs(z3->Im()) < fabs(z1->Im())) && (fabs(z3->Im()) < fabs(z2->Im())) && (fabs(z3->Im()) < fabs(z4->Im()))) {
      xsol_solve[0]=z3->Re();
    } else {
      xsol_solve[0]=z4->Re();
    }
    ysol_solve[0]=-(f1*pow(xsol_solve[0],2)
			     +f3*xsol_solve[0]+f5)/(f2*xsol_solve[0]+f4);
    nsol_solve = 1;
  }
  */
  delete z1;
  delete z2;
  delete z3;
  delete z4;
}




//******************************************************************************

void TTbar2LTagger::r8poly4_root ( Double_t a, Double_t b, Double_t c, Double_t d, Double_t e, TComplex *r1, 
  TComplex *r2, TComplex *r3, TComplex *r4 )

//******************************************************************************
//
//  Purpose:
//
//    R8POLY4_ROOT returns the four roots of a quartic polynomial.
//
//  Discussion:
//
//    The polynomial has the form:
//
//      A * X**4 + B * X**3 + C * X**2 + D * X + E = 0
//
//  Modified:
//
//    27 October 2005
//
//  Parameters:
//
//    Input, double A, B, C, D, the coefficients of the polynomial.
//    A must not be zero.
//
//    Output, complex *R1, *R2, *R3, *R4, the roots of the polynomial.
//
{
  Double_t a3;
  Double_t a4;
  Double_t b3;
  Double_t b4;
  Double_t c3;
  Double_t c4;
  Double_t d3;
  Double_t d4;
  TComplex p;
  TComplex q;
  TComplex r;
  TComplex zero;

  zero = 0.0;

  if ( a == 0.0 )
  {
    //cout << "\n";
    //cout << "R8POLY4_ROOT - Fatal error!\n";
    //cout << "  A must not be zero!\n";
    exit ( 1 );
  }

  a4 = b / a;
  b4 = c / a;
  c4 = d / a;
  d4 = e / a;
//
//  Set the coefficients of the resolvent cubic equation.
//
  a3 = 1.0;
  b3 = -b4;
  c3 = a4 * c4 - 4.0 * d4;
  d3 = -a4 * a4 * d4 + 4.0 * b4 * d4 - c4 * c4;
//
//  Find the roots of the resolvent cubic.
//
  r8poly3_root ( a3, b3, c3, d3, r1, r2, r3 );
//
//  Choose one root of the cubic, here R1.
//
//  Set R = sqrt ( 0.25 * A4**2 - B4 + R1 )
//
  r = c8_sqrt ( 0.25 * a4 * a4 - b4  + (*r1) );
  //  if ( r != zero )
  if ((r.Re()!=0.0) || (r.Im()!=0.0))
  {
    p = c8_sqrt ( 0.75 * a4 * a4 - r * r - 2.0 * b4 
        + 0.25 * ( 4.0 * a4 * b4 - 8.0 * c4 - a4 * a4 * a4 ) / r );

    q = c8_sqrt ( 0.75 * a4 * a4 - r * r - 2.0 * b4 
        - 0.25 * ( 4.0 * a4 * b4 - 8.0 * c4 - a4 * a4 * a4 ) / r );
  }
  else
  {
    p = c8_sqrt ( 0.75 * a4 * a4 - 2.0 * b4 
      + 2.0 * c8_sqrt ( (*r1) * (*r1) - 4.0 * d4 ) );

    q = c8_sqrt ( 0.75 * a4 * a4 - 2.0 * b4 
      - 2.0 * c8_sqrt ( (*r1) * (*r1) - 4.0 * d4 ) );
  }
//
//  Set the roots.
//
  *r1 = -0.25 * a4 + 0.5 * r + 0.5 * p;
  *r2 = -0.25 * a4 + 0.5 * r - 0.5 * p;
  *r3 = -0.25 * a4 - 0.5 * r + 0.5 * q;
  *r4 = -0.25 * a4 - 0.5 * r - 0.5 * q;
  return;
}


//******************************************************************************

void TTbar2LTagger::r8poly3_root ( Double_t a, Double_t b, Double_t c, Double_t d, TComplex *r1, TComplex *r2, TComplex *r3 )

//******************************************************************************
//
//  Purpose:
//
//    R8POLY3_ROOT returns the three roots of a cubic polynomial.
//
//  Discussion:
//
//    The polynomial has the form
//
//      A * X**3 + B * X**2 + C * X + D = 0
//
//  Modified:
//
//    25 October 2005
//
//  Parameters:
//
//    Input, double A, B, C, D, the coefficients of the polynomial.
//    A must not be zero.
//
//    Output, complex *R1, *R2, *R3, the roots of the polynomial, which
//    will include at least one real root.
//
{
  TComplex i;
  Double_t pi = 3.141592653589793;
  Double_t q;
  Double_t r;
  Double_t s1;
  Double_t s2;
  Double_t temp;
  Double_t theta;

  if ( a == 0.0 )
  {
    // cout << "\n";
    //cout << "R8POLY3_ROOT - Fatal error!\n";
    //cout << "  A must not be zero!\n";
    exit ( 1 );
  }

  i = TComplex ( 0.0, 1.0 );

  q = ( pow ( b / a, 2 ) - 3.0 * ( c / a ) ) / 9.0;

  r = ( 2.0 * pow ( b / a, 3 ) - 9.0 * ( b / a ) * ( c / a ) 
      + 27.0 * ( d / a ) ) / 54.0;

  if ( r * r < q * q * q )
  {
    theta = acos ( r / sqrt ( pow ( q, 3 ) ) );
    *r1 = -2.0 * sqrt ( q ) * cos (   theta              / 3.0 );
    *r2 = -2.0 * sqrt ( q ) * cos ( ( theta + 2.0 * pi ) / 3.0 );
    *r3 = -2.0 * sqrt ( q ) * cos ( ( theta + 4.0 * pi ) / 3.0 );
  }
  else if ( q * q * q <= r * r )
  {
    temp = -r + sqrt ( r * r - q * q * q );
    s1 = r8_sign ( temp ) * pow ( fabs ( temp ), 1.0 / 3.0 );

    temp = -r - sqrt ( r * r - q * q * q );
    s2 = r8_sign ( temp ) * pow ( fabs ( temp ), 1.0 / 3.0 );

    *r1 = s1 + s2;
    *r2 = -0.5 * ( s1 + s2 ) + i * 0.5 * sqrt ( 3.0 ) * ( s1 - s2 );
    *r3 = -0.5 * ( s1 + s2 ) - i * 0.5 * sqrt ( 3.0 ) * ( s1 - s2 );
  }

  *r1 = *r1 - b / ( 3.0 * a );
  *r2 = *r2 - b / ( 3.0 * a );
  *r3 = *r3 - b / ( 3.0 * a );

  return;
}


//******************************************************************************

//TComplex TTbar2LTagger::c8_sqrt ( TComplex x )
  Double_t TTbar2LTagger::c8_sqrt ( TComplex x )
//******************************************************************************
//
//  Purpose:
//
//    C8_SQRT returns the principal square root of a C8.
//
//  Discussion:
//
//    A C8 is a double precision complex value.
//
//  Modified:
//
//    27 October 2005
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, complex X, the number whose square root is desired.
//
//    Output, complex C8_SQRT, the square root of X.
//
{
  Double_t argument;
  Double_t magnitude;
  TComplex value;
  
  argument = c8_argument ( x );
  magnitude = c8_magnitude ( x );

  if ( magnitude == 0.0 )
  {
    value = TComplex ( 0.0, 0.0 );
  }
  else
  {
    value = pow ( magnitude, 1.0 / 2.0 ) 
      * TComplex ( cos ( argument / 2.0 ), sin ( argument / 2.0 ) );
  }

  return value;
}

//*********************************************************************

Double_t TTbar2LTagger::r8_sign ( Double_t x )

//*********************************************************************
//
//  Purpose:
//
//    R8_SIGN returns the sign of a double precision number.
//
//  Modified:
//
//    18 October 2004
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, double X, the number whose sign is desired.
//
//    Output, double R8_SIGN, the sign of X.
//
{
  if ( x < 0.0 )
  {
    return ( -1.0 );
  } 
  else
  {
    return ( 1.0 );
  }
}

//******************************************************************************

Double_t TTbar2LTagger::c8_argument ( TComplex x )

//******************************************************************************
//
//  Purpose:
//
//    C8_ARGUMENT returns the argument of a C8.
//
//  Discussion:
//
//    A C8 is a double precision TComplex value.
//
//  Modified:
//
//    21 October 2005
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, complex X, the value whose argument is desired.
//
//    Output, double C8_ARGUMENT, the argument of X.
//
{
  Double_t argument;

  argument = atan2(x.Im(),x.Re());

  return argument;
}

//******************************************************************************

Double_t TTbar2LTagger::c8_magnitude ( TComplex x )

//******************************************************************************
//
//  Purpose:
//
//    C8_MAGNITUDE returns the magnitude of a C8.
//
//  Discussion:
//
//    A C8 is a double precision complex value.
//
//  Modified:
//
//    22 October 2005
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, complex X, the value whose norm is desired.
//
//    Output, double C8_MAGNITUDE, the magnitude of X.
//
{
  Double_t magnitude;

  magnitude = sqrt(pow(x.Re(),2) + pow(x.Im(),2));

  return magnitude;
}
