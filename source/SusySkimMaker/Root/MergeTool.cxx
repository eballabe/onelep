#include "SusySkimMaker/MergeTool.h"
#include "SusySkimMaker/EventObject.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "AsgMessaging/StatusCode.h"
#include "TSystem.h"
#include "TKey.h"

MergeTool::MergeTool(TChain* parentTree, TChain* cbkChain) : m_parentTree(parentTree),
                                                             m_lumi(1.0),
                                                             m_isData(false),
                                                             m_eventNumber(0),
                                                             m_minEvent(0),
                                                             m_maxEvent(0),
                                                             m_childTrees(),
                                                             m_fields(),
                                                             m_cbkSumwMap(),
                                                             m_eventObject(0)
{
    if(cbkChain) m_cbkChain = (TChain*) cbkChain->Clone();
}
// ------------------------------------------------------------- //
StatusCode MergeTool::init(bool isData, bool isAf2,int minEvent,int maxEvent)
{

  const char* APP_NAME = "MergeTool";

  if (!m_cbkChain && !m_evtCounterHist) {
    Error("MergeTool::init", "Need to provide either a cutBookkeeper tree or an Event counter hist!");
    return StatusCode::FAILURE;
  }

  if (m_cbkChain) {
    fillCBKMap();
  }

  // Locations of branches in input tree
  SetBranchAddresses();

  // Init event object, for access to xsec, pileup information, etc
  m_eventObject = new EventObject();
  CHECK( m_eventObject->init_tools( isData, isAf2, true ) ); 

  // Save data flag
  m_isData = isData;

  m_minEvent = minEvent>0 ? minEvent : 0;
  m_maxEvent = maxEvent>0 ? maxEvent : m_parentTree->GetEntries();
  m_eventNumber=m_minEvent;

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------- //
void MergeTool::SetBranchAddresses()
{

  //
  Info("MergeTool::SetBranchAddresses","Setting branch addresses for parent tree...");

  // Normalization related quantites
  SetBranchAddress("DatasetNumber",DataType::Type::INT,MergeAttribute::NORMALIZATION);
  SetBranchAddress("RunNumber",DataType::Type::INT,MergeAttribute::NORMALIZATION);
  SetBranchAddress("genWeight",DataType::Type::DOUBLE,MergeAttribute::NORMALIZATION);
  SetBranchAddress("genWeightUp",DataType::Type::DOUBLE,MergeAttribute::NORMALIZATION);
  SetBranchAddress("genWeightDown",DataType::Type::DOUBLE,MergeAttribute::NORMALIZATION);
  SetBranchAddress("FS",DataType::Type::INT,MergeAttribute::NORMALIZATION);

  // If xsec branch exist, update
  SetBranchAddress("xsec",DataType::Type::FLOAT,MergeAttribute::XSEC);

}
// ------------------------------------------------------------- //
void MergeTool::calculateCommon()
{

  // Commmon variables
  calculateNormalization();

}
// ------------------------------------------------------------- //
void MergeTool::fill()
{

  for( auto& childTree : m_childTrees ){
    DataType* user = getProperty(childTree.second->branchName);
    // DMJ: Hack to increase max file size to 1 TB
    childTree.second->tree->SetMaxTreeSize(1000000000000LL);
    if( user ){
      if( user->intType==childTree.second->intValue ){
        childTree.second->tree->Fill();
      }
    }
    else{
      bool result = doTopOverlap(childTree.second->tree->GetName());
      if (result) {
        childTree.second->tree->Fill();
      }
    }
  }

}
// ------------------------------------------------------------- //
bool MergeTool::next()
{

  if( m_eventNumber>=m_maxEvent ) return false;

  // Retrieve this event
  int getEntry = m_parentTree->GetEntry(m_eventNumber);
  if(getEntry==-1){
    Error("MergeTool::next", "GetEntry() I/O occurred error!");
    abort();
  }
  else if(getEntry==0){
    return next();
  }

  if (m_eventNumber%100000 == 0) {
    Info("MergeTool::next","Processed %i / %i events...",m_eventNumber,m_maxEvent );
  }
  m_eventNumber++;

  return true;

}
// ------------------------------------------------------------- //
void MergeTool::fillOutputTrees(int minEvent,int maxEvent)
{

  if( m_childTrees.size()==0 ){
    Warning("MergeTool::fillOutputTrees","No child trees, nothing will be done!");
    return;
  }

  if( !m_eventObject ){
    Error("MergeTool::fillOutputTrees","Did you call init??? No valid EventObject tool, not filling the output trees!!");
    return;
  }

  Info("MergeTool::fillOutputTrees","Starting loop over parent tree with %lli entries",m_parentTree->GetEntries() );

  if( minEvent<0 ) minEvent = 0;
  if( maxEvent<0 ) maxEvent = m_parentTree->GetEntries();

  // Loop over all entries
  for( Int_t e=minEvent; e<maxEvent; e++){

    int getEntry = m_parentTree->GetEntry(e);
    if(getEntry==-1){
      Error("MergeTool::fillOutputTrees", "GetEntry() I/O occurred error!");
      abort();
    }
    else if(getEntry==0){
      // This is the return code for an invalid entry, so skip it
      continue;
    }

    //
    if (e%100000 == 0) {
      Info("MergeTool::fillOutputTrees","Processed %i / %lli events...",e,m_parentTree->GetEntries() );
    }

    /*
    if( getAttributeState(MergeAttribute::USER) ){
      DataType* eta = getProperty("probeEta");
      DataType* pt  = getProperty("probePt_smeared");
      DataType* f   = getProperty("probeFlavor");
      DataType* w   = getProperty("vetoProb");

      float pts = pt->floatType;
      if( pts>140.0 ) pts=135.0;


      // Muons
      if( f->intType==1 ){
        w->doubleType = m_muonRates->GetBinContent(m_muonRates->FindBin(pts,fabs(eta->floatType)) );
      }
      else if( f->intType==2 ){
        w->doubleType = m_electronRates->GetBinContent(m_electronRates->FindBin(pts,fabs(eta->floatType)) );
      }
      else{
        w->doubleType = 1.0;
      }
      //MsgLog::INFO("Merge","Weight for pT %f,eta %f and flavor %i is %.10f",pt->floatType,eta->floatType,f->intType,w->doubleType);
    }

    if( getAttributeState(MergeAttribute::USER) ){
      DataType* pt      = getProperty("lep1Pt");
      DataType* signal  = getProperty("lep1Signal");
      DataType* flavor  = getProperty("lep1AnalysisType");
      DataType* w       = getProperty("fakeWeight");

      double f  = 1.0;
      double e  = 1.0;
      double nS = signal->boolType==1 ? 1.0 : 0.0;

      // Electrons
      if( flavor->intType==1 ){
        e = m_electronRealRate->Eval( pt->floatType );
        f = m_electronFakeRate->Eval( pt->floatType );
      }
      // Muons
      else if( flavor->intType==2 ){
        e = m_muonRealRate->Eval( pt->floatType );
        f = m_muonFakeRate->Eval( pt->floatType );
      }

      w->doubleType = (f / (e-f)) * (e - nS);

      //MsgLog::INFO("merge","Lepton with fake weight %f for pT %f",w->doubleType,pt->floatType);
    }
*/

    // Methods for recalculation
    // Each method should be standalone
    // TODO: Move to common method
    calculateNormalization();
    calculateSumXsecNorm();

    // Fill output trees based on cuts
    // TODO: Move to fill method
    for( auto& childTree : m_childTrees ){

      DataType* user = getProperty(childTree.second->branchName);
      if( user ){
        if( user->intType==childTree.second->intValue ){
          childTree.second->tree->Fill();
        }
      }
      else{
        bool result = doTopOverlap(childTree.second->tree->GetName());
        if (result) {
          childTree.second->tree->Fill();
        }
      }

    } // Loop over child trees

  } // Loop over tree entries



}
// ---------------------------------------------------------------------- //
void MergeTool::calculateNormalization()
{

  // Information is not found on input, don't run
  if( !getAttributeState(MergeAttribute::NORMALIZATION) ){
    return;
  }

  //
  DataType* dsid          = getProperty("DatasetNumber");
  DataType* run           = getProperty("RunNumber");
  DataType* genWeight     = getProperty("genWeight");
  DataType* genWeightUp   = getProperty("genWeightUp");
  DataType* genWeightDown = getProperty("genWeightDown");
  DataType* fs            = getProperty("FS");

  DataType* xsec_prop;

  // Temporary hack
  if (!m_isData) {
    run->intType = 999;
  }

  // Normalization
  if( !m_isData ){

    float sumw = 0.;
    if ( !m_cbkChain ) {
      // Use evtCounterHist
      int iBin = m_evtCounterHist->GetXaxis()->FindBin(TString::Format("%d", dsid->intType));
      sumw = m_evtCounterHist->GetBinContent(iBin);
    } else {

      // When the FS is non-zero try to obtain
      // the CBK which has FS specific weights
      //   Note: As xsec in PMGTool database is always 0, FS=0 is forced when xsec is retrieved from PMG tool!!
      if( !m_eventObject->getUsePMGXsecTool() && fs->intType>0 ){
        sumw = m_cbkSumwMap[ std::make_tuple(dsid->intType, run->intType, TString::Format("SUSYWeight_ID_%i_sumOfEventWeights",fs->intType)) ];
      }

      // Means we didn't find anything above, or
      // that we are runnning over a background (FS==0)
      if( sumw<=0.0 && !isDijetSample(dsid->intType)){
        sumw = m_cbkSumwMap[ std::make_tuple(dsid->intType, run->intType, "AllExecutedEvents_sumOfEventWeights") ];
      }
      // Dijet MC needs to be normalised to nAcceptedEvents instead of sumOfEventWeights
      // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetStudies2012#Weighting_scheme_and_MC_quality
      // https://cds.cern.ch/record/1370089?
      else if (isDijetSample(dsid->intType)) {
        sumw = m_cbkNEventMap[ std::make_tuple(dsid->intType, run->intType, "AllExecutedEvents_nAcceptedEvents") ];
      }

    }

    //
    if ( !sumw ) {
      Error("MergeTool::calculateNormalization","no SumW %f found for DSID %i ",sumw,dsid->intType );
      abort();
      return;
    }

    // Get cross section and its up/down variations
    //   Note that the finalState is ignored in the getXsec function when m_usePMGXsecTool is true
    //   i.e. FS=0 cross section and its uncertainty will retrieved,
    //   since PMGTool only stores the FS=0 xsec right now. (23 Apr 2019 Shion)
    double xsec        = m_eventObject->getXsec(dsid->intType, fs->intType, EventObject::XSEC_INFO::ALL);
    double xsec_relunc = m_eventObject->getXsec(dsid->intType, fs->intType, EventObject::XSEC_INFO::UNCERT);
    double xsec_up     = xsec + xsec*xsec_relunc;
    double xsec_down   = xsec - xsec*xsec_relunc;
 
    // if xsec branch exists, update
    if (getAttributeState(MergeAttribute::XSEC)) {
      xsec_prop = getProperty("xsec");
      xsec_prop->floatType = xsec;
    }

    if ( xsec < 0 ) {
      Error("MergeTool::calculateNormalization","No xsec found for DSID %i, FS %i. Be careful with this DSID and FS!",dsid->intType, fs->intType );
    }

    // Set values
    genWeight->doubleType      = xsec      * m_lumi / sumw;
    genWeightUp->doubleType    = xsec_up   * m_lumi / sumw;
    genWeightDown->doubleType  = xsec_down * m_lumi / sumw;

  }
  else{
    genWeight->doubleType      = 1.0;
    genWeightUp->doubleType    = 1.0;
    genWeightDown->doubleType  = 1.0;
  }

}
// ------------------------------------------------------------------------------------- //
void MergeTool::calculateSumXsecNorm()
{

  // Check the user has provided all inputs
  if( m_isData || !getAttributeState(MergeAttribute::CBK_FSSUM) ){
    return;
  }
  DataType* dsid          = getProperty("DatasetNumber");
  DataType* run           = getProperty("RunNumber");
  DataType* genWeight     = getProperty("genSumWeight");
  DataType* genWeightUp   = getProperty("genSumWeightUp");
  DataType* genWeightDown = getProperty("genSumWeightDown");

  // Temporary hack
  if (!m_isData) {
    run->intType = 999;
  }

  float sumOfXSecs = 0.0;
  for(int fs=0; fs<=224; fs++){
    double xsec        = m_eventObject->getXsec(dsid->intType, fs, EventObject::XSEC_INFO::ALL);

    // TODO: Propagate the uncertainties
    //double xsec_relunc = m_eventObject->getXsec(dsid->intType, fs, EventObject::XSEC_INFO::UNCERT);
    if( xsec> 0.0 ){
      sumOfXSecs += xsec;
    }
  }

  if ( sumOfXSecs == 0.0 ) {
    Error("MergeTool::calculateSumXsecNorm","No xsec found for DSID %i. Aborting...",dsid->intType );
    abort();
    return;
  }

  // Relies on the total number of events
  double sumw2 = m_cbkSumwMap[ std::make_tuple(dsid->intType, run->intType, "AllExecutedEvents_sumOfEventWeights") ];

  if ( !sumw2 ) {
    Error("MergeTool::calculateSumXsecNorm","No SumW2 found for DSID %i. Aborting...",dsid->intType );
    abort();
    return;
  }

  // TODO: Uncertainties
  genWeight->doubleType      = sumOfXSecs * m_lumi / sumw2;
  genWeightUp->doubleType    = sumOfXSecs * m_lumi / sumw2;
  genWeightDown->doubleType  = sumOfXSecs * m_lumi / sumw2;

}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isTopHTFiltered(int DatasetNumber) {

  /*
    ttbar:
    mc16_13TeV.407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad
    mc16_13TeV.407343.PhPy8EG_A14_ttbarHT1k_1k5_hdamp258p75_nonallhad
    mc16_13TeV.407344.PhPy8EG_A14_ttbarHT6c_1k_hdamp258p75_nonallhad
   */

  if (DatasetNumber == 407342
      || DatasetNumber == 407343
      || DatasetNumber == 407344) return true;
  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isTopMETFiltered(int DatasetNumber) {

  /*
    ttbar:
    mc16_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad
    mc16_13TeV.407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad
    mc16_13TeV.407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad
  */

  if (DatasetNumber == 407345
        || DatasetNumber == 407346
        || DatasetNumber == 407347) return true;
  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isTopMET200(int /*DatasetNumber*/) {

  /*
    only enter samples here where there is a MET300 overlapping

    None at the moment, keeping for future use
  */

  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isTopMET300(int /*DatasetNumber*/) {

  /*
    only enter samples here where there is a MET400 overlapping

    None at the moment, keeping for future use
  */

  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isTopInclusive(int DatasetNumber) {

  /*
    ttbar:
    mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad
   */

  if (DatasetNumber == 410470) return true;
  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isTTBARSample(int DatasetNumber) {

  /*
    ttbar samples which have to be treated in the HT/MET overlap removal

    mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad
    mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad
    mc16_13TeV.407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad
    mc16_13TeV.407343.PhPy8EG_A14_ttbarHT1k_1k5_hdamp258p75_nonallhad
    mc16_13TeV.407344.PhPy8EG_A14_ttbarHT6c_1k_hdamp258p75_nonallhad
    mc16_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad
    mc16_13TeV.407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad
    mc16_13TeV.407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad

   */

  if (DatasetNumber ==410470
      || DatasetNumber == 410471
      || DatasetNumber == 407342
      || DatasetNumber == 407343
      || DatasetNumber == 407344
      || DatasetNumber == 407345
      || DatasetNumber == 407346
      || DatasetNumber == 407347) return true;
  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isWtSample(int /*DatasetNumber*/) {

  /*
    Wt samples which have to be treated in the HT/MET overlap removal

    None at the moment: https://its.cern.ch/jira/browse/ATLMCPROD-5692
   */

  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::isDijetSample(int DatasetNumber) {

  /*
    Dijet samples that have to be normalised to nEvents instead of sumw

    mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv
    mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv
    mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv
    mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv
    mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv
    mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv
    mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv
    mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv
    mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv
    mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv
    mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv
    mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv
    mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv
   */

  if ((DatasetNumber >= 361020) && (DatasetNumber <= 361032)) {
    return true;
  }
  return false;
}
// ------------------------------------------------------------------------------------- //
bool MergeTool::doTopOverlap(TString treeName)
{

  /*
    Create different trees for different schemes of overlap
   */

  // Information is not found on input, don't run
  if( !getAttributeState(MergeAttribute::TOPOVERLAP) ){
    return true;
  }

  // Get all necessary information
  float GenMET = getProperty("GenMET")->floatType;
  float GenHt = getProperty("GenHt")->floatType;
  int DatasetNumber = getProperty("DatasetNumber")->intType;

  float HtCut;
  float METCut = 200.;
  if (isTTBARSample(DatasetNumber)) {
    HtCut = 600.;
  }
  else if(isWtSample(DatasetNumber)) {
    HtCut = 500.;
  } else {
    return true;
  }

  // If this is running we have to recalculate normalisation
  // (in case the HTMET ran before and changed genWeight)
  calculateNormalization();

  if (treeName.BeginsWith("ttbar_") || treeName.BeginsWith("singletop_")) {

    // For high met only use met filtered
    // Meaning: ht filtered are only used in low met

    // inclusive and ht filtered samples get high met removed
    if ((isTopInclusive(DatasetNumber) || isTopHTFiltered(DatasetNumber)) && GenMET > METCut) return false;

    // inclusive sample also gets high ht removed
    // if (isTopInclusive(DatasetNumber) && GenHt > HtCut) return false;

    // At the moment we don't have the Ht filtered Wt sample in mc16 (so only remove for old ttbar)
    if (isTopInclusive(DatasetNumber) && isTTBARSample(DatasetNumber) && GenHt > HtCut ) return false;

    // Abort in case we reinclude the Ht filtered sample, to remind us
    if (isTopHTFiltered(DatasetNumber) && isWtSample(DatasetNumber)) {
      std::cout << "Trying to include Wt Ht filtered sample - need to re-activate the overlap removal! - Aborting." << std::endl;
      abort();
    }

    // MET200 sample - remove overlap with MET300
    if (isTopMET200(DatasetNumber) && GenMET > 300.) return false;

    // MET300 sample - remove overlap with MET400
    if (isTopMET300(DatasetNumber) && GenMET > 400.) return false;

  } else if (treeName.BeginsWith("ttbarHt_") || treeName.BeginsWith("singletopHt_")) {

    // For high HT only use HT filtered
    // Meaning: MET filtered are only used in low Ht

    // inclusive sample gets high MET and HT removed
    if (isTopInclusive(DatasetNumber) && (GenMET > METCut || GenHt > HtCut)) return false;

    // MET200 sample
    if (isTopMET200(DatasetNumber) && GenMET > 300.) return false;

    // MET300 sample - remove overlap with MET400
    if (isTopMET300(DatasetNumber) && GenMET > 400.) return false;

    // MET filtered samples get high HT removed
    if (isTopMETFiltered(DatasetNumber) && GenHt > HtCut) return false;

  } else if (treeName.BeginsWith("ttbarHtMET_") || treeName.BeginsWith("singletopHtMET_")) {

    // Use HT and MET filtered samples both
    // Divide by 2 in region where they overlap

    double& genWeight = getProperty("genWeight")->doubleType;
    double& genWeightUp = getProperty("genWeightUp")->doubleType;
    double& genWeightDown = getProperty("genWeightDown")->doubleType;

    // Divide genWeight by 2 in overlap of MET/HT samples region
    if (GenMET > METCut && GenHt > HtCut) {
      genWeight *= 0.5;
      genWeightUp *= 0.5;
      genWeightDown *= 0.5;
    }

    // Remove high HT and high MET from inclusive sample
    if (isTopInclusive(DatasetNumber) && (GenMET > METCut || GenHt > HtCut)) return false;

    // Remove MET300 from MET200 sample
    if (isTopMET200(DatasetNumber) && GenMET > 300.) return false;

    // MET300 sample - remove overlap with MET400
    if (isTopMET300(DatasetNumber) && GenMET > 400.) return false;

  }

  return true;
}
// ------------------------------------------------------------------------------------- //
MergeTool::DataType* MergeTool::getProperty(TString name)
{

  auto it = m_fields.find(name);

  if( it == m_fields.end() ){
    //Warning("MergeTool::getProperty","No property %s found!",name.Data() );
    return NULL;
  }

  return it->second;

}
// ---------------------------------------------------------------------- //
void MergeTool::SetBranchAddress(TString name,DataType::Type branchType, MergeAttribute att)
{

  // Do nothing
  if( name.IsWhitespace() ) return;

  // First check if already set
  // If it is, store this MergeAttribute
  // such that a given branch can be
  // associated with multiple attributes
  if( branchAddressSet(name) ){
    setAttributeState(att,true);
    return;
  }

  // Check if branch exists on input
  if(  m_parentTree->GetBranch( name.Data() ) ){

    // Create an entry into the field DB
    // Get reference to field above
    // Link to tree
    DataType* type = new DataType(branchType);

    m_fields.insert( std::pair<TString,DataType*>(name,type) );
    std::map<TString,DataType*>::iterator it = m_fields.find(name);

    if( branchType==DataType::Type::INT ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->intType);
    }
    else if( branchType==DataType::Type::DOUBLE ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->doubleType);
    }
    else if( branchType==DataType::Type::FLOAT ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->floatType);
    }
    else if( branchType==DataType::Type::BOOL ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->boolType);
    }
    else if( branchType==DataType::Type::ULL ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->ullType);
    }
    else if( branchType==DataType::Type::VECINT ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->vecIntType);
    }
    else if( branchType==DataType::Type::VECDOUBLE ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->vecDoubleType);
    }
    else if( branchType==DataType::Type::VECFLOAT ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->vecFloatType);
    }
    else if( branchType==DataType::Type::VECBOOL ){
      m_parentTree->SetBranchAddress( name.Data() , &it->second->vecBoolType);
    }
    // Save asssociated attribute as true
    setAttributeState(att,true);

  }
  else{
    Warning("MergeTool::SetBranchAddress","Could not find branch %s, will not be used in the merge!",name.Data() );

    // Turn off any calculations associated to this attribute
    setAttributeState(att,false);
    return;
  }

  //
  Info("MergeTool::SetBranchAddress","Found branch %s and setting/saving its adddress",name.Data() );

}
// ------------------------------------------------------------- //
bool MergeTool::branchAddressSet(TString name)
{

  auto it = m_fields.find(name);

  if( it!=m_fields.end() ){
    Info("MergeTool::branchAddressSet","Reusing already set branch address for %s",name.Data() );
    return true;
  }

  // Nothing set
  return false;

}
// ------------------------------------------------------------- //
void MergeTool::setAttributeState(MergeAttribute att, bool state)
{

  auto it = m_attributes.find(att);

  if( it==m_attributes.end() ){
    m_attributes.insert( std::pair<MergeAttribute,bool>(att,state) );
  }
  else{
    // Can only set to false, not false to true
    // Since we don't want this attribute to run
    // if *any* of the branches don't exist!
    if( it->second ) it->second = state;
  }

}
// ------------------------------------------------------------- //
bool MergeTool::getAttributeState(MergeAttribute att)
{

  auto it = m_attributes.find(att);

  if( it!=m_attributes.end() ){
    return it->second;
  }

  // Didn't find any attribute
  return false;

}
// -------------------------------------------------------------------------------- //
void MergeTool::addClone(TString name, TFile* outFile,TString branchName, int value)
{

  // Checks
  if( !m_parentTree ){
    MsgLog::ERROR("MergeTool::addClone","Invalid parent tree! Ignoring child tree %s",name.Data() );
    return;
  }

  if( m_parentTree->GetEntries()==0 ){
    MsgLog::WARNING("MergeTool::addClone","Parent tree has no entries!! Ignoring child tree %s",name.Data() );
    return;
  }

  ChildTree* childTree = new ChildTree();

  // Clone
  // Any modifications we make to the parent tree
  // will be also made here
  childTree->tree = m_parentTree->CloneTree(0);
  childTree->tree->SetName(name);
  childTree->tree->SetTitle(name);
  childTree->tree->SetDirectory(outFile);

  // Set branch address
  SetBranchAddress(branchName,DataType::Type::INT,MergeAttribute::USER);

  //
  childTree->branchName = branchName;
  childTree->intValue   = value;

  // Save
  m_childTrees.insert( std::pair<TString,ChildTree*>( name,childTree ) );

  Info("MergeTool::addClone","Added a tree name %s", name.Data() );

}
// ------------------------------------------------------------- //
void MergeTool::print()
{


}
// ------------------------------------------------------------- //
void MergeTool::fillCBKMap()
{

  if (!m_cbkChain) {
    Error("MergeTool::fillCBKMap", "Need to provide a cutBookkeeper tree to fill the cbk map!");
    abort();
  }

  unsigned int RunNumber;
  unsigned int DatasetNumber;

  m_cbkChain->SetBranchAddress("RunNumber", &RunNumber);
  m_cbkChain->SetBranchAddress("DatasetNumber", &DatasetNumber);

  // Set Addresses for all sumw branches
  std::map<TString, float> floatBranches;
  std::map<TString, int> intBranches;
  for (auto branch: *m_cbkChain->GetListOfBranches()) {
    TString branchName = branch->GetName();
    if (branchName.Contains("sumOfEventWeights")) {
      floatBranches[branchName] = 0;
      m_cbkChain->SetBranchAddress(branchName, &floatBranches[branchName]);
    }
    if (branchName.Contains("nAcceptedEvents")) {
      intBranches[branchName] = 0;
      m_cbkChain->SetBranchAddress(branchName, &intBranches[branchName]);
    }
  }

  // Loop over cbk tree and sum up the sumws
  for (int i=0; i<m_cbkChain->GetEntries(); i++) {

    int getEntry = m_cbkChain->GetEntry(i);
    if(getEntry==-1){
      Error("MergeTool::fillCBKMap", "GetEntry() I/O occurred error!");
      abort();
    }
    else if(getEntry==0){
      // This is the return code for an invalid entry, so skip it
      continue;
    }

    // Temporary hack
    if (!m_isData) {
      RunNumber = 999;
    }

    for (auto floatBranch : floatBranches) {
      std::tuple<int, int, TString> branchTuple = std::make_tuple(DatasetNumber, RunNumber, floatBranch.first);
      if (m_cbkSumwMap.find(branchTuple) == m_cbkSumwMap.end()) {
        m_cbkSumwMap[branchTuple] = 0.;
      }
      m_cbkSumwMap[branchTuple] += floatBranch.second;
    }
    for (auto intBranch : intBranches) {
      std::tuple<int, int, TString> branchTuple = std::make_tuple(DatasetNumber, RunNumber, intBranch.first);
      if (m_cbkNEventMap.find(branchTuple) == m_cbkNEventMap.end()) {
        m_cbkNEventMap[branchTuple] = 0.;
      }
      m_cbkNEventMap[branchTuple] += intBranch.second;
    }
  }

}
// ------------------------------------------------------------- //
MergeTool::~MergeTool()
{

  // Clean up memory
  for( auto& childTree : m_childTrees ){
    delete childTree.second;
  }
  m_childTrees.clear();

  for( auto& field : m_fields ){
    if(field.second){
      delete field.second;
    }
  }
  m_fields.clear();

  if( m_cbkChain ){
    delete m_cbkChain;
    m_cbkChain = NULL;
  }

  if( m_eventObject ) delete m_eventObject;
  
  if( m_parentTree ) delete m_parentTree;

}
