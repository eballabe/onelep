#include "SusySkimMaker/TrackVariable.h"
#include "SusySkimMaker/MsgLog.h"

// -------------------------------------------------------------------------------- //
TrackVariable::TrackVariable() : truthLink(0)
{

  trackType = TrackType::UNKNOWN;

  //
  setDefault(isTight,-1.0);
  setDefault(isLoose,-1.0);
  setDefault(q,-0);
  setDefault(d0,-1.0);
  setDefault(z0,-1.0);
  setDefault(d0Err,-1.0);
  setDefault(z0Err,-1.0);
  setDefault(ptcone20,0.0);
  setDefault(ptcone30,0.0);
  setDefault(ptcone40,0.0);
  setDefault(nIBLHits,0);
  setDefault(nPixHits,0);
  setDefault(nIBLHits,0);
  setDefault(nPixLayers,0);
  setDefault(nExpBLayerHits,0);
  setDefault(nPixHoles,0);
  setDefault(nPixOutliers,0);
  setDefault(nGangedFlaggedFakes,0);
  setDefault(nSCTHits,0);
  setDefault(nSCTHoles,0);
  setDefault(nSCTSharedHits,0);
  setDefault(nSCTOutliers,0);
  setDefault(nTRTHits,0);
  setDefault(nPixSpoiltHits,0);
  setDefault(type,-1);
  setDefault(origin,-1);
  setDefault(etcone20Topo,-1);
  setDefault(etcone30Topo,-1);
  setDefault(etcone40Topo,-1);
  setDefault(etclus20Topo,-1);
  setDefault(etclus30Topo,-1);
  setDefault(etclus40Topo,-1);
  setDefault(fitQuality,-1);
  setDefault(truthProVtx,-1);
  setDefault(vtxQuality,-1);
  setDefault(d0SV,-1);
  setDefault(z0SV,-1);
  setDefault(ptSV,-1);
  setDefault(etaSV,-1);
  setDefault(phiSV,-1);
  setDefault(d0ErrSV,-1);
  setDefault(z0ErrSV,-1);
  setDefault(pErrSV,-1);
  setDefault(truth_d0SV,1);
  setDefault(truth_z0SV,-1);
 

 
  //
  m_associatedTrack.clear();

}
// -------------------------------------------------------------------------------- //
TrackVariable::~TrackVariable()
{
 
  std::vector<TrackVariable*>::iterator iter;
  for (iter = m_associatedTrack.begin(); iter != m_associatedTrack.end(); iter++) {
    delete *iter;
  }

}
// -------------------------------------------------------------------------------- //
TrackVariable::TrackVariable(const TrackVariable &rhs):
  ObjectVariable       (rhs                      ),
  trackType            (rhs.trackType            ),
  isTight              (rhs.isTight              ),
  isLoose              (rhs.isLoose              ),
  q                    (rhs.q                    ),
  d0                   (rhs.d0                   ),
  z0                   (rhs.z0                   ),
  d0Err                (rhs.d0Err                ),
  z0Err                (rhs.z0Err                ),
  ptcone20             (rhs.ptcone20             ),
  ptcone30             (rhs.ptcone30             ),
  ptcone40             (rhs.ptcone40             ),
  nIBLHits             (rhs.nIBLHits             ),
  nPixLayers           (rhs.nPixLayers           ),
  nExpBLayerHits       (rhs.nExpBLayerHits       ),
  nPixHits             (rhs.nPixHits             ),
  nPixHoles            (rhs.nPixHoles            ),
  nPixOutliers         (rhs.nPixOutliers         ),
  nSCTHits             (rhs.nSCTHits             ),
  nSCTHoles            (rhs.nSCTHoles            ),
  nSCTSharedHits       (rhs.nSCTSharedHits       ),
  nSCTOutliers         (rhs.nSCTOutliers         ),
  nTRTHits             (rhs.nTRTHits             ),
  nPixSpoiltHits       (rhs.nPixSpoiltHits       ),
  nGangedFlaggedFakes  (rhs.nGangedFlaggedFakes  ),
  type                 (rhs.type                 ),
  origin               (rhs.origin               ),
  etcone20Topo         (rhs.etcone20Topo         ),
  etcone30Topo         (rhs.etcone30Topo         ),
  etcone40Topo         (rhs.etcone40Topo         ),
  etclus20Topo         (rhs.etclus20Topo         ),
  etclus30Topo         (rhs.etclus30Topo         ),
  etclus40Topo         (rhs.etclus40Topo         ),
  fitQuality           (rhs.fitQuality           ),
  truthProVtx          (rhs.truthProVtx          ),
  vtx                  (rhs.vtx                  ),
  vtxQuality           (rhs.vtxQuality           ),
  d0SV                 (rhs.d0SV                 ),
  z0SV                 (rhs.z0SV                 ),
  ptSV                 (rhs.ptSV                 ),
  etaSV                (rhs.etaSV                ),
  phiSV                (rhs.phiSV                ),
  d0ErrSV              (rhs.d0ErrSV              ),
  z0ErrSV              (rhs.z0ErrSV              ),
  pErrSV               (rhs.pErrSV               ),
  truth_d0SV           (rhs.truth_d0SV           ),
  truth_z0SV           (rhs.truth_z0SV           ),
  caloCluster          (rhs.caloCluster          ),
  truthLink            (rhs.truthLink            ),
  m_associatedTrack    (rhs.m_associatedTrack    )

{

}
// -------------------------------------------------------------------------------- //
TrackVariable& TrackVariable::operator=(const TrackVariable &rhs)
{

  if (this != &rhs) {
    ObjectVariable::operator=(rhs);
    trackType             = rhs.trackType;
    isTight               = rhs.isTight;
    isLoose               = rhs.isLoose;
    q                     = rhs.q;
    d0                    = rhs.d0;
    z0                    = rhs.z0;
    d0Err                 = rhs.d0Err;
    z0Err                 = rhs.z0Err;
    ptcone20              = rhs.ptcone20;
    ptcone30              = rhs.ptcone30;
    ptcone40              = rhs.ptcone40;
    nIBLHits              = rhs.nIBLHits;
    nPixLayers            = rhs.nPixLayers;
    nExpBLayerHits        = rhs.nExpBLayerHits;
    nPixHits              = rhs.nPixHits;
    nPixHoles             = rhs.nPixHoles;
    nPixOutliers          = rhs.nPixOutliers;
    nSCTHits              = rhs.nSCTHits;
    nSCTHoles             = rhs.nSCTHoles;
    nSCTSharedHits        = rhs.nSCTSharedHits;
    nSCTOutliers          = rhs.nSCTOutliers;
    nTRTHits              = rhs.nTRTHits;
    nPixSpoiltHits        = rhs.nPixSpoiltHits;
    nGangedFlaggedFakes   = rhs.nGangedFlaggedFakes;
    type                  = rhs.type;
    origin                = rhs.origin;
    etcone20Topo          = rhs.etcone20Topo;
    etcone30Topo          = rhs.etcone30Topo;
    etcone40Topo          = rhs.etcone40Topo;
    etclus20Topo          = rhs.etclus20Topo;
    etclus30Topo          = rhs.etclus30Topo;
    etclus40Topo          = rhs.etclus40Topo;
    fitQuality            = rhs.fitQuality;
    truthProVtx           = rhs.truthProVtx;
    vtx                   = rhs.vtx;
    vtxQuality            = rhs.vtxQuality;
    d0SV                  = rhs.d0SV;
    z0SV                  = rhs.z0SV;
    ptSV                  = rhs.ptSV;
    etaSV                 = rhs.etaSV;
    phiSV                 = rhs.phiSV;
    d0ErrSV               = rhs.d0ErrSV;
    z0ErrSV               = rhs.z0ErrSV;
    pErrSV                = rhs.pErrSV;
    truth_d0SV            = rhs.truth_d0SV;
    truth_z0SV            = rhs.truth_z0SV;
    caloCluster           = rhs.caloCluster;
    truthLink             = rhs.truthLink;
    m_associatedTrack     = rhs.m_associatedTrack;
  }

  return *this;

}
// -------------------------------------------------------------------------------- //
TrackVariable* TrackVariable::makeAssociatedTrack()
{

  TrackVariable* trk = new TrackVariable();
  m_associatedTrack.push_back(trk);

  return trk;

}
// -------------------------------------------------------------------------------- //
TrackVariable* TrackVariable::getAssociatedTrack(int idx)
{

  if( idx>this->nAssociatedTracks() ){
    MsgLog::ERROR("TrackVariable::getAssociatedTrack","Invalid index %i. Returning a NULL object",idx);
    return 0;
  }

  //
  return m_associatedTrack[idx];

}
// -------------------------------------------------------------------------------- //
void TrackVariable::print() const
{

    MsgLog::INFO("TrackVariable::print","Track pT %f eta %f and phi %f",this->Pt(), this->Eta(), this->Phi() );
    MsgLog::INFO("TrackVariable::print","Track d0 %f +/- %f and z0 %f +/- %f",this->d0, this->d0Err, this->z0, this->z0Err );
    MsgLog::INFO("TrackVariable::print","Track d0Sig %f and z0*sin(theta) %f",abs(this->d0/this->d0Err), this->z0*sin(this->Theta()) );
    MsgLog::INFO("TrackVariable::print","Track number of TRT hits %i",this->nTRTHits);
    MsgLog::INFO("TrackVariable::print","Track number of pixel hits %i, holes %i, and outliers hits %i",this->nPixHits, this->nPixHoles,this->nPixOutliers);
    MsgLog::INFO("TrackVariable::print","Track number of pixel layers %i and b-layer hits %i",this->nPixLayers,this->nExpBLayerHits);
    MsgLog::INFO("TrackVariable::print","Track number of SCT hits %i, holes %i, shared hits %i and outliers hits %i",this->nSCTHits, this->nSCTHoles,this->nSCTSharedHits,this->nSCTOutliers);
    MsgLog::INFO("TrackVariable::print","Track ptcone20 %f, ptcone30 %f, and ptcone40 %f",this->ptcone20,this->ptcone30,this->ptcone40);
    MsgLog::INFO("TrackVariable::print","Track fit quality %f",this->fitQuality);
    MsgLog::INFO("TrackVariable::print","Track vertex associated radius %f and quality %f",this->vtx.Perp(),this->vtxQuality);
    MsgLog::INFO("TrackVariable::print","Track d0 %f +/- %f and z0 %f +/- %f at the secondary vertex",this->d0SV,this->d0ErrSV,this->z0SV,this->z0ErrSV);
    MsgLog::INFO("TrackVariable::print","Track pT %f eta %f and phi %f at the secondary vertex",this->ptSV,this->etaSV,this->phiSV);
    MsgLog::INFO("TrackVariable::print","Truth type %i and origin  %i",this->type,this->origin);

}
