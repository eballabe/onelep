#include "SusySkimMaker/CutFlowTool.h"
#include "TH1.h"

// ----------------------------------------------------------------------------------- //
CutFlowTool::CutFlowTool()
{
  m_sysList.clear();
}
// ----------------------------------------------------------------------------------- //
std::vector<std::pair<TH1*,TH1*>> CutFlowTool::mergeCutFlows(TString stream_one, TString stream_two)
{

 // TODO: Small memory problems here, probabaly don't need the list, just pass the TDirectory of the file we want to write out

  std::vector<std::pair<TH1*,TH1*>> list;

  // Define a cutflow for each systematic
  for(unsigned int i=0; i<m_sysList.size(); i++){

    setSysState( m_sysList[i] );

    TString merged_weighted   = "merged_weighted_" + getWeightedName(stream_one) + "_" + getWeightedName(stream_two);
    TString merged_unweighted = "merged_unweighted_" + getUnWeightedName(stream_one) + "_" + getUnWeightedName(stream_two);

    TH1F* h_one_unweighted    = GetCutFlowHist( getUnWeightedName(stream_one) );
    TH1F* h_one_weighted      = GetCutFlowHist( getWeightedName(stream_one) );

    TH1F* h_two_unweighted    = GetCutFlowHist( getUnWeightedName(stream_two) );
    TH1F* h_two_weighted      = GetCutFlowHist( getWeightedName(stream_two) );


    int nOne = h_one_unweighted->GetNbinsX();
    int nBins = nOne + h_two_unweighted->GetNbinsX();

    TH1F* h_merged_unweighted = new TH1F(merged_unweighted,merged_unweighted,nBins+1,0,nBins+1);
    TH1F* h_merged_weighted   = new TH1F(merged_weighted,merged_weighted,nBins+1,0,nBins+1);

    // Unweighted copy of histograms
    int lastBin_unweighted = 0;
    for(int i=1; i<=h_one_unweighted->GetNbinsX(); i++){
      TString label = h_one_unweighted->GetXaxis()->GetBinLabel(i);
      //if(h_one_unweighted->GetBinContent(i)>0){
      if( !label.IsWhitespace() ){
        h_merged_unweighted->GetXaxis()->SetBinLabel(i,label);
        h_merged_unweighted->SetBinContent(i,h_one_unweighted->GetBinContent(i));
      }
      else{
        lastBin_unweighted = i - 1;
        break;
      }
    }
    for(int i=1; i<=h_two_unweighted->GetNbinsX(); i++){
      h_merged_unweighted->GetXaxis()->SetBinLabel(i+lastBin_unweighted,h_two_unweighted->GetXaxis()->GetBinLabel(i));
      h_merged_unweighted->SetBinContent(i+lastBin_unweighted,h_two_unweighted->GetBinContent(i));
    }

    // Weighted copy
    int lastBin_weighted = 0;
    for(int i=1; i<=h_one_weighted->GetNbinsX(); i++){
      TString label = h_one_weighted->GetXaxis()->GetBinLabel(i);
      //if(h_one_weighted->GetBinContent(i)>0){
      if( !label.IsWhitespace() ){
        h_merged_weighted->GetXaxis()->SetBinLabel(i,label);
        h_merged_weighted->SetBinContent(i,h_one_weighted->GetBinContent(i));
      }
      else{
        lastBin_weighted = i - 1 ;
        break;
      }
    }
    for(int i=1; i<=h_two_weighted->GetNbinsX(); i++){
      h_merged_weighted->GetXaxis()->SetBinLabel(i+lastBin_weighted,h_two_weighted->GetXaxis()->GetBinLabel(i));
      h_merged_weighted->SetBinContent(i+lastBin_weighted,h_two_weighted->GetBinContent(i));
    }

    list.push_back(std::make_pair<TH1*,TH1*>(h_merged_weighted,h_merged_unweighted));

  }

  return list;

}
// ----------------------------------------------------------------------------------- //
void CutFlowTool::defineCutFlow(TString stream, TFile* outputFile,bool unweightedHist, TString sys)
{

  /**
   * This function leaves m_sysState in the last state set by the loop
   */ 

  std::vector<TString> sysList;
  sysList.clear();

  if( sys.IsWhitespace() ){
    if(m_sysList.size()==0) sysList.push_back("");
    else                    sysList = m_sysList;
  }
  else{
    sysList.push_back(sys);
  }

  // Define a cutflow for each systematic
  for(unsigned int i=0; i<sysList.size(); i++){

    // Set global systematic state
    setSysState( sysList[i] );

    // Define two cutflows for weighted and unweighted number of events
    TString stream_weighted   = getWeightedName(stream); 
    TH1F* h_cutflow_weighted   = new TH1F(stream_weighted  ,"",20,0,20);
    h_cutflow_weighted->Reset();

    //h_cutflow_weighted->SetBit(TH1::kCanRebin);
    h_cutflow_weighted->SetDirectory(outputFile);

    // Store MC stat errors
    h_cutflow_weighted->Sumw2();

    m_cutFlowMap.insert(std::make_pair(stream_weighted,   h_cutflow_weighted));

    // Unweighted histogram, if the user requests it (default is true)
    if(unweightedHist){
      TString stream_unweighted = getUnWeightedName(stream); 
      TH1F* h_cutflow_unweighted = new TH1F(stream_unweighted,"",20,0,20);
      h_cutflow_unweighted->Reset();
      //h_cutflow_unweighted->SetBit(TH1::kCanRebin);
      h_cutflow_unweighted->SetDirectory(outputFile);
      m_cutFlowMap.insert(std::make_pair(stream_unweighted, h_cutflow_unweighted));
    }
  } // Loop over systematics

}
// ----------------------------------------------------------------------------------- //
void CutFlowTool::bookCut(TString stream,TString cutName,double weight,bool initialize)
{

  double unweighted = 1.0;

  // User wants to make the bins, but don't fill them with any values
  if(initialize){
    weight      = 0.0;
    unweighted  = 0.0;
  }

  // For weighted Histgram
  EventFill( getWeightedName(stream)  , cutName, weight);
  // For unweighted Histgram
  EventFill( getUnWeightedName(stream), cutName, unweighted);

}
// ----------------------------------------------------------------------------------- //
void CutFlowTool::EventFill(TString stream_full,TString cutName,double weight)
{

  std::map<TString,TH1F*>::iterator it = m_cutFlowMap.find(stream_full);

  // Nothing found
  if( it == m_cutFlowMap.end() ){

/*
    std::cout << " <CutFlowTool::bookCut> WARNING::No stream found in cutflow map " << stream_full << std::endl;
    std::cout << " <CutFlowTool::bookCut> This what exists in the cut flow map..." << std::endl;
    for(it = m_cutFlowMap.begin(); it != m_cutFlowMap.end(); it++){
      std::cout << "   => " << it->first << std::endl;
    }
*/

    return;
  }
  else{

    // This will find the bin with label cutName. If it doesn't exist,
    // it will add the bin label
    //Int_t bin = it->second->GetXaxis()->FindBin( cutName.Data() );
    
    // // Increment bin content
    //it->second->AddBinContent(bin,weight);

    // use Fill() instead of AddBinContent(), since the latter doesn't properly
    // register new entries which leads to problem later when hadding
    it->second->Fill(cutName.Data(), weight);
    
  }

}
// ----------------------------------------------------------------------------------- //
TH1F* CutFlowTool::GetCutFlowHist(TString stream_full)
{

  // stream_full means that this method is being passed the full stream name, 
  // ie processed through getWeightedName(), etc

  // Get cutflow histogram
  std::map<TString,TH1F*>::iterator it = m_cutFlowMap.find(stream_full);

  // Nothing found
  if( it == m_cutFlowMap.end() ){
    std::cout << " < CutFlowTool::GetCutFlowHist> WARNING::No stream found in cutflow map " << stream_full << std::endl;
    TH1F* cutFlowtmp = new TH1F("NOTFOUND","",20,0,20);
    return cutFlowtmp;
  }

  TH1F* cutFlowHist = it->second;
  return cutFlowHist;

}
// ----------------------------------------------------------------------------------- //
void CutFlowTool::Write(){
  
  std::map<TString,TH1F*>::iterator it;
  for(it = m_cutFlowMap.begin(); it != m_cutFlowMap.end(); it++){
    std::cout << "  => CutFlowTool::Writing CutFlowHistgram " << it->first << std::endl;
    it->second->Write();
  }
}
// ----------------------------------------------------------------------------------- //
void CutFlowTool::Print(TString stream)
{

  std::map<TString,TH1F*>::iterator it1 = m_cutFlowMap.find( getWeightedName(stream) );
  std::map<TString,TH1F*>::iterator it2 = m_cutFlowMap.find( getUnWeightedName(stream) );

  // Nothing found
  if( it1 == m_cutFlowMap.end() && it2 == m_cutFlowMap.end() ){
    // Print all streams in m_CutFlowMap
    std::map<TString,TH1F*>::iterator it_all;
    for(it_all = m_cutFlowMap.begin(); it_all != m_cutFlowMap.end(); it_all++) PrintHist(it_all->first,it_all->second);
    //
  }
  // Print the requested stream
  else{
    if(it1 != m_cutFlowMap.end()) PrintHist(it1->first,it1->second);
    if(it2 != m_cutFlowMap.end()) PrintHist(it2->first,it2->second);
  }

  return;

}
// ----------------------------------------------------------------------------------- //
void CutFlowTool::PrintHist(TString name,TH1F* hist)
{

  if( !hist ) return;

  std::cout << " <CutFlowTool::Print> Number of Event in CutFlowHistgram " << name << std::endl;
  std::cout << std::endl;

  for(int b = 1; b<=hist->GetNbinsX(); b++){
    if(strlen(hist->GetXaxis()->GetBinLabel(b))==0) continue;
    std::cout << " => " << std::left << std::setw(60) << hist->GetXaxis()->GetBinLabel(b) << std::setw(10) <<  hist->GetBinContent(b)  << std::endl;
  }
  
  std::cout << std::endl;

}
// ----------------------------------------------------------------------------------- //
TString CutFlowTool::getWeightedName(TString stream)
{
  return stream+"__"+m_sysState+"_weighted";
}
// ----------------------------------------------------------------------------------- //
TString CutFlowTool::getUnWeightedName(TString stream)
{
  return stream+"__"+m_sysState+"_unweighted";
}
// ----------------------------------------------------------------------------------- //
