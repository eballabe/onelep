#include "SusySkimMaker/MetaData.h"
#include <sstream> 
#include <stdio.h>
#include "TSystem.h"

MetaData::MetaData()
{

}
// ---------------------------------------------------------------------------- //
MetaData::MetaData(const MetaData& rhs) :
  TObject(rhs),
  stringMetaData(rhs.stringMetaData),
  sherpaOverlapEvents(rhs.sherpaOverlapEvents)
{
}
// ---------------------------------------------------------------------------- //
MetaData& MetaData::operator=(const MetaData& rhs)
{

 if (this != &rhs) {
   stringMetaData       = rhs.stringMetaData;
   sherpaOverlapEvents  = rhs.sherpaOverlapEvents;
  }
  return *this;

}
// ---------------------------------------------------------------------------- //
bool MetaData::compare(const MetaData* metaData)
{

  // Loop over all internal fields. Check they are the same
  // Return false anytime one is not found
  // Everything is TString

  // TODO: If the meta data is not found in the one pass, don't return false,
  // i.e. only compare to what this* contains

  std::cout << "This method has not yet been developed..." << std::endl;

  metaData->Print();

  // They are the same
  return true;

}
// ---------------------------------------------------------------------------- //
void MetaData::log(TString className,TString description,bool value)
{

  std::stringstream ss;
  ss << value;

  // Call string log
  log(className,description,TString(ss.str()));

}
// ---------------------------------------------------------------------------- //
void MetaData::log(TString className,TString description,float value)
{

  std::stringstream ss;
  ss << value;

  // Call string log
  log(className,description,TString(ss.str()));
  
}
// ---------------------------------------------------------------------------- //
void MetaData::log(TString className,TString description,int value)
{

  std::stringstream ss;
  ss << value;

  // Call string log
  log(className,description,TString(ss.str()));
  
}
// ---------------------------------------------------------------------------- //
void MetaData::log(TString className,TString description,TString value)
{

  TString value_clone = value;

  // Remove $ROOTCOREBIN -> depends on cluster
  if( value_clone.Contains( gSystem->ExpandPathName("$ROOTCOREBIN/") ) ){
    value_clone.ReplaceAll( gSystem->ExpandPathName("$ROOTCOREBIN/"), "" );
  }

  auto myClass = stringMetaData.find(className);

  if( myClass != stringMetaData.end() ){
    myClass->second.push_back( std::pair<TString,TString>(description,value_clone) );
  } 
  else{
    std::vector<std::pair<TString,TString>> val;
    val.push_back( std::pair<TString,TString>(description,value_clone) );
    stringMetaData.insert( std::pair<TString, std::vector<std::pair<TString,TString>> >(className,val) );
  }

}
// ---------------------------------------------------------------------------- //
void MetaData::print() const
{

  std::cout << std::endl;
  for( auto& it : stringMetaData ){
    //
    std::cout << "Meta data for class : " << it.first << std::endl;
    //
    for( auto& p : it.second ){
       printf ("   >> %-60s :  %-110s\n",p.first.Data(),p.second.Data() );
    }
  }
  std::cout << std::endl;

}
