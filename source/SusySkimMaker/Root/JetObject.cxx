#include "SusySkimMaker/JetObject.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "SusySkimMaker/Timer.h"
#include "SusySkimMaker/MsgLog.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "PathResolver/PathResolver.h"

using namespace MCTruthPartClassifier;

JetObject::JetObject() : m_truthClassifier(0), 
                         m_doTruthBtaggingEmulation(false),
                         m_writeDetailedSkim(false),
                         m_doBTagging(true),
                         m_doBTagScores(false),
                         m_ContinuousBtagScore(-2),
                         m_DL1rContainerName(""),
                         m_DL1dv01ContainerName("AntiKt4EMPFlowJets"),
                         m_DL1rContainerName_trkJet("AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903"),
                         m_DL1dv01ContainerName_trkJet("AntiKtVR30Rmax4Rmin02TrackJets"),
                         m_DRThresh_fatjet_trackjets(1.0),
                         m_bTagSelTool_DL1r(0),
                         m_bTagSelTool_DL1dv01(0),
                         m_bTagSelTool_trkJet_DL1r(0),
                         m_bTagSelTool_trkJet_DL1dv01(0),
                         m_WTaggerTool_Eff50(0),
                         m_ZTaggerTool_Eff50(0),
                         m_WTaggerTool_InclEff50(0),
                         m_ZTaggerTool_InclEff50(0),
                         m_WTaggerTool_Eff80(0),
                         m_ZTaggerTool_Eff80(0),
                         m_isData(false)
{
  
}

// ------------------------------------------------------------------------- //
JetObject::~JetObject(){

  Reset();

  if (m_btagEffTool)               delete m_btagEffTool;
  if (m_rnd)                       delete m_rnd;
  //if(m_bTagSelTool_MV2c10)        delete m_bTagSelTool_MV2c10;
  if(m_bTagSelTool_DL1r)           delete m_bTagSelTool_DL1r;
  if(m_bTagSelTool_DL1dv01)        delete m_bTagSelTool_DL1dv01;
  //if(m_bTagSelTool_DL1)           delete m_bTagSelTool_DL1;
  //if(m_bTagSelTool_trkJet_MV2c10) delete m_bTagSelTool_trkJet_MV2c10;
  if(m_bTagSelTool_trkJet_DL1r)    delete m_bTagSelTool_trkJet_DL1r;
  if(m_bTagSelTool_trkJet_DL1dv01) delete m_bTagSelTool_trkJet_DL1dv01;
  //if(m_bTagSelTool_trkJet_DL1)    delete m_bTagSelTool_trkJet_DL1;
  if(m_WTaggerTool_Eff50)          delete m_WTaggerTool_Eff50;
  if(m_ZTaggerTool_Eff50)          delete m_ZTaggerTool_Eff50;
  if(m_WTaggerTool_InclEff50)      delete m_WTaggerTool_InclEff50;
  if(m_ZTaggerTool_InclEff50)      delete m_ZTaggerTool_InclEff50;
  if(m_WTaggerTool_Eff80)          delete m_WTaggerTool_Eff80;
  if(m_ZTaggerTool_Eff80)          delete m_ZTaggerTool_Eff80;
  if(m_truthClassifier)            delete m_truthClassifier;

}

// ------------------------------------------------------------------------- //
StatusCode JetObject::init(TreeMaker*& treeMaker, bool isData, TString jetType/*="jets"*/)
{

  const char* APP_NAME = "JetObject";

  m_isData = isData;

  std::vector<TString> sysNames = treeMaker->getSysVector();

  for(unsigned int s=0; s<sysNames.size(); s++){
    TString sysName = sysNames[s];
    JetVector* mu  = new JetVector();
    // Save jet vector into map
    m_jetVectorMap.insert( std::pair<TString,JetVector*>(sysName,mu) );
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      MsgLog::INFO("JetObject::init","Adding a branch jets to skim tree: %s",sysTree->GetName() );
      std::map<TString,JetVector*>::iterator jetItr = m_jetVectorMap.find(sysName);
      sysTree->Branch(jetType,&jetItr->second);
    }
  }

  //
  CHECK( init_tools(isData,jetType) );

  MsgLog::INFO("JetObject::init","Successfully initialized JetObject class!");

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode JetObject::init_tools(bool isData, TString jetType/*="jets"*/)
{

  // MC truth classifier
  m_truthClassifier = new MCTruthClassifier("Jet_MCTruth");

  bool m_isrun3 = false;
  bool m_doFatJet = false;
  CentralDB::retrieve(CentralDBFields::ISRUN3,m_isrun3);
  CentralDB::retrieve(CentralDBFields::EMULATEBTAG,m_doTruthBtaggingEmulation);
  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);
  CentralDB::retrieve(CentralDBFields::WRITEDETAILEDSKIM,m_writeDetailedSkim);
  CentralDB::retrieve(CentralDBFields::DOBTAGGING,m_doBTagging);
  CentralDB::retrieve(CentralDBFields::DOBTAGSCORES,m_doBTagScores);
  CentralDB::retrieve(CentralDBFields::CONTINUOUSBTAGSCORE, m_ContinuousBtagScore);
  //CentralDB::retrieve(CentralDBFields::MV2C10CONTAINERNAME,m_MV2c10ContainerName);
  CentralDB::retrieve(CentralDBFields::DL1RCONTAINERNAME,m_DL1rContainerName);
  CentralDB::retrieve(CentralDBFields::DL1DV01CONTAINERNAME,m_DL1dv01ContainerName);

  //CentralDB::retrieve(CentralDBFields::MV2C10CONTAINERNAME_TRKJET,m_MV2c10ContainerName_trkJet);
  CentralDB::retrieve(CentralDBFields::DL1RCONTAINERNAME_TRKJET,m_DL1rContainerName_trkJet);
  CentralDB::retrieve(CentralDBFields::DL1DV01CONTAINERNAME_TRKJET,m_DL1dv01ContainerName_trkJet);
  CentralDB::retrieve(CentralDBFields::DRTHRESH_FATJET_TRACKJETS,m_DRThresh_fatjet_trackjets);
  CentralDB::retrieve(CentralDBFields::DOFATJET,m_doFatJet);

  //
  // BTaggingSelectionTool for calculating btagging weights
  //
  // NOTE: These are separately defined from SUSYTools.
  //       Make sure you have the consistent JetAuthor configuration as the ST,
  //       and you DAOD does have such containers in it.
  //       Example and instruction can be found in CentralDBFields.h 
  //

  if(m_doBTagging && m_doBTagScores){ 
    
    // Dummy config file and WP (they can be just arbitrary but make sure the calib file and WP do exist.)
    
    // Config file used for "outdated" working points (i.e. mv2c10, dl1r, dl1)
    std::string dummyConfig;
    if(m_isrun3){dummyConfig = "xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root";}
    else{dummyConfig = "xAODBTaggingEfficiency/13TeV/2023-22-13TeV-MC20-CDI-2023-09-13_v1.root";}
    std::string dummyWP = "FixedCutBEff_85";

    //std::cout<<"m_MV2c10ContainerName = "<<m_MV2c10ContainerName<<std::endl;
    
    // EMPFlow jets
    if(jetType=="jets"){
      
      // DL1dv01 for EMPFlow jets
      if(!m_DL1dv01ContainerName.IsWhitespace()){
        m_bTagSelTool_DL1dv01 = new BTaggingSelectionTool("BTagSel_DL1dv01");
        CHECK( m_bTagSelTool_DL1dv01->setProperty("FlvTagCutDefinitionsFileName", dummyConfig) );
        CHECK( m_bTagSelTool_DL1dv01->setProperty("TaggerName",    "DL1dv01"  ) );
        CHECK( m_bTagSelTool_DL1dv01->setProperty("OperatingPoint", dummyWP) ); // dummy WP
        CHECK( m_bTagSelTool_DL1dv01->setProperty("JetAuthor",      m_DL1dv01ContainerName.Data()) );
        CHECK( m_bTagSelTool_DL1dv01->setProperty("MinPt",      20e3 ) );    
        if( !m_bTagSelTool_DL1dv01->initialize() ){
          MsgLog::ERROR("JetObject::init_tools", "Initialization of tool BTagSel_DL1dv01 failed!");
          return StatusCode::FAILURE;
        }
      }
    }
    
    if(m_doFatJet){
      MsgLog::INFO("JetObject::init_tools","jetType==fatjets");
      // 3-var 80% WP
      m_WTaggerTool_Eff80 = new SmoothedWZTagger("SmoothedWZTagger/WTagger_Eff80");
      CHECK( m_WTaggerTool_Eff80->setProperty("ConfigFile", "SmoothedContainedWTagger_AntiKt10UFOCSSKSoftDrop_FixedSignalEfficiency80_20220221.dat") ); 
      CHECK( m_WTaggerTool_Eff80->setProperty("CalibArea", "Winter2024_R22_PreRecs/SmoothedWZTaggers/") );
      CHECK( m_WTaggerTool_Eff80->setProperty("IsMC", isData ? false : true) );
      if( !m_WTaggerTool_Eff80->initialize() ){
        MsgLog::ERROR("JetObject::init_tools", "Initialization of tool SmoothedWZTagger/WTagger failed!");
        return StatusCode::FAILURE;
      }
    
      m_ZTaggerTool_Eff80 = new SmoothedWZTagger("SmoothedWZTagger/ZTagger_Eff80");
      CHECK( m_ZTaggerTool_Eff80->setProperty("ConfigFile", "SmoothedContainedZTagger_AntiKt10UFOCSSKSoftDrop_FixedSignalEfficiency80_20220221.dat") ); 
      CHECK( m_ZTaggerTool_Eff80->setProperty("CalibArea", "Winter2024_R22_PreRecs/SmoothedWZTaggers/") );
      CHECK( m_ZTaggerTool_Eff80->setProperty("IsMC", isData ? false : true) );
      if( !m_ZTaggerTool_Eff80->initialize() ){
        MsgLog::ERROR("JetObject::init_tools", "Initialization of tool SmoothedWZTagger/ZTagger failed!");
        return StatusCode::FAILURE;
      }    
    }
     
  }
  

  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
void JetObject::fillJetContainer(xAOD::Jet* jet_xAOD,const xAOD::VertexContainer* vertices,
                                 std::string sys_name,std::map<TString,float> FT_SF, 
                                 std::map<TString,float> JVT_SF,std::map<TString,bool> trigMap)
{

  Timer::Instance()->Start( "JetObject::fillJetContainer" );

  std::map<TString,JetVector*>::iterator it = m_jetVectorMap.find(sys_name);

  if( it==m_jetVectorMap.end() ){
    MsgLog::ERROR("JetObject::fillJetContainer","Request to get jet for unknown systematic: %s",sys_name.c_str() );
    return;
  } 

  JetVariable* jet = new JetVariable();

  // TLV
  jet->SetPtEtaPhiM(jet_xAOD->pt() * m_convertFromMeV,
                    jet_xAOD->eta(),
                    jet_xAOD->phi(),
                    jet_xAOD->m() * m_convertFromMeV );

  // Fill b-tagging information
  fillBTaggingInfo(jet_xAOD,jet);

  // Weight maps
  jet->FT_SF   = FT_SF;
  jet->JVT_SF  = JVT_SF;

  // Jet classification, from SUSYTools
  jet->signal = cacc_signal( *jet_xAOD ); 
  jet->bad    = cacc_bad( *jet_xAOD );
  jet->passOR = cacc_passOR( *jet_xAOD );

  // JVT, recalculated in SUSYTools
  jet->jvt = cacc_Jvt.isAvailable(*jet_xAOD) ? cacc_Jvt( *jet_xAOD ) : 1.;

  // Trigger decision
  jet->triggerMap = trigMap;

  // Truth information
  jet->truthLabel = cacc_PartonTruthLabelID.isAvailable( *jet_xAOD ) ? 
    cacc_PartonTruthLabelID( *jet_xAOD ) : 0;

  // nTrk
  jet->nTrk = getNumTrkPt500(jet_xAOD,vertices);

  // For calculating the tile energy
  std::vector<float> jetEnergyPerSampling = {};
  jet_xAOD->getAttribute(xAOD::JetAttribute::EnergyPerSampling, jetEnergyPerSampling);
  
  //https://twiki.cern.ch/twiki/bin/view/AtlasProtected/Run2JetMoments#Sampling_layers
  float tileEnergy = jetEnergyPerSampling[CaloSampling::TileBar0];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileBar1];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileBar2];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileGap1];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileGap2];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileGap3];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileExt0];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileExt1];
  tileEnergy      += jetEnergyPerSampling[CaloSampling::TileExt2];

  jet->tileEnergy = tileEnergy;

  // Detailed jet information
  fillDetailedJetVariables(jet_xAOD,jet);
  
  // Truth information
  const xAOD::Jet* truthJet = getLink<xAOD::Jet>( jet_xAOD, "GhostTruthAssociationLink" );

  if( truthJet ){
    jet->truthTLV.SetPtEtaPhiM( truthJet->pt() * m_convertFromMeV,
                                truthJet->eta(),
                                truthJet->phi(),
                                truthJet->m() * m_convertFromMeV );

    // What to do here??
    jet->barcode = 0;

  }

  // Save
  it->second->push_back(jet);

  Timer::Instance()->End( "JetObject::fillJetContainer" );

}
// ------------------------------------------------------------------------- //
void JetObject::fillDetailedJetVariables(xAOD::Jet* jet_xAOD,JetVariable*& jet)
{

  // Don't fill unless user wants
  if( !m_writeDetailedSkim ) return;
  
  float tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::EMFrac,tmp) ;
  jet-> EMFrac = tmp;
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::HECFrac,tmp) ;
  jet-> HECFrac= tmp;
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::LArQuality,tmp) ;
  jet-> LArQuality= tmp;
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::HECQuality,tmp) ;
  jet-> HECQuality= tmp;
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::Timing,tmp) ;
  jet-> Timing= tmp;
  tmp=0;
  
  std::vector<float> sumPtTrkvec;
  jet_xAOD->getAttribute( xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec );
  double sumpttrk = 0;
  if( ! sumPtTrkvec.empty() ) sumpttrk = sumPtTrkvec[0];
  
  jet-> sumpttrk= sumpttrk;
  
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::FracSamplingMax,tmp) ;
  jet-> FracSamplingMax= tmp;
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::NegativeE,tmp) ;
  jet-> NegativeE= tmp;
  tmp=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::AverageLArQF,tmp) ;
  jet-> AverageLArQF= tmp;
  int tmpint=0;
  jet_xAOD->getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex,tmpint) ;
  jet-> FracSamplingMaxIndex= tmpint;
  tmp=0;                                                  
  jet_xAOD->getAttribute(xAOD::JetAttribute::Width,tmp) ; 
  jet->Width= tmp;                                        

}
// ------------------------------------------------------------------------- //
void JetObject::fillFatjetContainer(xAOD::Jet* fatjet_xAOD,
                                    const xAOD::JetContainer* trackjets_xAOD,
                                    const xAOD::VertexContainer* vertices,
                                    std::string sys_name)
{

  Timer::Instance()->Start( "JetObject::fillFatjetContainer" );

  std::map<TString,JetVector*>::iterator it = m_jetVectorMap.find(sys_name);

  if( it==m_jetVectorMap.end() ){
    MsgLog::ERROR("JetObject::fillFatjetContainer","Request to get fatjet for unknown systematic: %s",sys_name.c_str() );
    return;
  } 

  JetVariable* fatjet = new JetVariable();

  // TLV
  fatjet->SetPtEtaPhiM(fatjet_xAOD->pt() * m_convertFromMeV,
                    fatjet_xAOD->eta(),
                    fatjet_xAOD->phi(),
                    fatjet_xAOD->m() * m_convertFromMeV );

  // Number of tracks in the ungloomed jet
  fatjet->nTrk = getFatjetNumTrkPt500(fatjet_xAOD,vertices);

  // Fill b-tagging information
  //if( !fillFatjetBTaggingInfo(fatjet_xAOD,fatjet) )
  //fillFatjetBTaggingInfo_DRMatch(fatjet_xAOD, fatjet, trackjets_xAOD, m_DRThresh_fatjet_trackjets);
  fillFatjetBTaggingInfo(fatjet_xAOD,fatjet);

  // Jet classification
  fatjet->signal = cacc_signal.isAvailable( *fatjet_xAOD ) ? cacc_signal( *fatjet_xAOD ) : false;  

  // JVT, recalculated in SUSYTools
  fatjet->jvt = cacc_Jvt.isAvailable(*fatjet_xAOD) ? cacc_Jvt( *fatjet_xAOD ) : 1.;
  
  // Truth information
  fatjet->truthLabel = cacc_FatjetTruthLabel.isAvailable( *fatjet_xAOD ) ? cacc_FatjetTruthLabel( *fatjet_xAOD ) : -9; 

  //
  // Substructure
  //

  // Tau subjettiness
  float Tau1 = cacc_Tau1_wta.isAvailable(*fatjet_xAOD) ? cacc_Tau1_wta(*fatjet_xAOD) : 0.;
  float Tau2 = cacc_Tau2_wta.isAvailable(*fatjet_xAOD) ? cacc_Tau2_wta(*fatjet_xAOD) : 0.;
  float Tau3 = cacc_Tau3_wta.isAvailable(*fatjet_xAOD) ? cacc_Tau3_wta(*fatjet_xAOD) : 0.;
  fatjet->Tau21 = Tau1>0. ? Tau2/Tau1 : 0.;
  fatjet->Tau32 = Tau2>0. ? Tau3/Tau2 : 0.;

  // D2
  if(cacc_D2.isAvailable(*fatjet_xAOD))
    fatjet->D2 =  cacc_D2(*fatjet_xAOD) ;
  else{
    float ECF1 = cacc_ECF1.isAvailable(*fatjet_xAOD) ? cacc_ECF1(*fatjet_xAOD) : 0.;
    float ECF2 = cacc_ECF2.isAvailable(*fatjet_xAOD) ? cacc_ECF2(*fatjet_xAOD) : 0.;
    float ECF3 = cacc_ECF3.isAvailable(*fatjet_xAOD) ? cacc_ECF3(*fatjet_xAOD) : 0.;
    fatjet->D2 = ECF2!=0 ? ECF3*ECF1*ECF1*ECF1/ECF2/ECF2/ECF2 : -1.;
  }

  //
  // Xbb scores
  //
  fatjet->XbbScoreQCD   = cacc_XbbScoreQCD.isAvailable(*fatjet_xAOD)   ? cacc_XbbScoreQCD(*fatjet_xAOD)   : -999.;
  fatjet->XbbScoreTop   = cacc_XbbScoreTop.isAvailable(*fatjet_xAOD)   ? cacc_XbbScoreTop(*fatjet_xAOD)   : -999.;
  fatjet->XbbScoreHiggs = cacc_XbbScoreHiggs.isAvailable(*fatjet_xAOD) ? cacc_XbbScoreHiggs(*fatjet_xAOD) : -999.;

  //
  // Boson tagging (SF only provided for the inclusive 50% eff WP at the moment)
  //
  //const asg::AcceptData resW50 = m_WTaggerTool_Eff50->tag(*fatjet_xAOD);
  //CHECK(m_WTaggerTool_Eff50->tag(*fatjet_xAOD));
  //fatjet->passWTag50      = fatjet_xAOD->auxdata<bool>("SmoothWContained50_Tagged");
  //fatjet->passWTag50_mass = fatjet_xAOD->auxdata<bool>("SmoothWContained50_PassMassLow") && fatjet_xAOD->auxdata<bool>("SmoothWContained50_PassMassHigh");
  //fatjet->passWTag50_sub  = fatjet_xAOD->auxdata<bool>("SmoothWContained50_PassD2");
  //fatjet->passWTag50_nTrk = fatjet_xAOD->auxdata<bool>("SmoothWContained50_PassNtrk");
  //fatjet->bjt_wsf50       = fatjet_xAOD->auxdata<float>("SmoothWContained50_SF");
    
  //CHECK(m_ZTaggerTool_Eff50->tag(*fatjet_xAOD));
  //fatjet->passZTag50      = fatjet_xAOD->auxdata<bool>("SmoothZContained50_Tagged");
  //fatjet->passZTag50_mass = fatjet_xAOD->auxdata<bool>("SmoothZContained50_PassMassLow") && fatjet_xAOD->auxdata<bool>("SmoothZContained50_PassMassHigh");
  //fatjet->passZTag50_sub  = fatjet_xAOD->auxdata<bool>("SmoothZContained50_PassD2");
  //fatjet->passZTag50_nTrk = fatjet_xAOD->auxdata<bool>("SmoothZContained50_PassNtrk");
  //fatjet->bjt_zsf50       = fatjet_xAOD->auxdata<float>("SmoothZContained50_SF");

  //CHECK(m_WTaggerTool_InclEff50->tag(*fatjet_xAOD));
  //fatjet->passWTagIncl50      = fatjet_xAOD->auxdata<bool>("SmoothInclusiveW50_Tagged");
  //fatjet->passWTagIncl50_mass = fatjet_xAOD->auxdata<bool>("SmoothInclusiveW50_PassMassLow") && fatjet_xAOD->auxdata<bool>("SmoothInclusiveW50_PassMassHigh");
  //fatjet->passWTagIncl50_sub  = fatjet_xAOD->auxdata<bool>("SmoothInclusiveW50_PassD2");
  //fatjet->passWTagIncl50_nTrk = fatjet_xAOD->auxdata<bool>("SmoothInclusiveW50_PassNtrk");
  //fatjet->bjt_wsfIncl50       = fatjet_xAOD->auxdata<float>("SmoothInclusiveW50_SF");
    
  //CHECK(m_ZTaggerTool_InclEff50->tag(*fatjet_xAOD));
  //fatjet->passZTagIncl50      = fatjet_xAOD->auxdata<bool>("SmoothInclusiveZ50_Tagged");
  //fatjet->passZTagIncl50_mass = fatjet_xAOD->auxdata<bool>("SmoothInclusiveZ50_PassMassLow") && fatjet_xAOD->auxdata<bool>("SmoothInclusiveZ50_PassMassHigh");
  //fatjet->passZTagIncl50_sub  = fatjet_xAOD->auxdata<bool>("SmoothInclusiveZ50_PassD2");
  //fatjet->passZTagIncl50_nTrk = fatjet_xAOD->auxdata<bool>("SmoothInclusiveZ50_PassNtrk");
  //fatjet->bjt_zsfIncl50       = fatjet_xAOD->auxdata<float>("SmoothInclusiveZ50_SF");
  CHECK(m_WTaggerTool_Eff80->tag(*fatjet_xAOD));
  fatjet->passWTag80      = cacc_SmoothWContained80_Tagged.isAvailable(*fatjet_xAOD) ? cacc_SmoothWContained80_Tagged(*fatjet_xAOD) : -10.;
  //fatjet->passWTag80_mass = cacc_SmoothWContained80_PassMass.isAvailable(*fatjet_xAOD) ? cacc_SmoothWContained80_PassMass(*fatjet_xAOD) : -10.;
  //fatjet->passWTag80_sub  = cacc_SmoothWContained80_PassD2.isAvailable(*fatjet_xAOD) ? cacc_SmoothWContained80_PassD2(*fatjet_xAOD) : -10.;
  //fatjet->passWTag80_nTrk = cacc_SmoothWContained80_PassNtrk.isAvailable(*fatjet_xAOD) ? cacc_SmoothWContained80_PassNtrk(*fatjet_xAOD) : -10.;
  fatjet->bjt_wsf80       = cacc_SmoothWContained80_SF.isAvailable(*fatjet_xAOD) ? cacc_SmoothWContained80_SF(*fatjet_xAOD) : -10.;
    
  CHECK(m_ZTaggerTool_Eff80->tag(*fatjet_xAOD));
  fatjet->passZTag80      = cacc_SmoothZContained80_Tagged.isAvailable(*fatjet_xAOD) ? cacc_SmoothZContained80_Tagged(*fatjet_xAOD) : -10.;
  //fatjet->passZTag80_mass = cacc_SmoothZContained80_PassMass.isAvailable(*fatjet_xAOD) ? cacc_SmoothZContained80_PassMass(*fatjet_xAOD) : -10.;
  //fatjet->passZTag80_sub  = cacc_SmoothZContained80_PassD2.isAvailable(*fatjet_xAOD) ? cacc_SmoothZContained80_PassD2(*fatjet_xAOD) : -10.;
  //fatjet->passZTag80_nTrk = cacc_SmoothZContained80_PassNtrk.isAvailable(*fatjet_xAOD) ? cacc_SmoothZContained80_PassNtrk(*fatjet_xAOD) : -10.;
  fatjet->bjt_zsf80       = cacc_SmoothZContained80_SF.isAvailable(*fatjet_xAOD) ? cacc_SmoothZContained80_SF(*fatjet_xAOD) : -10.;
    
  // Keep the nominal for calculating boson tagging SF variation
  if( sys_name==""  ){
    fatjet->Nominal_pt  = fatjet->Pt();
    fatjet->Nominal_eta = fatjet->Eta();
    fatjet->Nominal_phi = fatjet->Phi();
    fatjet->Nominal_m   = fatjet->M();
    /*
    if(fatjet->nTrk>=0){
      fatjet->Nominal_bjt_weffIncl50 = fatjet_xAOD->auxdata<float>("SmoothInclusiveW_efficiency");
      fatjet->Nominal_bjt_zeffIncl50 = fatjet_xAOD->auxdata<float>("SmoothInclusiveZ_efficiency");
    }
    */
  }
  
  // More detailed jet information
  //fillDetailedJetVariables(fatjet_xAOD,fatjet);

  // Save
  it->second->push_back(fatjet);

  Timer::Instance()->End( "JetObject::fillFatjetContainer" );

}
// ------------------------------------------------------------------------- //
void JetObject::fillTrackjetContainer(xAOD::Jet* trackjet_xAOD,
                                      std::string sys_name,
                                      std::map<TString,float> FT_SF)
{

  Timer::Instance()->Start( "JetObject::fillTrackjetContainer" );

  std::map<TString,JetVector*>::iterator it = m_jetVectorMap.find(sys_name);

  if( it==m_jetVectorMap.end() ){
    MsgLog::ERROR("JetObject::fillTrackjetContainer","Request to get jet for unknown systematic: %s",sys_name.c_str() );
    return;
  } 

  JetVariable* trackjet = new JetVariable();

  // TLV
  trackjet->SetPtEtaPhiM(trackjet_xAOD->pt() * m_convertFromMeV,
                         trackjet_xAOD->eta(),
                         trackjet_xAOD->phi(),
                         trackjet_xAOD->m() * m_convertFromMeV );

  // Fill b-tagging information
  fillTrackjetBTaggingInfo(trackjet_xAOD,trackjet);

  // Weight maps
  trackjet->FT_SF  = FT_SF;

  // Jet classification, from SUSYTools
  trackjet->signal = cacc_signal.isAvailable( *trackjet_xAOD ) ? cacc_signal( *trackjet_xAOD ) : false; 
  trackjet->bad    = cacc_bad.isAvailable   ( *trackjet_xAOD ) ? cacc_bad   ( *trackjet_xAOD ) : false;
  trackjet->passOR = cacc_passOR.isAvailable( *trackjet_xAOD ) ? cacc_passOR( *trackjet_xAOD ) : false;
  trackjet->passDRcut = cacc_passDRcut.isAvailable( *trackjet_xAOD ) ? cacc_passDRcut( *trackjet_xAOD ) : false;

  // JVT, recalculated in SUSYTools
  trackjet->jvt = cacc_Jvt.isAvailable(*trackjet_xAOD) ? cacc_Jvt( *trackjet_xAOD ) : 1.;

  // Truth information
  trackjet->truthLabel = cacc_HadronConeExclTruthLabelID.isAvailable( *trackjet_xAOD ) ? 
    cacc_HadronConeExclTruthLabelID( *trackjet_xAOD ) : 0; 

  // nTrk
  trackjet->nTrk = trackjet_xAOD->getConstituents().size();

  // Detailed jet information
  fillDetailedJetVariables(trackjet_xAOD,trackjet);

  // Save
  it->second->push_back(trackjet);

  Timer::Instance()->End( "JetObject::fillTrackjetContainer" );

}
// ------------------------------------------------------------------------- //
void JetObject::fillJetContainerTruth(xAOD::Jet* jet_xAOD, std::string sys_name /*=""*/)
{

  std::map<TString,JetVector*>::iterator it = m_jetVectorMap.find(sys_name);
    
  if( it==m_jetVectorMap.end() ){
    MsgLog::ERROR( "JetObject::fillJetContainerTruth","Request to get jet for unknown systematic: %s",sys_name.c_str() );
    return;
  } 

  JetVariable* jet = new JetVariable();

  // TLV
  jet->SetPtEtaPhiM(jet_xAOD->pt() * m_convertFromMeV,
                    jet_xAOD->eta(),
                    jet_xAOD->phi(),
                    jet_xAOD->m() * m_convertFromMeV );
  
  // B-jet classification
  double beff = getBTagEfficiency(jet_xAOD);
  bool bjet = isTruthBJet(jet_xAOD,beff);
  //jet->mv2c10  = bjet ? 1.0 : -1.0; 
  //jet->dl1     = bjet ? 1.0 : -1.0; 
  jet->dl1r    = bjet ? 1.0 : -1.0; 
  jet->dl1dv01 = bjet ? 1.0 : -1.0; 
  jet->bjet    = bjet; 
  jet->btagEff = beff;

  std::map<TString, float> FT_SF;
  FT_SF[""] = 1.0;
  jet->FT_SF = FT_SF;

  // TODO:
  //  Maybe some truth matching, to see if this jet is from the hard scattering or pileup?
  jet->jvt = 1.0;
  jet->bad = false;

  // TODO:
  //  Dependent on jvt/bad jet truth matching outcome?
  jet->signal = true;
  jet->passOR = true;

  // Truth information
  jet->truthLabel = cacc_HadronConeExclTruthLabelID.isAvailable( *jet_xAOD ) ? 
    cacc_HadronConeExclTruthLabelID( *jet_xAOD ) : 0; 

  // Save
  it->second->push_back(jet);

}
// ------------------------------------------------------------------------- //
void JetObject::fillFatjetContainerTruth(xAOD::Jet* fatjet_xAOD, std::string sys_name /*=""*/)
{

  std::map<TString,JetVector*>::iterator it = m_jetVectorMap.find(sys_name);
    
  if( it==m_jetVectorMap.end() ){
    MsgLog::ERROR( "JetObject::fillFatjetContainerTruth","Request to get jet for unknown systematic: %s",sys_name.c_str() );
    return;
  } 

  JetVariable* fatjet = new JetVariable();

  // TLV
  fatjet->SetPtEtaPhiM(fatjet_xAOD->pt() * m_convertFromMeV,
		       fatjet_xAOD->eta(),
		       fatjet_xAOD->phi(),
		       fatjet_xAOD->m() * m_convertFromMeV );
  
  // Substructure
  // Tau32
  float Tau2 = cacc_Tau2_wta.isAvailable(*fatjet_xAOD) ? cacc_Tau2_wta(*fatjet_xAOD) : 0.;
  float Tau3 = cacc_Tau3_wta.isAvailable(*fatjet_xAOD) ? cacc_Tau3_wta(*fatjet_xAOD) : 0.;
  fatjet->Tau32 = Tau2>0. ? Tau3/Tau2 : 0.;
  
  // D2
  if(cacc_D2.isAvailable(*fatjet_xAOD))
    fatjet->D2 =  cacc_D2(*fatjet_xAOD) ;
  else{
    float ECF1 = cacc_ECF1.isAvailable(*fatjet_xAOD) ? cacc_ECF1(*fatjet_xAOD) : 0.;
    float ECF2 = cacc_ECF2.isAvailable(*fatjet_xAOD) ? cacc_ECF2(*fatjet_xAOD) : 0.;
    float ECF3 = cacc_ECF3.isAvailable(*fatjet_xAOD) ? cacc_ECF3(*fatjet_xAOD) : 0.;
    fatjet->D2 = ECF2!=0 ? ECF3*ECF1*ECF1*ECF1/ECF2/ECF2/ECF2 : -1.;
  }

  // Truth information
  fatjet->truthLabel = cacc_FatjetTruthLabel.isAvailable( *fatjet_xAOD ) ? 
    cacc_FatjetTruthLabel( *fatjet_xAOD ) : -9; 
  
  // Save
  it->second->push_back(fatjet);

}
// ------------------------------------------------------------------------- //
void JetObject::fillBTaggingInfo(xAOD::Jet* jet_xAOD, JetVariable*& jetOut)
{

  if( !m_doBTagging ) return;

  //double weight_mv2c10 = -1.; 
  //if(m_bTagSelTool_MV2c10) m_bTagSelTool_MV2c10->getTaggerWeight(*jet_xAOD, weight_mv2c10);
  double weight_dl1r = -1.; 
  if(m_bTagSelTool_DL1r) {
    m_bTagSelTool_DL1r->getTaggerWeight(*jet_xAOD, weight_dl1r);
  }
  double weight_dl1dv01 = -1;
  if(m_bTagSelTool_DL1dv01) {
    m_bTagSelTool_DL1dv01->getTaggerWeight(*jet_xAOD, weight_dl1dv01);
  }
  
  //double weight_dl1 = -1.; 
  // commented out above
  // if(m_bTagSelTool_DL1) {
    // m_bTagSelTool_DL1->getTaggerWeight(*jet_xAOD, weight_dl1);
  // }

  //
  jetOut->dl1r    = weight_dl1r;
  jetOut->dl1dv01    = weight_dl1dv01;
  jetOut->bjet_continuous = cacc_bjet.isAvailable( *jet_xAOD ) ? cacc_bjet( *jet_xAOD ) : -2;
  //
  if (m_ContinuousBtagScore > -2){ // When User-level criteria for continuous btag score set
    jetOut->bjet    = cacc_bjet.isAvailable( *jet_xAOD ) ? cacc_bjet( *jet_xAOD ) >= m_ContinuousBtagScore : false;
  } else {
    jetOut->bjet    = cacc_bjet.isAvailable( *jet_xAOD ) ? cacc_bjet( *jet_xAOD ) : false;
  }
  jetOut->btagEff = 1.0; // getBTagEfficiency(jet_xAOD); 

}
// ------------------------------------------------------------------------- //
void JetObject::fillTrackjetBTaggingInfo(xAOD::Jet* trackjet_xAOD, JetVariable*& trackjet)
{

  if( !m_doBTagging ) return;


  double weight_dl1dv01 = -1.; 
  if(m_bTagSelTool_trkJet_DL1dv01) m_bTagSelTool_trkJet_DL1dv01->getTaggerWeight(*trackjet_xAOD, weight_dl1dv01);

  trackjet->dl1dv01    = weight_dl1dv01;

  //
  trackjet->bjet    = cacc_bjet.isAvailable( *trackjet_xAOD ) ? cacc_bjet( *trackjet_xAOD ) : false;
  trackjet->btagEff = 1.0; // getBTagEfficiency(jet_xAOD); 

}
// ------------------------------------------------------------------------- //
bool JetObject::fillFatjetBTaggingInfo(xAOD::Jet* fatjet_xAOD, JetVariable*& fatjet)
{

  //
  // Count the number of btagged track jets ghost-associated to the fat jet
  //

  // Get the link to the collection of ghost-associated track jets
  std::vector<const xAOD::Jet*> associated_trackJets;
  if(!fatjet_xAOD->getAssociatedObjects<xAOD::Jet>("GhostAntiKtVR30Rmax4Rmin02TrackJets", associated_trackJets)){
    //MsgLog::WARNING("JetObject::fillFatjetContainer","No associated track jets found on fatJet");
    return false;
  }

  // Number of ghost-associated track jets
  fatjet->nGATrackJets = associated_trackJets.size();
  
  // Initialize the number of Ghost-Associated Track B-Jets (GATBJ)
  fatjet->GATrackJets.clear();
  fatjet->nGATrackBJets85Eff = 0; // MV2c10 85% eff WP
  fatjet->nGATrackBJets77Eff = 0; // MV2c10 77% eff WP
  fatjet->nGATrackBJets70Eff = 0; // MV2c10 70% eff WP
  fatjet->nGATrackBJets60Eff = 0; // MV2c10 60% eff WP
  fatjet->nGATrackBJets85Eff_DL1r  = 0; // DL1r 85% eff WP
  fatjet->nGATrackBJets77Eff_DL1r  = 0; // DL1r 77% eff WP
  fatjet->nGATrackBJets70Eff_DL1r  = 0; // DL1r 70% eff WP
  fatjet->nGATrackBJets60Eff_DL1r  = 0; // DL1r 60% eff WP

  // Loop over the GA track collection
  for(int nGATJ = 0; nGATJ<(int)associated_trackJets.size(); nGATJ++){
    auto& fatGATJ = associated_trackJets.at(nGATJ);
    if(!fatGATJ) continue;

    // Track jet selection
    if( fabs(fatGATJ->eta()) > 2.5 || fatGATJ->pt()<20e3 ) continue;

    // Track jet cleaning
    if(!cacc_passDRcut.isAvailable ( *fatGATJ )) MsgLog::WARNING("JetObject::fillFatjetBTaggingInfo_DRMatch","Failed to retrieve passDRcut");
    else if(!cacc_passDRcut( *fatGATJ )) continue;

    // Fill the track jet
    fillFatjetBTaggingInfo_Impl(fatGATJ, fatjet);
  }

  // Flagged as b-taggd if it has at least one MV2c10 85% eff WP b-jet
  fatjet->bjet = fatjet->nGATrackBJets85Eff>=1;

  return true;

}
// ------------------------------------------------------------------------- //
void JetObject::fillFatjetBTaggingInfo_DRMatch(xAOD::Jet* fatjet_xAOD, 
                                               JetVariable*& fatjet,
                                               const xAOD::JetContainer* trackjets_xAOD,
                                               const float DRThresh)
{

  //
  // Count the number of btagged track jets ghost-associated to the fat jet
  //
  MsgLog::INFO("JetObject::fillFatjetBTaggingInfo_DRMatch","Start function");

  //if(!trackjets_xAOD){
  //  MsgLog::WARNING("JetObject::fillFatjetBTaggingInfo_DRMatch","The input track jet container is null. Exit.");
  //  return ;
  //}

  // Initialize the number of DR-matched Track B-Jets (the name GATBJ might be mis-leading since it doesn't use ghost-association here but it falls back)
  fatjet->GATrackJets.clear();
  fatjet->nGATrackJets = 0;
  fatjet->nGATrackBJets85Eff = 0; // MV2c10 85% eff WP
  fatjet->nGATrackBJets77Eff = 0; // MV2c10 77% eff WP
  fatjet->nGATrackBJets70Eff = 0; // MV2c10 70% eff WP
  fatjet->nGATrackBJets60Eff = 0; // MV2c10 60% eff WP
  fatjet->nGATrackBJets85Eff_DL1r  = 0; // DL1r 85% eff WP
  fatjet->nGATrackBJets77Eff_DL1r  = 0; // DL1r 77% eff WP
  fatjet->nGATrackBJets70Eff_DL1r  = 0; // DL1r 70% eff WP
  fatjet->nGATrackBJets60Eff_DL1r  = 0; // DL1r 60% eff WP
  
  // Loop over the whole track jet collection
  for(auto trackjet_xAOD : *trackjets_xAOD){
    if(!trackjet_xAOD) continue;

    // Track jet selection
    if( fabs(trackjet_xAOD->eta()) > 2.5 || trackjet_xAOD->pt()<20e3 ) continue;

    // Track jet cleaning
    if(!cacc_passDRcut.isAvailable ( *trackjet_xAOD )) MsgLog::WARNING("JetObject::fillFatjetBTaggingInfo_DRMatch","Failed to retrieve passDRcut");
    else if(!cacc_passDRcut( *trackjet_xAOD )) continue;

    // DR matching
    if( hypot(trackjet_xAOD->eta()-fatjet_xAOD->eta(), acos(cos( trackjet_xAOD->phi()-fatjet_xAOD->phi() ))) > DRThresh  ) continue;

    // Increment njets/nbjets
    fatjet->nGATrackJets++;

    // Fill the track jet
    fillFatjetBTaggingInfo_Impl(trackjet_xAOD, fatjet);
  }

  // Flagged as b-taggd if it has at least one MV2c10 85% eff WP b-jet
  fatjet->bjet = fatjet->nGATrackBJets85Eff>=1;

}

// ------------------------------------------------------------------------- //
void JetObject::fillFatjetBTaggingInfo_Impl(const xAOD::Jet* trackjet_xAOD, JetVariable*& fatjet)
{
    
    TLorentzVector this_jet(0,0,0,0);
    this_jet.SetPtEtaPhiM(trackjet_xAOD->pt() * m_convertFromMeV,
			  trackjet_xAOD->eta(),
			  trackjet_xAOD->phi(),
			  trackjet_xAOD->m() * m_convertFromMeV );
    fatjet->GATrackJets.push_back(this_jet);
        
    //
    // Count MV2c10-based b-jets assuming AntiKtVR30Rmax4Rmin02TrackJets_BTagging201810
    //

    // Returned bit from the pseudo-continuous b-tagging:
    //  -  5 if between 60% and 0%
    //  -  4 if between 70% and 60%
    //  -  3 if between 77% and 70%
    //  -  2 if between 85% and 77%
    //  -  1 if between 100% and 85%
    //  -  0 if smaller than -1e4-> should never happen
    //  - -1 if bigger than 1e4 or not in b-tagging acceptance

    // int btag_pc_index_MV2c10 = m_bTagSelTool_trkJet_MV2c10 ? 
    //   m_bTagSelTool_trkJet_MV2c10->getQuantile(*trackjet_xAOD) : -999;

    // if( btag_pc_index_MV2c1 0>=2 ) fatjet->nGATrackBJets85Eff++;
    // if( btag_pc_index_MV2c10 >=3 ) fatjet->nGATrackBJets77Eff++;
    // if( btag_pc_index_MV2c10 >=4 ) fatjet->nGATrackBJets70Eff++;
    // if( btag_pc_index_MV2c10 >=5 ) fatjet->nGATrackBJets60Eff++;

    int btag_pc_index_DL1r = m_bTagSelTool_trkJet_DL1r ? 
      m_bTagSelTool_trkJet_DL1r->getQuantile(*trackjet_xAOD) : -999;

    if( btag_pc_index_DL1r >=2 ) fatjet->nGATrackBJets85Eff_DL1r++;
    if( btag_pc_index_DL1r >=3 ) fatjet->nGATrackBJets77Eff_DL1r++;
    if( btag_pc_index_DL1r >=4 ) fatjet->nGATrackBJets70Eff_DL1r++;
    if( btag_pc_index_DL1r >=5 ) fatjet->nGATrackBJets60Eff_DL1r++;

    return ;

}
// ------------------------------------------------------------------------- //
bool JetObject::isTruthBJet(xAOD::Jet* jet_xAOD, double bTagEfficiency)
{

  // Ignore the fake bjet for now
  bool bjet = false;
  if(cacc_GhostBHadronsFinalCount.isAvailable(*jet_xAOD))
    bjet = cacc_GhostBHadronsFinalCount(*jet_xAOD) > 0; 
  else if(cacc_ConeTruthLabelID.isAvailable(*jet_xAOD))
    bjet = std::abs(cacc_ConeTruthLabelID(*jet_xAOD))==5;
  else if(cacc_PartonTruthLabelID.isAvailable(*jet_xAOD))
    bjet = std::abs(cacc_PartonTruthLabelID(*jet_xAOD))==5;

  if(bjet){
    bjet = gRandom->Uniform(0.,1.) < bTagEfficiency;
  }

  return bjet;

}
// ------------------------------------------------------------------------- //
double JetObject::getBTagEfficiency(xAOD::Jet* jet_xAOD)
{

  // M.G. This method is disabled right now until a tool is implemented 
  // in a robust manner

  float eff = 0.0;
  return eff;

  if (fabs(jet_xAOD->eta()) <= 2.5) {
    CP::CorrectionCode result;
    result = m_btagEffTool->getEfficiency(*jet_xAOD,eff);

    //
    switch (result) {
    case CP::CorrectionCode::Error:
      MsgLog::WARNING("JetObject::getBTagEfficiency","Failed to retrieve b-tag efficiency! Setting to 0.0");;
      break;
    case CP::CorrectionCode::OutOfValidityRange:
      MsgLog::WARNING("JetObject::getBTagEfficiency","No valid b-tag efficiency! pt, eta = %f , %f. Setting to 0.0",jet_xAOD->pt(),jet_xAOD->eta() );
      break;
    default:
      break;
    }

  } // Check eta requirements

  return eff;

}
// ------------------------------------------------------------------------- //
int JetObject::getNumTrkPt500(xAOD::Jet* jet_xAOD,const xAOD::VertexContainer* vertices)
{

  const xAOD::Vertex* v = 0;

  //
  for( const auto& vx : *vertices ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
      v = vx;
      break;
    }
  }

  if( !v ) return -1;

  // What we want
  std::vector<int> v_NumTrkPt500 =jet_xAOD->getAttribute< std::vector<int> >(xAOD::JetAttribute::NumTrkPt500);
  if(v_NumTrkPt500.size()==0){
    MsgLog::WARNING("JetObject::getNumTrkPt500","Attribute xAOD::JetAttribute::NumTrkPt500 is empty or invalid! Return -1.");
    return -1;
  }

  return v_NumTrkPt500[v->index()];

}
// ------------------------------------------------------------------------- //
int JetObject::getFatjetNumTrkPt500(xAOD::Jet* jet_xAOD,const xAOD::VertexContainer* vertices)
{

  const xAOD::Vertex* v = 0;

  //
  for( const auto& vx : *vertices ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
      v = vx;
      break;
    }
  }

  if( !v ) return -1;

  // Get the link to the ungroomed fat jet
  static SG::AuxElement::Accessor<ElementLink<xAOD::JetContainer> > ungroomedLink("Parent");
  const xAOD::Jet *ungroomedJet = 0;
  if(ungroomedLink.isAvailable(*jet_xAOD)){    
    ElementLink<xAOD::JetContainer> linkToUngroomed = ungroomedLink(*jet_xAOD);
    if(linkToUngroomed.isValid()){
      ungroomedJet = *linkToUngroomed;
      // Get the xAOD::JetAttribute::NumTrkPt500 attribute from the ungroomed jet
      if(cacc_NumTrkPt500.isAvailable(*ungroomedJet)){
        std::vector<int> v_NumTrkPt500 = ungroomedJet->getAttribute< std::vector<int> >(xAOD::JetAttribute::NumTrkPt500);
        if(v_NumTrkPt500.size()==0){
          MsgLog::WARNING("JetObject::getFatjetNumTrkPt500","Attribute xAOD::JetAttribute::NumTrkPt500 is empty or invalid for the ungroomed fat jet!");
          MsgLog::WARNING("JetObject::getFatjetNumTrkPt500","Make sure the input file does contain AntiKt10LCTopoJetsAuxDyn.NumTrkPt500. Return -1.");
          return -1;
        }
        return v_NumTrkPt500[v->index()];      
      }
    }
  }

  return -1;

}
// ------------------------------------------------------------------------- //
void JetObject::Reset()
{

  std::map<TString,JetVector*>::iterator it;
  for(it = m_jetVectorMap.begin(); it != m_jetVectorMap.end(); it++){
    
    // Free up memory
    for (JetVector::iterator jetItr = it->second->begin(); jetItr != it->second->end(); jetItr++) {
      delete *jetItr;
    }

    // Clear all entries
    it->second->clear();

  }

}
// ----------------------------------------------------------------------- //
const JetVector* JetObject::getObj(TString sysName)
{

  std::map<TString,JetVector*>::iterator it = m_jetVectorMap.find(sysName);

  if( it==m_jetVectorMap.end() ){
    std::cout << "<JetObject::getObj> WARNING Cannot get jet vector for systematic: " << sysName << std::endl;
    return NULL;
  }

  return it->second;

}

