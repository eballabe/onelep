#include "SusySkimMaker/TruthVariable.h"
#include "SusySkimMaker/MsgLog.h"

TruthVariable::TruthVariable()
{

  setDefault(q,-99);
  setDefault(pdgId,0);
  setDefault(status,0);
  setDefault(barcode,0);
  setDefault(parentPdgId,0);
  setDefault(parentBarcode,0);
  setDefault(decayVtxPerp,0.0);
  setDefault(prodVtxPerp,0.0);
  setDefault(isAntiKt4Jet,false);
  setDefault(bjet,false);

}
// -------------------------------------------------------------------------------- //
TruthVariable::TruthVariable(const TruthVariable &rhs):
  ObjectVariable(rhs),
  q(rhs.q),
  pdgId(rhs.pdgId),
  status(rhs.status),
  barcode(rhs.barcode),
  parentPdgId(rhs.parentPdgId),
  parentBarcode(rhs.parentBarcode),
  decayVtxPerp(rhs.decayVtxPerp),
  prodVtxPerp(rhs.prodVtxPerp),
  isAntiKt4Jet(rhs.isAntiKt4Jet),
  bjet(rhs.bjet),
  bareTLV(rhs.bareTLV)
{
}
// -------------------------------------------------------------------------------- //
TruthVariable& TruthVariable::operator=(const TruthVariable &rhs)
{
  if (this != &rhs) {
    ObjectVariable::operator=(rhs);
    q               = rhs.q;
    pdgId           = rhs.pdgId;
    status          = rhs.status;
    barcode         = rhs.barcode;
    parentPdgId     = rhs.parentPdgId;
    parentBarcode   = rhs.parentBarcode;
    decayVtxPerp    = rhs.decayVtxPerp;
    prodVtxPerp     = rhs.prodVtxPerp;
    isAntiKt4Jet    = rhs.isAntiKt4Jet;
    bjet            = rhs.bjet;
    bareTLV         = rhs.bareTLV;
  }
  return *this;
}
// -------------------------------------------------------------------------------- //
void TruthVariable::print()
{

  MsgLog::INFO("TruthVarible::print()","Listing truth particle properties...");
  MsgLog::INFO("TruthVarible::print()","Truth particle pT %f",this->Pt() );
  MsgLog::INFO("TruthVarible::print()","Truth particle eta %f",this->Eta() );
  MsgLog::INFO("TruthVarible::print()","Truth particle pdg ID %i",this->pdgId );
  MsgLog::INFO("TruthVarible::print()","Truth particle barcode %i",this->barcode );
  MsgLog::INFO("TruthVarible::print()","Truth particle parent pdgID %i",this->parentPdgId );
  MsgLog::INFO("TruthVarible::print()","Particle classified as a bjet %i",this->bjet );


}

