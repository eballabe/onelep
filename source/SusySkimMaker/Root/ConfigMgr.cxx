#include "SusySkimMaker/ConfigMgr.h"
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "AsgMessaging/StatusCode.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "PathResolver/PathResolver.h"

//
ConfigMgr::ConfigMgr() : treeMaker(0),
                         histMaker(0),
                         cutflow(0),
                         metaData(0),
                         obj(0),
                         objectTools(0),
                         systematics(0),
                         susyObj("ST"),
                         susyObjLoose("STLoose"),
                         susyObjTighter("STTighter"),
                         susyObjTenacious("STTenacious"),
                         m_sysState(""),
                         m_SUSYToolsConfigFile(""),
                         m_eventCounter(0),
                         m_sumOfWeights(0),
                         m_origNEvents(0),
                         m_isData(false),
                         m_dsid(0),
                         m_derivName(""),
                         m_CBKTree(0),
                         m_forceProcTDC("")
{

  // Trigger analysis objects
  m_anaTrig.clear();
  m_cbkInputFiles.clear();

}
// -------------------------------------------------------------------------- //
void ConfigMgr::Log()
{

  // Log all tools that ConfigMgr uses
  objectTools->Log(metaData);

}
// -------------------------------------------------------------------------- //
void ConfigMgr::Reset_perEvt()
{

  // Reset tools that need a per evt cleanse
  this->objectTools->Reset_perEvt();

  // Fill counter histogram
  if (m_isData) {
    // force data to use 8 digit runNumber
    this->m_eventCounter->Fill(TString::Format("%08d",objectTools->m_eventObject->getRunNumber()).Data(), 1);
  }
  else {
    this->m_eventCounter->Fill(TString::Format("%d",objectTools->m_eventObject->getDSID()).Data(), 1);
  }

}
// -------------------------------------------------------------------------- //
void ConfigMgr::Reset_perSys()
{

  this->objectTools->Reset_perSys();
  this->treeMaker->Reset();

}
// -------------------------------------------------------------------------- //
bool ConfigMgr::init_skim_running(TFile*& file)
{

  // Nominal only
  std::vector<TString> sys;
  sys.push_back("");

  // TreeMaker
  treeMaker = new TreeMaker();
  treeMaker->setSysStringVector(sys);
  treeMaker->setStreamState("tree");
  treeMaker->createTrees(file,"tree");

  // Histograms
  histMaker = new HistMaker();
  histMaker->addOutputFile(file);

  // CutFlowTool
  cutflow = new CutFlowTool();
  cutflow->setSysList(sys);

  // Only other tools we need
  objectTools  = new ObjectTools();
  obj          = new Objects();

  return true;

}
// -------------------------------------------------------------------------- //
StatusCode ConfigMgr::create_xAOD_tools()
{

  // Make all the tools for running over xAOD
  // Benefit of calling this before init_xAOD_running is you can directly
  // communicate now with the tools, before they are initialized

  centralDB     = new CentralDB();
  metaData      = new MetaData();
  cutflow       = new CutFlowTool();
  treeMaker     = new TreeMaker();
  histMaker     = new HistMaker();
  objectTools   = new ObjectTools();
  obj           = new Objects();
  systematics   = new Systematics();
  // Also create all tools inside ObjectTools class
  objectTools->createTools();
  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
StatusCode ConfigMgr::init_xAOD_running(TFile* skimFile, 
                                        xAOD::TEvent* event,
                                        float nEvents,
                                        int writeSkims,
                                        int writeTrees,
                                        int isData,
                                        int isAf2,
                                        TString sysSetName,
                                        bool truthOnly,
                                        int dsid,
                                        bool doFatjets,
                                        bool doTrackjets,
                                        TString SUSYTools)
{

  const char* APP_NAME = "ConfigMgr";

  m_isData = isData;
  m_dsid   = dsid  ;

  // In case the user didn't call before this function
  if( !treeMaker ) create_xAOD_tools();

  // Force process for TruthDecayContainer (TDC) if requested
  CentralDB::retrieve(CentralDBFields::FORCEPROCESSTDC, m_forceProcTDC);
  if (m_forceProcTDC != "") {
    MsgLog::INFO(APP_NAME, "Forcing process for TruthDecayContainer to be %s", m_forceProcTDC.Data());
  }

  // check if also configure instances of SUSYTools for the various MET WPs
  bool getloosemetwp = false;
  CentralDB::retrieve(CentralDBFields::ADDMETLOOSEWP, getloosemetwp);
  bool gettightermetwp = false;
  CentralDB::retrieve(CentralDBFields::ADDMETTIGHTERWP, gettightermetwp);
  bool gettenaciousmetwp = false;
  CentralDB::retrieve(CentralDBFields::ADDMETTENACIOUSWP, gettenaciousmetwp);

  // Turn the LHE flag in TreeMaker on if one wants.
  // This will be switched off if no weights are found in the file at the end of SusySkimDriver::xAODNtMaker::fileExecute.
  bool writeLHE3 = false;
  bool writeLHE3_flat = false;
  CentralDB::retrieve(CentralDBFields::WRITELHE3, writeLHE3);
  CentralDB::retrieve(CentralDBFields::WRITELHE3FLAT, writeLHE3_flat);
  treeMaker->setWriteLHEWeights(writeLHE3, writeLHE3_flat);

  //
  // Main tool: SUSYTools
  //

  // VERBOSE = 1, DEBUG = 2, INFO = 3, WARNING = 4, ERROR = 5
  int susyToolsMsgLvl = (int) MSG::WARNING;
  CentralDB::retrieve(CentralDBFields::SUSYTOOLSVERBOSITY, susyToolsMsgLvl);
  CHECK(susyObj.setProperty("OutputLevel", (MSG::Level) susyToolsMsgLvl));
  susyObj.msg().setLevel((MSG::Level) susyToolsMsgLvl);
  
  if(getloosemetwp    )CHECK(susyObjLoose.setProperty("OutputLevel", (MSG::Level) susyToolsMsgLvl));
  if(gettightermetwp  )CHECK(susyObjTighter.setProperty("OutputLevel", (MSG::Level) susyToolsMsgLvl));
  if(gettenaciousmetwp)CHECK(susyObjTenacious.setProperty("OutputLevel", (MSG::Level) susyToolsMsgLvl));
 
  m_SUSYToolsConfigFile = SUSYTools;
  if( m_SUSYToolsConfigFile=="" ){
    CentralDB::retrieve(CentralDBFields::STCONFIG,m_SUSYToolsConfigFile);
  }
  
  MsgLog::INFO("ConfigMgr::init_xAOD_running","Initializing SUSYTools with configuration file: %s ...",m_SUSYToolsConfigFile.Data() );

  CHECK( susyObj.setProperty( "ConfigFile", m_SUSYToolsConfigFile.Data() ) );

  TString m_SUSYToolsConfigFileLooseMETWP = "";
  TString m_SUSYToolsConfigFileTighterMETWP = "";
  TString m_SUSYToolsConfigFileTenaciousMETWP = "";
  if(getloosemetwp){
    CHECK( susyObjLoose.setProperty( "ConfigFile", m_SUSYToolsConfigFile.Data() ) );
    CHECK( susyObjLoose.setProperty( "METJetSelection" , "Loose") );
    CHECK( susyObjLoose.setProperty( "FwdJetDoJVT", true) );
  }
  if(gettightermetwp){
    CHECK( susyObjTighter.setProperty( "ConfigFile", m_SUSYToolsConfigFile.Data() ) );
    CHECK( susyObjTighter.setProperty( "METJetSelection" , "Tighter") );
    CHECK( susyObjTighter.setProperty( "FwdJetDoJVT", true) );
    
  }
  if(gettenaciousmetwp){
    CHECK( susyObjTenacious.setProperty( "ConfigFile", m_SUSYToolsConfigFile.Data() ) );
    CHECK( susyObjTenacious.setProperty( "METJetSelection" , "Tenacious") );
    CHECK( susyObjTenacious.setProperty( "FwdJetDoJVT" , true) );
  }
  
  CHECK( susyObj.setProperty( "PRWLumiCalcFiles",  objectTools->m_eventObject->getLumiCalcList(isData) ) );
	 
  bool doAutoPRWTool = false;
  CentralDB::retrieve(CentralDBFields::USEAUTOMAGICPRW, doAutoPRWTool);

  bool useCommonMCFilesPRWTool = false;
  CentralDB::retrieve(CentralDBFields::PRWUSECOMMONMCFILES, useCommonMCFilesPRWTool);

  //Addition for SpecialPRWConfigs (i.e. specific samples to not use the automagic tool for)
  std::map < std::vector<int>, TString> SpecialPRWConfigsMap;

  //SpecialPRWConfigsMap[ {dsid, run number} ] = PRW string;
  SpecialPRWConfigsMap[ {364221, 300000} ] = CentralDBFields::PRWSpecial_Ztautau_364221_mc16d;
  SpecialPRWConfigsMap[ {364222, 300000} ] = CentralDBFields::PRWSpecial_Znunu_364222_mc16d;
  SpecialPRWConfigsMap[ {364223, 284500} ] = CentralDBFields::PRWSpecial_Znunu_364223_mc16a;
  SpecialPRWConfigsMap[ {364224, 310000} ] = CentralDBFields::PRWSpecial_WjetsPTV_364224_mc16e;
  SpecialPRWConfigsMap[ {310502, 284500} ] = CentralDBFields::PRWSpecial_QCDdijet_PowerLaw_mc16a;
  SpecialPRWConfigsMap[ {361027, 300000} ] = CentralDBFields::PRWSpecial_QCDdijet_Pyth_361027_mc16d;
  SpecialPRWConfigsMap[ {361032, 300000} ] = CentralDBFields::PRWSpecial_QCDdijet_Pyth_361032_mc16d;
  SpecialPRWConfigsMap[ {800036, 284500} ] = CentralDBFields::PRWSpecial_QCDdijet_Pyth8235_800036_mc16a;
  SpecialPRWConfigsMap[ {800036, 300000} ] = CentralDBFields::PRWSpecial_QCDdijet_Pyth8235_800036_mc16d;
  SpecialPRWConfigsMap[ {800036, 310000} ] = CentralDBFields::PRWSpecial_QCDdijet_Pyth8235_800036_mc16e;
  SpecialPRWConfigsMap[ {412043, 284500} ] = CentralDBFields::PRWSpecial_FourTop_412043_mc20a;
  SpecialPRWConfigsMap[ {412043, 300000} ] = CentralDBFields::PRWSpecial_FourTop_412043_mc20d;
  SpecialPRWConfigsMap[ {412043, 310000} ] = CentralDBFields::PRWSpecial_FourTop_412043_mc20e;
  SpecialPRWConfigsMap[ {512289, 310000} ] = CentralDBFields::PRWSpecial_Ztautau_512289_mc20d;

  unsigned int RunNumber = objectTools->m_eventObject->getRunNumber();
  bool specialPRWConfig = false;

  if (SpecialPRWConfigsMap.find( {m_dsid, RunNumber}) != SpecialPRWConfigsMap.end()) {
      std::cout << "Found special PRW" << std::endl;
      MsgLog::INFO("ConfigMgr::init_xAOD_running", "Special PRW, not using automagic PRW tool!");
      specialPRWConfig = true;
  }

  if(doAutoPRWTool && !specialPRWConfig ){
    MsgLog::INFO("ConfigMgr::init_xAOD_running", "Using automagic PRW tool!");
    CHECK( susyObj.setBoolProperty("AutoconfigurePRWTool",true) );
    CHECK( susyObj.setBoolProperty("PRWUseCommonMCFiles",useCommonMCFilesPRWTool) );
    if(getloosemetwp    )CHECK( susyObjLoose.setBoolProperty("AutoconfigurePRWTool",true) );
    if(gettightermetwp  )CHECK( susyObjTighter.setBoolProperty("AutoconfigurePRWTool",true) );
    if(gettenaciousmetwp)CHECK( susyObjTenacious.setBoolProperty("AutoconfigurePRWTool",true) );

    // option to enable patch to get correct PRW files for dsid 366010 to 366035
    bool doPRWZnunuFix = false;
    CentralDB::retrieve(CentralDBFields::ENABLEPRWZNUNUFIX, doPRWZnunuFix);
    if(doPRWZnunuFix){
      if      (m_dsid >= 366010 && m_dsid <= 366017) CHECK( susyObj.setProperty("AutoconfigurePRWToolHFFilter","BFilter") );
      else if (m_dsid >= 366019 && m_dsid <= 366026) CHECK( susyObj.setProperty("AutoconfigurePRWToolHFFilter","CFilterBVeto") );
      else if (m_dsid >= 366028 && m_dsid <= 366035) CHECK( susyObj.setProperty("AutoconfigurePRWToolHFFilter","CVetoBVeto") );
    }

  }

  else{

    //Not using auto magic PRW Tool
    if (specialPRWConfig) {
        std::vector<std::string> PRWFiles;
        CentralDB::retrieve(SpecialPRWConfigsMap.find({m_dsid, RunNumber})->second, PRWFiles);
        MsgLog::INFO("ConfigMgr::init_xAOD_running", ("Using Special PRW file " + PRWFiles[0]).c_str());
        CHECK( susyObj.setProperty( "PRWConfigFiles",PRWFiles ) );
    }

    //If not special PRWconfig, use EventObject::getPRWProfileList()
    else {
      CHECK( susyObj.setProperty( "PRWConfigFiles",   objectTools->m_eventObject->getPRWProfileList() ) );
    }

    if(getloosemetwp    )CHECK( susyObjLoose.setProperty( "PRWConfigFiles",   objectTools->m_eventObject->getPRWProfileList() ) );
    if(gettightermetwp  )CHECK( susyObjTighter.setProperty( "PRWConfigFiles",   objectTools->m_eventObject->getPRWProfileList() ) );
    if(gettenaciousmetwp)CHECK( susyObjTenacious.setProperty( "PRWConfigFiles",   objectTools->m_eventObject->getPRWProfileList() ) );

    // Actual mu reweighting!
    auto actualMuList = objectTools->m_eventObject->getActualMuList();
    if( actualMuList.size()>0 ){

      // Check we only have a single actual mu file
      // Since SUSYTools can only handle 1 file for this property!
      if( actualMuList.size()!=1 ){
        MsgLog::INFO("ConfigMgr::init_xAOD_running","Detected %i actual mu PRW files! SUSYTools can only handle 1 at the moment! Please fix your configuration file!", actualMuList.size() );
        return StatusCode::FAILURE;
      }

      // Do it!
      MsgLog::INFO("ConfigMgr::init_xAOD_running","Detected actual mu file. Configuring SUSYTools with this file!!" );
      CHECK( susyObj.setProperty( "PRWActualMuFile",actualMuList[0] ) );
      if(getloosemetwp    )CHECK( susyObjLoose.setProperty( "PRWActualMuFile",actualMuList[0] ) );
      if(gettightermetwp  )CHECK( susyObjTighter.setProperty( "PRWActualMuFile",actualMuList[0] ) );
      if(gettenaciousmetwp)CHECK( susyObjTenacious.setProperty( "PRWActualMuFile",actualMuList[0] ) );


    } // Actual mu reweighting configuration

  }

  // Configure and initialize
  if(isData){
    CHECK( susyObj.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::Data) );
    if(getloosemetwp    )CHECK( susyObjLoose.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::Data) );
    if(gettightermetwp  )CHECK( susyObjTighter.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::Data) );
    if(gettenaciousmetwp)CHECK( susyObjTenacious.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::Data) );

  }
  else if(isAf2){
    // AF2
    CHECK( susyObj.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::AtlfastII) );  
    if(getloosemetwp    )CHECK( susyObjLoose.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::AtlfastII) );  
    if(gettightermetwp  )CHECK( susyObjTighter.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::AtlfastII) );  
    if(gettenaciousmetwp)CHECK( susyObjTenacious.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::AtlfastII) );  

  }
  else{
    // FullSim
    CHECK( susyObj.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::FullSim) );
    if(getloosemetwp    )CHECK( susyObjLoose.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::FullSim) );
    if(gettightermetwp  )CHECK( susyObjTighter.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::FullSim) );
    if(gettenaciousmetwp)CHECK( susyObjTenacious.setProperty("DataSource", ST::ISUSYObjDef_xAODTool::DataSource::FullSim) );
  }

  // Truth only mode doesn't use ST
  if( !truthOnly ){

    // Initialize SUSYTools
    CHECK( susyObj.initialize() );
    if(getloosemetwp    )CHECK( susyObjLoose.initialize() );
    if(gettightermetwp  )CHECK( susyObjTighter.initialize() );
    if(gettenaciousmetwp)CHECK( susyObjTenacious.initialize() );

    // Initialize systematics class
    CHECK( systematics->init(susyObj,writeTrees,writeSkims,sysSetName,isData) );
    /**
    if(getloosemetwp    )CHECK( systematics->init(susyObjLoose,0,0,sysSetName,isData) );
    if(gettightermetwp  )CHECK( systematics->init(susyObjTighter,0,0,sysSetName,isData) );
    if(gettenaciousmetwp)CHECK( systematics->init(susyObjTenacious,0,0,sysSetName,isData) );
    */
 
  }
  else{
    CHECK( systematics->initDefaultSet(writeTrees,writeSkims) );

  }

  // Create trees for skim stream
  if( writeSkims>0 ) {
    treeMaker->setSysStringVector( systematics->getKinematicSkimList() );
    treeMaker->createTrees(skimFile,"skim");
  }

  // Ensure we have all systematics in the list
  // Note this also contains the nominal set
  if(writeTrees>=1 && writeSkims<2){
    treeMaker->setSysStringVector( systematics->getKinematicTreeList() );
  }

  // Store systematics for cutflow tool
  cutflow->setSysList( systematics->getKinematicTreeList() );

  // Connect object classes to output trees/files
  CHECK( objectTools->init(event,treeMaker,metaData,nEvents,isData,isAf2,truthOnly,dsid,doFatjets,doTrackjets) );

  m_eventCounter = new TH1F("eventCounter","eventCounter",400,0,400);
  m_sumOfWeights = new TH1F("weighted__AOD","weighted__AOD",400,0,400);
  m_origNEvents  = new TH1F("unweighted__AOD","unweighted__AOD",400,0,400);

  // Return gracefully
  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
void ConfigMgr::doWeightSystematics(bool useTrackJets, TString trigChains)
{

  // Keeping this functionality here, since all selectors already call
  // this function, and all tools that Systematics need are here

  // Only for nominal
  if( m_sysState != "" || !this->systematics ) return;

  // Fill these branches!
  systematics->fillSysBranches(treeMaker,obj,objectTools,useTrackJets,trigChains);

}
// -------------------------------------------------------------------------- //
void ConfigMgr::setSysState(TString sysState)
{

  this->m_sysState = sysState;
  this->cutflow->setSysState(sysState);
  this->treeMaker->setTreeState(sysState);
  this->objectTools->setSysState( std::string(sysState.Data() ) );

}
// -------------------------------------------------------------------------- //
void ConfigMgr::setSystematicSet(const CP::SystematicSet sysSet)
{

  // TODO
  // Replace above method, such that all tools have this info,
  // will help adding in CP tools in Object classes
  // At the moment only testing

  this->objectTools->setSystematicSet(sysSet);

}
// -------------------------------------------------------------------------- //
void ConfigMgr::doTriggerAna()
{

  int randomRunNumber = obj->evt.randomRunNumber;

  // Turned on when we detect that we
  // are running over lepton triggers,
  bool doSFs = false;

  // Loop over all configurations
  for( auto& bN : m_anaTrig ){

    TString branchName = bN.first;

    // False until proven true
    // Take an OR between any chain
    bool matchResult=false;

    for( auto anaTrig : bN.second ){
      // Initialization
      anaTrig->matchedLeptons = std::vector<LeptonVariable*>();
      if( matchResult ) break;

      // Here we apply the run number filter for matching
      if( anaTrig->run_start>=0 && randomRunNumber<anaTrig->run_start ) continue;
      if( anaTrig->run_end>=0 && randomRunNumber>anaTrig->run_end     ) continue;

      // We are within a good year, where the trigger
      // is running and should be applied
      for( auto& trig : anaTrig->splitTrigChain ){

        if( matchResult ) break;

        // MET triggers
        if( trig.BeginsWith("HLT_xe") ){

          //std::cout << "Considering trigger : " << trig << " and its result was : " << obj->met.trigMatchResult(trig) << " with met " << obj->met.Et << std::endl;
          //std::cout << "From the event DB : " << obj->evt.getHLTEvtTrigDec(trig.Data() ) << std::endl;
          //if( !obj->met.trigMatchResult(trig) ) continue;
          if( !obj->evt.getHLTEvtTrigDec(trig.Data()) ) continue;

          matchResult = true;
          break;

        } // End of MET triggers

        // Photon triggers
        else if( trig.BeginsWith("HLT_g") ){

          for( auto& photon : obj->basePhotons ){
            matchResult |= photon->trigMatchResult(trig);
          }

        } // End of photon triggers

        // Lepton triggers
        else{

          //
          doSFs=true;

          bool isSingleElTrig = false;
          bool isDiElTrig = false;
          bool isSingleMuTrig = false;
          bool isDiMuTrig = false;
          bool isElMuTrig = false;
          bool isPhotonTrig = false;

          int nElTrigMatched = 0;
          int nMuTrigMatched = 0;

          std::string trig_string = std::string(trig);
          int muCountInTrigName = 0;
          unsigned int nPos = 0;
          int muMult = 0;

          // Parse trigger name and count the number of times that "mu" occurs
          while((unsigned int)(nPos = trig_string.find("mu", nPos)) != (unsigned int)std::string::npos) {

            muCountInTrigName++;

            // Check if a number is prepended to "mu", e.g. "2mu" as in the case of a symmetric dimuon trigger,
            // and adjust the mu-count according to the mu-multiplicity (subtract 1 since the string "mu" has already been counted as one muon)
            if ( isdigit( trig_string.at(nPos-1) ) ) {
            
              muMult = atoi( &trig_string.at(nPos-1) );
              muCountInTrigName = muCountInTrigName + (muMult-1);
            }
            // Continue to search for "mu" at the position in the trigger name directly after the last occurence
            nPos += std::string("mu").size();
          }
          if ( trig.BeginsWith("HLT_e") && !trig.Contains("mu") ) {
            isSingleElTrig = true;
          }
          else if ( trig.BeginsWith("HLT_2e") && !trig.Contains("mu") ) {
            isDiElTrig = true;
          }
          else if ( trig.BeginsWith("HLT_mu") && muCountInTrigName == 1 ) {
            isSingleMuTrig = true;
          }
          else if ( trig.BeginsWith("HLT_2mu") || muCountInTrigName == 2 ) { // include both HLT_2mu* and HLT_mu*_mu* as dimuon triggers
            isDiMuTrig = true;
          }
          else if ( trig.BeginsWith("HLT_e") && muCountInTrigName == 1 ) {
            isElMuTrig = true;
          }
          else if ( trig.BeginsWith("HLT_g") ) {
            isPhotonTrig = true;
          }

          for( auto& photon : obj->basePhotons ){
            if ( !photon->trigMatchResult(trig) ) continue;
            if ( isPhotonTrig ) matchResult = true;
          }

          unsigned int lepCount = 0;
          unsigned int nLep = obj->baseLeptons.size();

          for( auto& lep : obj->baseLeptons ){

            ++lepCount;

            if( (isSingleMuTrig || isDiMuTrig) && !lep->isMu() ) {
              continue;
            }
            if( (isSingleElTrig || isDiElTrig) && lep->isMu() ){
              continue;
            }

            if( !lep->trigMatchResult(trig) ) continue;

            // Cache the matched lepton
            anaTrig->matchedLeptons.push_back(lep);

            if (lep->isMu()) ++nMuTrigMatched;
            else ++nElTrigMatched;

            if ( isSingleElTrig && nElTrigMatched == 1 ) matchResult = true;
            else if ( isSingleMuTrig && nMuTrigMatched == 1 ) matchResult = true;
            else if ( isDiElTrig && nElTrigMatched == 2 ) matchResult = true;
            else if ( isDiMuTrig && nMuTrigMatched == 2 ) matchResult = true;
            else if ( isElMuTrig && nElTrigMatched == 1 && nMuTrigMatched == 1 ) matchResult = true;
            else {
              continue;
            }

            if (matchResult) {
              break;
            }

          } // Loop over leptons
        } // Lepton triggers
      } // Loop over chains within a given SF
    } // Loop over all SFs

    // Store output
    treeMaker->setBoolVariable( TString::Format("trigMatch_%s",branchName.Data()), matchResult );

    // User request SFs to be saved?
    // Only implemented for lepton triggers at the moment
    if( !doSFs ) continue;

    // Loop over leptons
    //printf("\n \n \n Computing SF. starting loop over leptons...\n ");
    double sum_data_weight = 1.0;
    double sum_mc_weight = 1.0;

    for( auto& lep : obj->baseLeptons ){
      for( auto anaTrig : bN.second ){
	
        // Here we apply the run number filter for SFs, consistent with 
        // what is performed for the matching
        if( anaTrig->run_start>=0 && randomRunNumber<anaTrig->run_start ) continue;
        if( anaTrig->run_end>=0 && randomRunNumber>anaTrig->run_end     ) continue;

        double mc_eff   = 0.0;
        double data_eff = 0.0;
        lep->trigEff.getEff(anaTrig->trigChainSF,"",mc_eff,data_eff); // Nominal only

        if( mc_eff>0.0 && data_eff>0.0 ){
          sum_data_weight *= (1.0 - data_eff);
          sum_mc_weight   *= (1.0 - mc_eff);
        }

      } // Loop over trigger SFs
    } // Loop over baseLeptons

    // Finally compute the SF and we are done!
    double sf = 1.0;
    if( fabs(1.0-sum_mc_weight)>0.0 ){
      sf = (1.0 - sum_data_weight) / (1.0 - sum_mc_weight);
    }

    //
    //treeMaker->setDoubleVariable( Systematics::getBranchPrefix(ObjectTools::WeightType::TRIG,branchName), sf );

  } // Loop over branchName

}
// -------------------------------------------------------------------------- //
void ConfigMgr::addTriggerAna(TString SF,int run_start,int run_end,TString branchName)
{

  // Container for analysis
  TriggerAna* trigAna = new TriggerAna();

  //
  trigAna->run_start    = run_start;
  trigAna->run_end      = run_end;
  trigAna->trigChainSF  = SF;
  trigAna->branchName   = branchName;

  // Scale factors can be logical ORs of many chains
  // Save the decision for each individual chain
  // Assumes we will get the "_OR_" syntax..

  trigAna->splitTrigChain.clear();
  if( SF.Contains("_OR_") ){
    TString clone = TString(SF);

    // some hacks for electrons
    clone.ReplaceAll("SINGLE_E_2015_", "");
    clone.ReplaceAll("_2016_2018_", "_OR_");
    clone.ReplaceAll("_2016_", "_OR_");

    clone.ReplaceAll("_OR_","&");
    Info("ConfigMgr::addTriggerAna", "Replaced trigger string before splitting: %s", clone.Data());

    std::unique_ptr<TObjArray> singleChains( clone.Tokenize("&") );
    if( singleChains->GetEntriesFast() ){
      for (int i = 0 ; i < singleChains->GetEntries() ; i++){
        // Ensure proper naming, since Egamma doesn't have "HLT_"
        TString triggerChainName = singleChains->At(i)->GetName();
        if( !triggerChainName.BeginsWith("HLT_") ){
          triggerChainName = "HLT_"+triggerChainName;
          Info("ConfigMgr::addTriggerAna","Unexpected trigger naming, fixing to %s",triggerChainName.Data() );
        }
        trigAna->splitTrigChain.push_back(triggerChainName);
      }

    }
  }
  else{
    trigAna->splitTrigChain.push_back(SF);
  }

  // TODO: When running in xAOD mode, when we have the trigger tool initialized,
  // I would like to check that each of the chains above, are actually saved in the framework
  // Otherwise one risks the possibility of having an incomplete result

  //
  // Now for the automatic analysis for the user
  // Only perform this if the branch name has not already been added
  auto bN = m_anaTrig.find( branchName );

  // New
  if( bN==m_anaTrig.end() ){

    // Automatically store this trigger weight
    //treeMaker->addDoubleVariable( Systematics::getBranchPrefix(ObjectTools::WeightType::TRIG,trigAna->branchName), 1.0 );

    // Automatically add a branch for the OR of all trigger SF
    treeMaker->addBoolVariable( TString::Format("trigMatch_%s",trigAna->branchName.Data()), false );

    // Automatically configure for systematics
    // Adds all systematic branches for the user
    // Note the Systematics class will only add this
    // to the nominal tree
    // TODO: This won't work right now
    //if( systematics ){
    //  systematics->addTriggerChainSF( trigAna->trigChainSF );
    //}

    // Finally save this info
    std::vector<TriggerAna*> trigAnaVec;
    trigAnaVec.push_back(trigAna);

    //
    m_anaTrig.insert( std::pair<TString,std::vector<TriggerAna*>>(branchName,trigAnaVec) );

  }
  else{
    bN->second.push_back(trigAna);
  }

  // Done!

}
// -------------------------------------------------------------------------- //
std::vector<LeptonVariable*> ConfigMgr::getTrigAnaMatchedLepton(TString branchName)
{

  auto bN = m_anaTrig.find( branchName );
  for( auto TA : bN->second ){
    if( !TA->matchedLeptons.empty() ) return TA->matchedLeptons;
  }
  return {};

}
// -------------------------------------------------------------------------- //
void ConfigMgr::fillEvtCounterOrigSOW(float origSumOfWeights) {

  if (m_isData) {
    // force data to use 8 digit runNumber
    this->m_sumOfWeights->Fill(TString::Format("%08d",objectTools->m_eventObject->getRunNumber()).Data(), origSumOfWeights);
  } else {
    this->m_sumOfWeights->Fill(TString::Format("%d",objectTools->m_eventObject->getDSID()).Data(), origSumOfWeights);
  }

}
// -------------------------------------------------------------------------- //
void ConfigMgr::fillEvtCounterOrigNEvents(float origNEvents) {

  if (m_isData) {
    // force data to use 8 digit runNumber
    this->m_origNEvents->Fill(TString::Format("%08d",objectTools->m_eventObject->getRunNumber()).Data(), origNEvents);
  } else {
    this->m_origNEvents->Fill(TString::Format("%d",objectTools->m_eventObject->getDSID()).Data(), origNEvents);
  }

}
// -------------------------------------------------------------------------- //
void ConfigMgr::fillCBKTree(const xAOD::CutBookkeeperContainer* cbks, TString inputFile/*=""*/)
{

  // Protection against duplicate fills
  if( m_cbkInputFiles.find(inputFile) != m_cbkInputFiles.end() ){
    MsgLog::WARNING("ConfigMgr::fillCBKTree","The input file %s was already used for the CBK TTree. Ignoring!!",inputFile.Data() );
    return;
  }
  m_cbkInputFiles.insert( std::pair<TString,bool>(inputFile,true) );

  std::map<std::string, float> sumOfWeightsMap;
  std::map<std::string, int> nEventsMap;

  unsigned int DatasetNumber = objectTools->m_eventObject->getDSID();
  unsigned int RunNumber = objectTools->m_eventObject->getRunNumber();

  TString derivationName = "";
  CentralDB::retrieve(CentralDBFields::DXAODKERNEL,derivationName);

  std::set<std::string> addedBranches;
  if (!m_CBKTree) {

    // Create the tree if it didn't exist yet
    m_CBKTree = new TTree("CutBookkeepers", "");
    m_CBKTree->SetDirectory(0);

    // lazily create the branches
    for (unsigned int i=0; i<cbks->size(); i++) {
      const auto& cbk = cbks->at(i);

      sumOfWeightsMap[cbk->name()] = 0.;
      if (addedBranches.find(cbk->name()) == addedBranches.end()) {
        sumOfWeightsMap[cbk->name()] = 0.;
        m_CBKTree->Branch((cbk->name()+"_sumOfEventWeights").c_str(), &sumOfWeightsMap[cbk->name()]);

        // nAcceptedEvents only for nominal
        if (cbk->name() == "AllExecutedEvents" || cbk->name() == derivationName) {
          nEventsMap[cbk->name()] = 0.;
          m_CBKTree->Branch((cbk->name()+"_nAcceptedEvents").c_str(), &nEventsMap[cbk->name()]);
        }
        addedBranches.insert(cbk->name());
      }
    }

    m_CBKTree->Branch("DatasetNumber", &DatasetNumber);
    m_CBKTree->Branch("RunNumber", &RunNumber);
    m_CBKTree->Branch("inputFile", &inputFile);
  }

  // Check which branches (full Name) were added
  for (auto branch: *m_CBKTree->GetListOfBranches()) {
    addedBranches.insert(branch->GetName());
  }

  int maxCycle = -1;
  for (unsigned int i=0; i<cbks->size(); i++) {
    const auto& cbk = cbks->at(i);

    if (cbk->name() == "AllExecutedEvents") {
      // Special treatment for "AllExecutedEvents" - use only largest cycle from StreamAOD
      if (!(cbk->cycle() > maxCycle && cbk->inputStream() == "StreamAOD")) {
        continue;
      } else {
        maxCycle = cbk->cycle();
      }
    }
    if (addedBranches.find(cbk->name()+"_sumOfEventWeights") != addedBranches.end()) {
      sumOfWeightsMap[cbk->name()] = cbk->sumOfEventWeights();
      m_CBKTree->SetBranchAddress((cbk->name()+"_sumOfEventWeights").c_str(), &sumOfWeightsMap[cbk->name()]);
    }

    if (addedBranches.find(cbk->name()+"_nAcceptedEvents") != addedBranches.end()) {
      nEventsMap[cbk->name()] = cbk->nAcceptedEvents();
      m_CBKTree->SetBranchAddress((cbk->name()+"_nAcceptedEvents").c_str(), &nEventsMap[cbk->name()]);
    }
  }
  m_CBKTree->SetBranchAddress("DatasetNumber", &DatasetNumber);
  m_CBKTree->SetBranchAddress("RunNumber", &RunNumber);

  TString* filenamePointer = &inputFile;
  m_CBKTree->SetBranchAddress("inputFile", &filenamePointer);

  m_CBKTree->Fill();

}
// -------------------------------------------------------------------------- //
ConfigMgr::~ConfigMgr()
{

  // Clean up memory
  if( treeMaker   ) delete treeMaker;
  if( histMaker   ) delete histMaker;
  if( cutflow     ) delete cutflow;
  if( metaData    ) delete metaData;
  if( obj         ) delete obj;
  if( centralDB   ) delete centralDB;
  if( objectTools ) delete objectTools;
  if( systematics ) delete systematics;
  if( m_CBKTree   ) delete m_CBKTree;
  // Clean up TriggerAna memory
  for(auto& trigAna : m_anaTrig){
    for(auto& ana : trigAna.second){
      delete ana;
    }
    trigAna.second.clear();
  }
  m_anaTrig.clear();

}
